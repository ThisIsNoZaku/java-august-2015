/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Makes10Test {

    private Makes10 has10;

    public Makes10Test() {
    }

    @Before
    public void setUp() {
        has10 = new Makes10();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHas10() {
        assertTrue(has10.makes10(9, 10));
    }

    @Test
    public void testNo10() {
        assertFalse(has10.makes10(9, 9));
    }

    @Test
    public void testSum10() {
        assertTrue(has10.makes10(1, 9));
    }

}
