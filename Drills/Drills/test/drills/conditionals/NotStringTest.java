/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NotStringTest {
    
    public NotStringTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCandy() {
        String in = "candy";
        assertNotSame(in, NotString.notString(in));
    }
    
    @Test
    public void testX() {
        String in = "x";
        assertNotSame(in, NotString.notString(in));
    }
    @Test
    public void testNotBad() {
        String in = "not bad";
        assertSame(in, NotString.notString(in));
    }
}
