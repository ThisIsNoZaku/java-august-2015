/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StartOzTest {

    public StartOzTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testOzymandias() {
        assertEquals("oz", StartOz.startOz("ozymandias"));
    }

    @Test
    public void testBzoo() {
        assertEquals("z", StartOz.startOz("bzoo"));
    }

    @Test
    public void testOxx() {
        assertEquals("o", StartOz.startOz("oxx"));
    }
}
