/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SumDoubleTest {

    SumDouble d;

    public SumDoubleTest() {
    }

    @Before
    public void setUp() {
        d = new SumDouble();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSum1() {
        assertThat(d.sumDouble(1, 2), is(3));
    }

    @Test
    public void testSum2() {
        assertThat(d.sumDouble(3, 2), is(5));
    }

    @Test
    public void testDoubles() {
        assertThat(d.sumDouble(2, 2), is(8));
    }
}
