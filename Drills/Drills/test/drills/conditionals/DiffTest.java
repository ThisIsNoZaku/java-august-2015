/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DiffTest {

    private Diff21 diff;

    public DiffTest() {
    }

    @Before
    public void setUp() {
        diff = new Diff21();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testDiff23() {
        assertThat(diff.diff21(23), is(4));
    }

    @Test
    public void testDiff10() {
        assertThat(diff.diff21(10), is(11));
    }

    @Test
    public void testDiff21() {
        assertThat(diff.diff21(21), is(0));
    }
}
