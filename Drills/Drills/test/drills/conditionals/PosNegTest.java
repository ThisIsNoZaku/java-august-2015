/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PosNegTest {

    public PosNegTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(PosNeg.posNeg(1, -1, false));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(PosNeg.posNeg(-1, 1, false));
    }

    @Test
    public void testSomeMethod3() {
        assertTrue(PosNeg.posNeg(-4, -5, true));
    }
}
