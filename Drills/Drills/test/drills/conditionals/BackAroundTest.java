/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class BackAroundTest {

    public BackAroundTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCat() {
        String in = "cat";
        assertEquals("tcatt", BackAround.backAround(in));
    }

    @Test
    public void testHello() {
        String in = "Hello";
        assertEquals("oHelloo", BackAround.backAround(in));
    }

    @Test
    public void testA() {
        String in = "a";
        assertEquals("aaa", BackAround.backAround(in));
    }
}
