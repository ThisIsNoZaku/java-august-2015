/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class IxStartTest {

    public IxStartTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMixSnacks() {
        assertTrue(IxStart.ixStart("mix snacks"));
    }

    @Test
    public void testPixSnacks() {
        assertTrue(IxStart.ixStart("pix snacks"));
    }

    @Test
    public void testPizSnacks() {
        assertFalse(IxStart.ixStart("piz snacks"));
    }

}
