/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MissingCharTest {

    public MissingCharTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testKitten1() {
        String in = "kitten";
        assertEquals("ktten", MissingChar.missingChar(in, 1));
    }

    @Test
    public void testKitten0() {
        String in = "kitten";
        assertEquals("itten", MissingChar.missingChar(in, 0));
    }

    @Test
    public void testKitten4() {
        String in = "kitten";
        assertEquals("kittn", MissingChar.missingChar(in, 4));
    }
}
