/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class IcyhotTest {

    public IcyhotTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(IcyHot.icyHot(120, -1));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(IcyHot.icyHot(-1, 120));
    }

    @Test
    public void testSomeMethod3() {
        assertFalse(IcyHot.icyHot(2, 120));
    }
}
