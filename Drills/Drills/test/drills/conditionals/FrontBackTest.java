/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontBackTest {

    public FrontBackTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCode() {
        String in = "code";
        assertEquals("eodc", FrontBack.frontBack(in));
    }

    @Test
    public void testA() {
        String in = "a";
        assertEquals("a", FrontBack.frontBack(in));
    }

    @Test
    public void testAb() {
        String in = "ab";
        assertEquals("ba", FrontBack.frontBack(in));
    }

}
