/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EndUpTest {

    public EndUpTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("HeLLO", EndUp.endUp("Hello"));
    }

    @Test
    public void testHiThere() {
        assertEquals("hi thERE", EndUp.endUp("hi there"));
    }

    @Test
    public void testHi() {
        assertEquals("HI", EndUp.endUp("hi"));
    }
}
