/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CloserTest {

    public CloserTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test8_13() {
        assertEquals(8, Closer.closer(8, 13));
    }

    @Test
    public void test13_8() {
        assertEquals(8, Closer.closer(8, 13));
    }

    @Test
    public void test13_7() {
        assertEquals(8, Closer.closer(8, 13));
    }

}
