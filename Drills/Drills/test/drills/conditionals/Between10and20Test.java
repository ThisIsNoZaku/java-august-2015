/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Between10and20Test {

    public Between10and20Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(Between10and20.between10and20(12, 99));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(Between10and20.between10and20(21, 12));
    }

    @Test
    public void testSomeMethod3() {
        assertFalse(Between10and20.between10and20(8, 99));
    }
}
