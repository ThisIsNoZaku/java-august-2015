/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MischievousChildrenTest {

    MischievousChildren children;

    public MischievousChildrenTest() {
    }

    @Before
    public void setUp() {
        children = new MischievousChildren();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testBothTrue() {
        assertTrue(children.areWeInTrouble(true, true));
    }

    @Test
    public void testBothFalse() {
        assertTrue(children.areWeInTrouble(false, false));
    }

    @Test
    public void testDifferent() {
        assertFalse(children.areWeInTrouble(true, false));
    }
}
