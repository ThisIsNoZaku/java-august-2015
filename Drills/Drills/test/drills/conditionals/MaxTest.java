/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MaxTest {

    public MaxTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertEquals(3, Max.max(1, 2, 3));
    }

    @Test
    public void testSomeMethod2() {
        assertEquals(3, Max.max(1, 3, 2));
    }

    @Test
    public void testSomeMethod3() {
        assertEquals(3, Max.max(3, 2, 1));
    }
}
