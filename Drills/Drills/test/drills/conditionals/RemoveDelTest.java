/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RemoveDelTest {

    public RemoveDelTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAdelbc() {
        String in = "adelbc";
        assertEquals("abc", RemoveDel.removeDel(in));
    }

    @Test
    public void testAdelhello() {
        String in = "adelHello";
        assertEquals("aHello", RemoveDel.removeDel(in));
    }

    @Test
    public void testadedbc() {
        String in = "adedbc";
        assertEquals("adedbc", RemoveDel.removeDel(in));
    }
}
