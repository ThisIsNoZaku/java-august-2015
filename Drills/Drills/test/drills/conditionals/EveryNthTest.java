/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EveryNthTest {

    public EveryNthTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMiracle2() {
        assertEquals("Mrce", EveryNth.everyNth("Miracle", 2));
    }

    @Test
    public void testAbcdefg2() {
        assertEquals("aceg", EveryNth.everyNth("abcdefg", 2));
    }

    @Test
    public void testabcdefg3() {
        assertEquals("adg", EveryNth.everyNth("abcdefg", 3));
    }
}
