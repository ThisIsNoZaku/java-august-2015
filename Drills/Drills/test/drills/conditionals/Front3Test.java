/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Front3Test {

    public Front3Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMicrosoft() {
        String in = "Microsoft";
        assertEquals("MicMicMic", Front3.front3(in));
    }

    @Test
    public void testChocolate() {
        String in = "Chocolate";
        assertEquals("ChoChoCho", Front3.front3(in));
    }

    @Test
    public void testAt() {
        String in = "at";
        assertEquals("atatat", Front3.front3(in));
    }

}
