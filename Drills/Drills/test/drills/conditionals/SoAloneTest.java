/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SoAloneTest {

    public SoAloneTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(SoAlone.soAlone(13, 99));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(SoAlone.soAlone(21, 19));
    }

    @Test
    public void testSomeMethod3() {
        assertFalse(SoAlone.soAlone(13, 13));
    }

}
