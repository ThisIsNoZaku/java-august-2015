/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Multiple3or5Test {

    public Multiple3or5Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test3() {
        int in = 3;
        assertTrue(Multiple3or5.multiple(in));
    }

    @Test
    public void test10() {
        int in = 10;
        assertTrue(Multiple3or5.multiple(in));
    }

    @Test
    public void test8() {
        int in = 8;
        assertFalse(Multiple3or5.multiple(in));
    }

}
