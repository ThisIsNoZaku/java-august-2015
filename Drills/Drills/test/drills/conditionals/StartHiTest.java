/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StartHiTest {

    public StartHiTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHiThere() {
        assertTrue(StartHi.startHi("hi there"));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(StartHi.startHi("hi"));
    }

    @Test
    public void testSomeMethod3() {
        assertFalse(StartHi.startHi("high up"));
    }
}
