/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SleepingInTest {

    private SleepingIn sleep;

    public SleepingInTest() {
    }

    @Before
    public void setUp() {
        sleep = new SleepingIn();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testWeekday() {
        assertTrue(sleep.canSleepIn(false, false));
    }

    @Test
    public void testVacation() {
        assertFalse(sleep.canSleepIn(true, false));
    }

    @Test
    public void testNeither() {
        assertTrue(sleep.canSleepIn(false, true));
    }
}
