/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ParrotTroubleTest {
    private ParrotTrouble pt;
    
    public ParrotTroubleTest() {
    }
    
    @Before
    public void setUp() {
        pt = new ParrotTrouble();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTalkingBefore7() {
        assertTrue(pt.parrotTrouble(true, 6));
    }
    
    @Test
    public void testTalkingAfter7() {
        assertFalse(pt.parrotTrouble(true, 7));
    }
    
    @Test
    public void testNotTalking() {
        assertFalse(pt.parrotTrouble(false, 6));
    }
    
}
