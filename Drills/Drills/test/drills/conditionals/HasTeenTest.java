/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HasTeenTest {

    public HasTeenTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(HasTeen.hasTeen(13, 20, 10));
    }

    @Test
    public void testSomeMethod2() {
        assertTrue(HasTeen.hasTeen(20, 19, 10));
    }

    @Test
    public void testSomeMethod3() {
        assertTrue(HasTeen.hasTeen(20, 10, 12));
    }
}
