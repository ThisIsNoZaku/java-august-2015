/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NearHundredTest {

    public NearHundredTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNearHundred103() {
        assertTrue(NearHundred.nearHundred(103));
    }

    @Test
    public void testNearHundred90() {
        assertTrue(NearHundred.nearHundred(90));
    }

    @Test
    public void testNearHundred89() {
        assertFalse(NearHundred.nearHundred(89));
    }

    @Test
    public void testNearHundred() {
        for (int i = 1; i <= 1000; i++) {
            if (i >= 90 && (i % 100 <= 10 || i % 100 >= 90)) {
                assertTrue(NearHundred.nearHundred(i));
            } else {
                assertFalse(NearHundred.nearHundred(i));
            }
        }
    }

}
