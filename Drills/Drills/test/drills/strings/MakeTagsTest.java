/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MakeTagsTest {

    public MakeTagsTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testYayI() {
        assertEquals("<i>Yay</i>", MakeTags.makeTags("i", "Yay"));
    }

    @Test
    public void testHelloI() {
        assertEquals("<i>Hello</i>", MakeTags.makeTags("i", "Hello"));
    }

    @Test
    public void testYayCite() {
        assertEquals("<cite>Yay</cite>", MakeTags.makeTags("cite", "Yay"));
    }

}
