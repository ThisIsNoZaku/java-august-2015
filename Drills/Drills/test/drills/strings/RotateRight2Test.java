/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RotateRight2Test {

    public RotateRight2Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("loHel", RotateRight2.rotate("Hello"));
    }

    @Test
    public void testJava() {
        assertEquals("vaja", RotateRight2.rotate("java"));
    }

    @Test
    public void testHi() {
        assertEquals("hi", RotateRight2.rotate("hi"));
    }
}
