/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SwapLastTest {

    public SwapLastTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCoding() {
        assertEquals("codign", SwapLast.swap("coding"));
    }

    @Test
    public void testCat() {
        assertEquals("cta", SwapLast.swap("cat"));
    }

    @Test
    public void testAb() {
        assertEquals("ba", SwapLast.swap("ab"));
    }
}
