/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Rotate2LeftTest {
    
    public Rotate2LeftTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertEquals("lloHe", Rotate2Left.rotate2Left("Hello"));
    }
    @Test
    public void testSomeMethod2() {
        assertEquals("vaja", Rotate2Left.rotate2Left("java"));
    }
    @Test
    public void testSomeMethod3() {
        assertEquals("Hi", Rotate2Left.rotate2Left("Hi"));
    }
    
}
