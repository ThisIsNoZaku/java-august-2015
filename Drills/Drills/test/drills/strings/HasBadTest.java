/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HasBadTest {

    public HasBadTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testBadxx() {
        assertTrue(HasBad.has("badxx"));
    }

    @Test
    public void testXbadxx() {
        assertTrue(HasBad.has("xbadxx"));
    }

    @Test
    public void testXxbadxx() {
        assertFalse(HasBad.has("xxbadxx"));
    }
}
