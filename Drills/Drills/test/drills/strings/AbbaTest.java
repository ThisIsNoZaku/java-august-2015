/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AbbaTest {

    public AbbaTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        assertEquals("HiByeByeHi", Abba.abba("Hi", "Bye"));
    }

    @Test
    public void testSomeMethod2() {
        assertEquals("YoAliceAliceYo", Abba.abba("Yo", "Alice"));
    }

    @Test
    public void testSomeMethod3() {
        assertEquals("WhatUpUpWhat", Abba.abba("What", "Up"));
    }
}
