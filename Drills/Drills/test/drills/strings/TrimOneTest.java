/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TrimOneTest {

    public TrimOneTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertEquals("ell", TrimOne.trimOne("Hello"));
    }

    @Test
    public void testSomeMethod2() {
        assertEquals("av", TrimOne.trimOne("Java"));
    }

    @Test
    public void testSomeMethod3() {
        assertEquals("odin", TrimOne.trimOne("Coding"));
    }
}
