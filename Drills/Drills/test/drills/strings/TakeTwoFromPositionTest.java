/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TakeTwoFromPositionTest {
    
    public TakeTwoFromPositionTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testJava0() {
        assertEquals("ja", TakeTwoFromPosition.take("java", 0));
    }
    @Test
    public void testJava2() {
        assertEquals("va", TakeTwoFromPosition.take("java", 2));
    }
    @Test
    public void testSomeMethod() {
        assertEquals("ja", TakeTwoFromPosition.take("java", 3));
    }
    
}
