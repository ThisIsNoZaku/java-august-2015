/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LastCharsTest {

    public LastCharsTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLastChars() {
        assertEquals("ls", LastChars.lastChars("last", "chars"));
    }

    @Test
    public void testYoMama() {
        assertEquals("ya", LastChars.lastChars("yo", "mama"));
    }

    @Test
    public void testHi() {
        assertEquals("h@", LastChars.lastChars("hi", ""));
    }
}
