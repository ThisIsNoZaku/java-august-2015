/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StripXTest {
    
    public StripXTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testXhix() {
        assertEquals("Hi", StripX.strip("xHix"));
    }
    @Test
    public void testXhi() {
        assertEquals("Hi", StripX.strip("xHi"));
    }
    @Test
    public void testHxix() {
        assertEquals("Hxi", StripX.strip("Hxix"));
    }
    
}
