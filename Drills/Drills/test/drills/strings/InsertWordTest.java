/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InsertWordTest {

    public InsertWordTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testYay() {
        assertEquals("<<Yay>>", InsertWord.insertWord("<<>>", "Yay"));
    }

    @Test
    public void testWooHoo() {
        assertEquals("<<WooHoo>>", InsertWord.insertWord("<<>>", "WooHoo"));
    }

    @Test
    public void testWord() {
        assertEquals("[[Word]]", InsertWord.insertWord("[[]]", "Word"));
    }
}
