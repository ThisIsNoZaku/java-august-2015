/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MiddleTwoTest {

    public MiddleTwoTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testString() {
        assertEquals("ri", MiddleTwo.middleTwo("string"));
    }

    @Test
    public void testCode() {
        assertEquals("od", MiddleTwo.middleTwo("code"));
    }

    @Test
    public void testPractice() {
        assertEquals("ct", MiddleTwo.middleTwo("Practice"));
    }
}
