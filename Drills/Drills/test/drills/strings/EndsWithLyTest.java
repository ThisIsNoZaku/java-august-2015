/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EndsWithLyTest {

    public EndsWithLyTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testOddly() {
        assertTrue(EndsWithLy.endsWithLy("oddly"));
    }

    @Test
    public void testY() {
        assertFalse(EndsWithLy.endsWithLy("y"));
    }

    @Test
    public void testOddy() {
        assertFalse(EndsWithLy.endsWithLy("oddy"));
    }

}
