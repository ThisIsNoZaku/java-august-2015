/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SayHiTest {

    public SayHiTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHiBob() {
        assertEquals("Hello Bob", SayHi.sayHi("Bob"));
    }

    @Test
    public void testHiAllice() {
        assertEquals("Hello Alice", SayHi.sayHi("Alice"));
    }

    @Test
    public void testHiX() {
        assertEquals("Hello X", SayHi.sayHi("X"));
    }
}
