/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TakeOneTest {
    
    public TakeOneTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testSomeMethod1() {
        assertEquals("H", TakeOne.takeOne("Hello", true));
    }
    @Test
    public void testSomeMethod2() {
        assertEquals("o", TakeOne.takeOne("Hello", false));
    }
    @Test
    public void testSomeMethod3() {
        assertEquals("o", TakeOne.takeOne("oh", true));
    }
    
}
