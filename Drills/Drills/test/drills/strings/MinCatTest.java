/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MinCatTest {

    public MinCatTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello_Hi() {
        assertEquals("loHi", MinCat.cat("Hello", "Hi"));
    }

    @Test
    public void testHello_Java() {
        assertEquals("elloJava", MinCat.cat("Hello", "Java"));
    }

    @Test
    public void testJava_Hello() {
        assertEquals("javaello", MinCat.cat("java", "Hello"));
    }
}
