/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AtFirstTest {

    public AtFirstTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("he", AtFirst.atFirst("hello"));
    }

    @Test
    public void testHi() {
        assertEquals("hi", AtFirst.atFirst("hi"));
    }

    @Test
    public void testH() {
        assertEquals("h@", AtFirst.atFirst("h"));
    }
}
