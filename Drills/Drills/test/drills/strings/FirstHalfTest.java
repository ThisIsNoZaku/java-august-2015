/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstHalfTest {
    
    public FirstHalfTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testWooHoo() {
        assertEquals("Woo", FirstHalf.firstHalf("WooHoo"));
    }
    @Test
    public void testHelloThere() {
        assertEquals("Hello", FirstHalf.firstHalf("HelloThere"));
    }
    @Test
    public void testAbcdef() {
        assertEquals("abc", FirstHalf.firstHalf("abcdef"));
    }
}
