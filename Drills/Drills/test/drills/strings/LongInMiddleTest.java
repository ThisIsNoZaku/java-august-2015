/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LongInMiddleTest {

    public LongInMiddleTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        assertEquals("hiHellohi", LongInMiddle.longInMiddle("Hello", "hi"));
    }

    @Test
    public void testSomeMethod1() {
        assertEquals("hiHellohi", LongInMiddle.longInMiddle("hi", "Hello"));
    }

    @Test
    public void testSomeMethod2() {
        assertEquals("baaab", LongInMiddle.longInMiddle("aaa", "b"));
    }
}
