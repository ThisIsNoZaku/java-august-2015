/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TweakFrontTest {

    public TweakFrontTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("llo", TweakFront.tweak("Hello"));
    }

    @Test
    public void testAway() {
        assertEquals("aay", TweakFront.tweak("away"));
    }

    @Test
    public void testAbed() {
        assertEquals("abed", TweakFront.tweak("abed"));
    }
}
