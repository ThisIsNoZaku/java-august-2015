/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontAgainTest {
    
    public FrontAgainTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testEdited() {
        assertTrue(FrontAgain.frontAgain("edited"));
    }
    @Test
    public void testEdit() {
        assertFalse(FrontAgain.frontAgain("edit"));
    }
    @Test
    public void testEd() {
        assertTrue(FrontAgain.frontAgain("ed"));
    }
}
