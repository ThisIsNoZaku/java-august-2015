/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MultipleEndingsTest {

    public MultipleEndingsTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("lololo", MultipleEndings.multipleEndings("Hello"));
    }

    @Test
    public void testAb() {
        assertEquals("ababab", MultipleEndings.multipleEndings("ab"));
    }

    @Test
    public void testHi() {
        assertEquals("HiHiHi", MultipleEndings.multipleEndings("Hi"));
    }
}
