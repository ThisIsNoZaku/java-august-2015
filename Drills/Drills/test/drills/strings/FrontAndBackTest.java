/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontAndBackTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello2() {
        assertEquals("Helo", FrontAndBack.n("Hello", 2));
    }

    @Test
    public void testChocolate3() {
        assertEquals("Choate", FrontAndBack.n("Chocolate", 3));
    }

    @Test
    public void testChocolate1() {
        assertEquals("Ce", FrontAndBack.n("Chocolate", 1));
    }

}
