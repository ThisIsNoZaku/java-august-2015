/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ConCatTest {

    public ConCatTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAbCat() {
        assertEquals("abcat", ConCat.concat("ab", "cat"));
    }

    @Test
    public void testDogCat() {
        assertEquals("dogcat", ConCat.concat("dog", "cat"));
    }

    @Test
    public void testAbc() {
        assertEquals("abc", ConCat.concat("abc", ""));
    }
}
