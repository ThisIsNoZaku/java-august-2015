/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DoNotYakTest {

    public DoNotYakTest() {
    }

    @Test
    public void testYakpak() {
        assertEquals("pak", DoNotYak.doNotYak("yakpak"));
    }

    @Test
    public void testPakyak() {
        assertEquals("pak", DoNotYak.doNotYak("pakyak"));
    }

    @Test
    public void testYak123ya() {
        assertEquals("123ya", DoNotYak.doNotYak("yak123ya"));
    }
}
