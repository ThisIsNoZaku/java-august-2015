/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontTimesTest {

    public FrontTimesTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testChocolate2() {
        assertEquals("ChoCho", FrontTimes.frontTime("Chocolate", 2));
    }

    @Test
    public void testChocolate3() {
        assertEquals("ChoChoCho", FrontTimes.frontTime("Chocolate", 3));
    }

    @Test
    public void testAbc3() {
        assertEquals("AbcAbcAbc", FrontTimes.frontTime("Abc", 3));
    }
}
