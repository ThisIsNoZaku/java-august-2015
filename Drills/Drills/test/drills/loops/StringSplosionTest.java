/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringSplosionTest {

    public StringSplosionTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCode() {
        assertEquals("CCoCodCode", StringSplosion.stringSplosion("Code"));
    }

    @Test
    public void testAbc() {
        assertEquals("aababc", StringSplosion.stringSplosion("abc"));
    }

    @Test
    public void testAb() {
        assertEquals("aab", StringSplosion.stringSplosion("ab"));
    }
}
