/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringTimesTest {
    
    public StringTimesTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testHi2() {
        assertEquals("HiHi", StringTimes.stringTimes("Hi", 2));
    }
    @Test
    public void testHi3() {
        assertEquals("HiHiHi", StringTimes.stringTimes("Hi", 3));
    }
    @Test
    public void testHi1() {
        assertEquals("Hi", StringTimes.stringTimes("Hi", 1));
    }
    
}
