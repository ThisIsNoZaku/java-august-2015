/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DoubleXTest {

    public DoubleXTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAxxbc() {
        assertTrue(DoubleX.doubleX("axxbc"));
    }

    @Test
    public void testAxaxxax() {
        assertFalse(DoubleX.doubleX("axaxxax"));
    }

    @Test
    public void testXxxxx() {
        assertTrue(DoubleX.doubleX("xxxxx"));
    }
}
