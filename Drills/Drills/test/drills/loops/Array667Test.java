/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Array667Test {

    public Array667Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertEquals(1, Array667.array667(new int[]{6, 6, 2}));
    }

    @Test
    public void testSomeMethod2() {
        assertEquals(1, Array667.array667(new int[]{6, 6, 2, 6}));
    }

    @Test
    public void testSomeMethod3() {
        assertEquals(1, Array667.array667(new int[]{6, 7, 2, 6}));
    }
}
