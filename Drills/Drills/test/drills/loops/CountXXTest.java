/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CountXXTest {

    public CountXXTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAbcxx() {
        assertEquals(1, CountXX.countxx("abcxx"));
    }

    @Test
    public void testXxx() {
        assertEquals(2, CountXX.countxx("xxx"));
    }

    @Test
    public void testXxxx() {
        assertEquals(3, CountXX.countxx("xxxx"));
    }
}
