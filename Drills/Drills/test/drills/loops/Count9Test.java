/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Count9Test {

    public Count9Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1_2_9() {
        assertEquals(1, Count9.count9(new int[]{1, 2, 9}));
    }

    @Test
    public void test1_9_9() {
        assertEquals(2, Count9.count9(new int[]{1, 9, 9}));
    }

    @Test
    public void test1_9_9_3_9() {
        assertEquals(3, Count9.count9(new int[]{1, 9, 9, 3, 9}));
    }
}
