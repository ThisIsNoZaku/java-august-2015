/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AltPairsTest {

    public AltPairsTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testKitten() {
        assertEquals("kien", AltPairs.altPairs("kitten"));
    }

    @Test
    public void testChocolate() {
        assertEquals("Chole", AltPairs.altPairs("Chocolate"));
    }

    @Test
    public void testCodingHorror() {
        assertEquals("Congrr", AltPairs.altPairs("CodingHorror"));
    }

}
