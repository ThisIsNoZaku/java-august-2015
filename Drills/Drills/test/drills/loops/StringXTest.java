/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringXTest {

    public StringXTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testXxHxix() {
        assertEquals("xHix", StringX.stringX("xxHxix"));
    }

    @Test
    public void testAbxxxcd() {
        assertEquals("abcd", StringX.stringX("abxxxcd"));
    }

    @Test
    public void testXabxxxcdx() {
        assertEquals("xabcdx", StringX.stringX("xabxxxcdx"));
    }

}
