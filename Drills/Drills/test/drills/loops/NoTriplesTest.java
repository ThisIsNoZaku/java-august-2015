/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NoTriplesTest {

    public NoTriplesTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        assertTrue(NoTriples.noTriples(new int[]{1, 1, 2, 2, 1}));
    }

    @Test
    public void testSomeMethod2() {
        assertFalse(NoTriples.noTriples(new int[]{1, 1, 2, 2, 2, 1}));
    }

    @Test
    public void testSomeMethod3() {
        assertFalse(NoTriples.noTriples(new int[]{1, 1, 1, 2, 2, 2, 1}));
    }
}
