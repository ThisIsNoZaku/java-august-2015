/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SubStringMatchTest {

    public SubStringMatchTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMethod1() {
        assertEquals(3, SubStringMatch.subStringMatch("xxcaazz", "xxbaaz"));
    }

    @Test
    public void testMethod2() {
        assertEquals(2, SubStringMatch.subStringMatch("abc", "abc"));
    }

    @Test
    public void testMethod3() {
        assertEquals(0, SubStringMatch.subStringMatch("abc", "axc"));
    }
}
