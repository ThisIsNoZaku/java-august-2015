/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EveryOtherTest {
    
    public EveryOtherTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testHello() {
        assertEquals("Hlo", EveryOther.everyOther("Hello"));
    }
    @Test
    public void testHi() {
        assertEquals("H", EveryOther.everyOther("Hi"));
    }
    @Test
    public void testHeeololeo() {
        assertEquals("Hello", EveryOther.everyOther("Heeololeo"));
    }
}
