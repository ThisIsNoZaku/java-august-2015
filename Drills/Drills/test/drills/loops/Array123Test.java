/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Array123Test {

    public Array123Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1_1_2_3_1() {
        assertTrue(Array123.array123(new int[]{1, 1, 2, 3, 1}));
    }

    @Test
    public void test1_1_2_4_1() {
        assertFalse(Array123.array123(new int[]{1, 1, 2, 4, 1}));
    }

    @Test
    public void test1_1_2_1_2_3() {
        assertTrue(Array123.array123(new int[]{1, 1, 2, 1, 2, 3}));
    }
}
