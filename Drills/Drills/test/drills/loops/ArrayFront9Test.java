/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ArrayFront9Test {

    public ArrayFront9Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSome1_2_9_3_4() {
        assertTrue(ArrayFront9.arrayFront9(new int[]{1, 2, 9, 3, 4}));
    }

    @Test
    public void testSome1_2_3_4_9() {
        assertTrue(ArrayFront9.arrayFront9(new int[]{1, 2, 9, 3, 4}));
    }

    @Test
    public void testSome1_2_3_4_5() {
        assertTrue(ArrayFront9.arrayFront9(new int[]{1, 2, 9, 3, 4}));
    }

}
