/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CanHazTableTest {

    public CanHazTableTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testStylishness1() {
        assertEquals(2, CanHazTable.canHazTable(5, 10));
    }

    @Test
    public void testStylishness2() {
        assertEquals(0, CanHazTable.canHazTable(5, 2));
    }

    @Test
    public void testStylishness3() {
        assertEquals(1, CanHazTable.canHazTable(5, 5));
    }

}
