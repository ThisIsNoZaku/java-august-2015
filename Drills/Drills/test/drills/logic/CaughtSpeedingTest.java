/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CaughtSpeedingTest {

    public CaughtSpeedingTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test60NoBirthday() {
        assertEquals(0, CaughtSpeeding.caughtSpeeding(60, false));
    }

    @Test
    public void test65NoBirthday() {
        assertEquals(0, CaughtSpeeding.caughtSpeeding(60, false));
    }

    @Test
    public void test65Birthday() {
        assertEquals(0, CaughtSpeeding.caughtSpeeding(60, false));
    }
}
