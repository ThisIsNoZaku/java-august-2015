/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AlarmClockTest {

    public AlarmClockTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMondayNoVacation() {
        assertEquals("7:00", AlarmClock.alarmClock(1, false));
    }

    @Test
    public void FridayNoVacation() {
        assertEquals("7:00", AlarmClock.alarmClock(5, false));
    }

    @Test
    public void testSaturdayNoVacation() {
        assertEquals("10:00", AlarmClock.alarmClock(0, false));
    }
}
