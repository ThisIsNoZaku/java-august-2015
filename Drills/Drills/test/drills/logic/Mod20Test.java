/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod20Test {

    public Mod20Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test20() {
        assertFalse(Mod20.mod20(20));
    }

    @Test
    public void test21() {
        assertTrue(Mod20.mod20(21));
    }

    @Test
    public void test22() {
        assertTrue(Mod20.mod20(22));
    }
}
