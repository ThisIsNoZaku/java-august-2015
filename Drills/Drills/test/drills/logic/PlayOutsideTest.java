/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PlayOutsideTest {

    public PlayOutsideTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod70NotSummer() {
        assertTrue(PlayOutside.playOutside(70, false));
    }

    @Test
    public void testSomeMethod95NotSummer() {
        assertFalse(PlayOutside.playOutside(90, false));
    }

    @Test
    public void testSomeMethod95Summer() {
        assertTrue(PlayOutside.playOutside(95, true));
    }
}
