/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AnswerCellTest {

    public AnswerCellTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() {
        assertTrue(AnswerCell.answerCell(false, false, false));
    }

    @Test
    public void test2() {
        assertFalse(AnswerCell.answerCell(false, false, true));
    }

    @Test
    public void test3() {
        assertFalse(AnswerCell.answerCell(true, false, false));
    }
}
