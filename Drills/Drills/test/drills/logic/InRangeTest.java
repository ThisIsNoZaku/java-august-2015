/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InRangeTest {

    public InRangeTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test5Inside() {
        assertTrue(InRange.inRange(5, false));
    }

    @Test
    public void test11Inside() {
        assertFalse(InRange.inRange(11, false));
    }

    @Test
    public void test11Outside() {
        assertTrue(InRange.inRange(11, true));
    }
}
