/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LastDigitTest {
    
    public LastDigitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test23_19_13() {
        assertTrue(LastDigit.lastDigit(23,19,13));
    }
    @Test
    public void test23_19_12() {
        assertFalse(LastDigit.lastDigit(23,19,12));
    }
    @Test
    public void test23_19_3() {
        assertTrue(LastDigit.lastDigit(23,19,3));
    }
}
