/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SkipSumTest {

    public SkipSumTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSum34() {
        assertEquals(7, SkipSum.skipSum(3, 4));
    }

    @Test
    public void testSum94() {
        assertEquals(20, SkipSum.skipSum(9, 4));
    }

    @Test
    public void testSum1011() {
        assertEquals(21, SkipSum.skipSum(10, 11));
    }
}
