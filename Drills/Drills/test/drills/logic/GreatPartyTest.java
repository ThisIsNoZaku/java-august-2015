/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GreatPartyTest {

    public GreatPartyTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test30CigarWeekday() {
        assertFalse(GreatParty.greatParty(30, false));
    }

    @Test
    public void test50CigarWeekday() {
        assertTrue(GreatParty.greatParty(50, false));
    }

    @Test
    public void test70CigarWeekend() {
        assertTrue(GreatParty.greatParty(70, true));
    }
}
