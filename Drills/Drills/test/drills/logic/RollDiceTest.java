/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RollDiceTest {
    
    public RollDiceTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void test2_3() {
        assertEquals(5, RollDice.rollDice(2, 3, true));
    }
    
    @Test
    public void test3_3() {
        assertEquals(7, RollDice.rollDice(3, 3, true));
    }
    @Test
    public void test3_3_noDoubles() {
        assertEquals(6, RollDice.rollDice(3, 3, false));
    }
}
