/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod35Test {

    public Mod35Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test3() {
        assertTrue(Mod35.mod35(3));
    }

    @Test
    public void test10() {
        assertTrue(Mod35.mod35(10));
    }

    @Test
    public void test15() {
        assertFalse(Mod35.mod35(15));
    }
}
