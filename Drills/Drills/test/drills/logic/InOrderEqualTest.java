/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InOrderEqualTest {
    
    public InOrderEqualTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void test2_5_11() {
        assertTrue(InOrderEqual.inOrderEquals(2, 5, 11, false));
    }
    @Test
    public void test5_7_61() {
        assertFalse(InOrderEqual.inOrderEquals(5, 7, 6, false));
    }
    @Test
    public void test5_5_7_Equals() {
        assertTrue(InOrderEqual.inOrderEquals(5, 5, 7, true));
    }
}
