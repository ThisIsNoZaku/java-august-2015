/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LoveSixTest {

    public LoveSixTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test64() {
        assertTrue(LoveSix.loveSix(6, 4));
    }

    @Test
    public void test45() {
        assertFalse(LoveSix.loveSix(4, 5));
    }

    @Test
    public void test15() {
        assertTrue(LoveSix.loveSix(1, 5));
    }
}
