/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SpecialElevenTest {
    
    public SpecialElevenTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test22() {
        assertTrue(SpecialEleven.specialEleven(22));
    }
    @Test
    public void test23() {
        assertTrue(SpecialEleven.specialEleven(23));
    }
    @Test
    public void test24() {
        assertFalse(SpecialEleven.specialEleven(24));
    }
}
