/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TwoIsOneTest {
    
    public TwoIsOneTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test123() {
        assertTrue(TwoIsOne.twoIsOne(1,2,3));
    }
    @Test
    public void test312() {
        assertTrue(TwoIsOne.twoIsOne(3,1,2));
    }
    @Test
    public void test322() {
        assertFalse(TwoIsOne.twoIsOne(3,2,2));
    }
}
