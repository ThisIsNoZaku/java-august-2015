/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AreInOrderTest {

    public AreInOrderTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test124False() {
        assertTrue(AreInOrder.areInOrder(1, 2, 4, false));
    }

    @Test
    public void test121False() {
        assertFalse(AreInOrder.areInOrder(1, 2, 1, false));
    }

    @Test
    public void test112True() {
        assertTrue(AreInOrder.areInOrder(1, 1, 2, true));
    }

}
