/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.Keep23;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Keep23Test {

    Keep23 k23;

    public Keep23Test() {
    }

    @Before
    public void setUp() {
        k23 = new Keep23();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testDouble2s() {
        int[] in = new int[]{2, 2, 3};
        assertTrue(k23.Double23(in));
    }

    @Test
    public void testDouble3s() {
        int[] in = new int[]{3, 4, 5, 3};
        assertTrue(k23.Double23(in));
    }

    @Test
    public void testNoDoubles() {
        int[] in = new int[]{2, 3, 2, 2};
        assertFalse(k23.Double23(in));
    }
}
