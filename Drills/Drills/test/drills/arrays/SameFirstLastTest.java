/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.SameFirstLast;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SameFirstLastTest {

    SameFirstLast sfl;

    public SameFirstLastTest() {
    }

    @Before
    public void setUp() {
        sfl = new SameFirstLast();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void firstLastSameTest() {
        assertTrue(sfl.firstLastSame(new int[]{1, 2, 3, 1}));
    }

    @Test
    public void firstLastSameTest2() {
        assertTrue(sfl.firstLastSame(new int[]{1, 2, 1}));
    }

    @Test
    public void firstLastDifferentTest() {
        assertFalse(sfl.firstLastSame(new int[]{1, 2, 3}));
    }

}
