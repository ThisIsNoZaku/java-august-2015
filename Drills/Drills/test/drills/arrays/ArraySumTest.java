/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.ArraySum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ArraySumTest {

    ArraySum s;

    public ArraySumTest() {
    }

    @Before
    public void setUp() {
        s = new ArraySum();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sum1Test() {
        assertTrue(s.sum(new int[]{1, 2, 3}) == 6);
    }

    @Test
    public void sum2Test() {
        assertTrue(s.sum(new int[]{5, 11, 2}) == 18);
    }

    @Test
    public void sum3Test() {
        assertTrue(s.sum(new int[]{7, 0, 0}) == 7);
    }
}
