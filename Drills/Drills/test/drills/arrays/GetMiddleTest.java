/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.GetMiddle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GetMiddleTest {

    private GetMiddle gm;

    public GetMiddleTest() {
    }

    @Before
    public void setUp() {
        gm = new GetMiddle();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetMiddle1() {
        int[][] in = new int[][]{{1, 2, 3}, {4, 5, 6}};
        assertArrayEquals(gm.getMiddle(in[0], in[1]), new int[]{2, 5});
    }

    @Test
    public void testGetMiddle2() {
        int[][] in = new int[][]{{7, 7, 7}, {3, 8, 0}};
        assertArrayEquals(gm.getMiddle(in[0], in[1]), new int[]{7, 8});
    }

    @Test
    public void testGetMiddle3() {
        int[][] in = new int[][]{{5, 2, 9}, {1, 4, 5}};
        assertArrayEquals(gm.getMiddle(in[0], in[1]), new int[]{2, 4});
    }
}
