/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.MakePi;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MakePiTest {

    MakePi pi;

    public MakePiTest() {
    }

    @Before
    public void setUp() {
        pi = new MakePi();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        assertArrayEquals(new int[]{3, 1, 4}, pi.makePi(3));
    }
}
