/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.Make2;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Make2Test {

    private Make2 make2;

    public Make2Test() {
    }

    @Before
    public void setUp() {
        make2 = new Make2();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMake2First() {
        int[] a = new int[]{4, 5};
        int[] b = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{4, 5}, make2.make2(a, b));
    }

    @Test
    public void testMake2OneElemetFirst() {
        int[] a = new int[]{4};
        int[] b = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{4, 1}, make2.make2(a, b));
    }

    @Test
    public void testMake2EmptyFirst() {
        int[] a = new int[]{};
        int[] b = new int[]{1, 2};
        assertArrayEquals(new int[]{1, 2}, make2.make2(a, b));
    }
}
