/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.Fix23;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Fix23Test {

    private Fix23 f23;

    public Fix23Test() {
    }

    @Before
    public void setUp() {
        f23 = new Fix23();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        int[] in = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{1, 2, 0}, f23.fix23(in));
    }

    @Test
    public void testSomeMethod2() {
        int[] in = new int[]{2, 3, 5};
        assertArrayEquals(new int[]{2, 0, 5}, f23.fix23(in));
    }

    @Test
    public void testSomeMethod3() {
        int[] in = new int[]{1, 2, 1};
        assertArrayEquals(new int[]{1, 2, 1}, f23.fix23(in));
    }
}
