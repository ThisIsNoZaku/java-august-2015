/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.Unlucky1;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Unlucky1Test {

    private Unlucky1 unlucky;

    public Unlucky1Test() {
    }

    @Before
    public void setUp() {
        unlucky = new Unlucky1();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        int[] in = new int[]{1, 3, 4, 5};
        assertTrue(unlucky.isUnlucky(in));
    }

    @Test
    public void testSomeMethod2() {
        int[] in = new int[]{2, 1, 3, 4, 5};
        assertTrue(unlucky.isUnlucky(in));
    }

    @Test
    public void testSomeMethod3() {
        int[] in = new int[]{1, 1, 1};
        assertFalse(unlucky.isUnlucky(in));
    }
}
