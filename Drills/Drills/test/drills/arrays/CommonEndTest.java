/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.CommonEnd;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CommonEndTest {

    CommonEnd ce;

    public CommonEndTest() {
    }

    @Before
    public void setUp() {
        ce = new CommonEnd();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSameEndDiffLength() {
        assertTrue(ce.commonEnd(new int[]{1,2,3}, new int[]{7,3}));
    }

    @Test
    public void testSameStartDiffLength() {
        assertTrue(ce.commonEnd(new int[]{1,2,3}, new int[]{1,3}));
    }
    
    @Test
    public void testDiffStartEnd(){
        assertFalse(ce.commonEnd(new int[]{1,2,3}, new int[]{7,3,2}));
    }

}
