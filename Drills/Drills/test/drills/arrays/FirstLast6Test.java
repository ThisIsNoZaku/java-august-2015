/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.FirstLast6;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstLast6Test {

    FirstLast6 fl;

    @Before
    public void setUp() {
        fl = new FirstLast6();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFirstTrue() {
        assertTrue(fl.firstLast6(new int[]{1, 2, 6}));
    }

    @Test
    public void testLastTrue() {
        assertTrue(fl.firstLast6(new int[]{6, 1, 2, 3}));
    }

    @Test
    public void testNoneTrue() {
        assertFalse(fl.firstLast6(new int[]{13, 6, 1, 2, 3}));
    }

}
