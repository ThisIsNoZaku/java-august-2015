/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.KeepLast;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class KeepLastTest {

    private KeepLast kl;

    public KeepLastTest() {
    }

    @Before
    public void setUp() {
        kl = new KeepLast();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testKeepLast1() {
        int[] in = new int[]{4, 5, 6};
        assertArrayEquals(kl.keepLast(in), new int[]{0, 0, 0, 0, 0, 6});
    }

    @Test
    public void testKeepLast2() {
        int[] in = new int[]{1, 2};
        assertArrayEquals(kl.keepLast(in), new int[]{0, 0, 0, 2});
    }

    @Test
    public void testKeepLast3() {
        int[] in = new int[]{3};
        assertArrayEquals(kl.keepLast(in), new int[]{0, 3});
    }
}
