/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.HigherWins;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HigherWinsTest {

    HigherWins hw;

    public HigherWinsTest() {
    }

    @Before
    public void setUp() {
        hw = new HigherWins();
    }

    @After
    public void tearDown() {
    }

    /**
     * Given an array of ints, figure out which is larger between the first and
     * last elements in the array, and set all the other elements to be that
     * value. Return the changed array.
     *
     * HigherWins({1, 2, 3}) -> {3, 3, 3} HigherWins({11, 5, 9}) -> {11, 11, 11}
     * HigherWins({2, 11, 3}) -> {3, 3, 3}
     */
    @Test
    public void testHighestWins1() {
        int[] in = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{3, 3, 3}, hw.highestWins(in));
    }

    @Test
    public void testHighestWins2() {
        int[] in = new int[]{11, 5, 9};
        assertArrayEquals(new int[]{11, 11, 11}, hw.highestWins(in));
    }

    @Test
    public void testHighestWins3() {
        int[] in = new int[]{2, 11, 3};
        assertArrayEquals(new int[]{3, 3, 3}, hw.highestWins(in));
    }

}
