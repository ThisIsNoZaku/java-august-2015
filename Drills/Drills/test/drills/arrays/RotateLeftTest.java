/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.RotateLeft;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RotateLeftTest {

    RotateLeft rl;

    public RotateLeftTest() {
    }

    @Before
    public void setUp() {
        rl = new RotateLeft();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRotate1() {
        int[] in = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{2, 3, 1}, rl.rotateLeft(in));
    }

    @Test
    public void testRotate2() {
        int[] in = new int[]{5, 11, 9};
        assertArrayEquals(new int[]{11, 9, 5}, rl.rotateLeft(in));
    }

    @Test
    public void testRotate3() {
        int[] in = new int[]{7, 0, 0};
        assertArrayEquals(new int[]{0, 0, 7}, rl.rotateLeft(in));
    }
}
