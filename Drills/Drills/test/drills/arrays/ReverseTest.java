/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.Reverse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ReverseTest {

    Reverse r;

    public ReverseTest() {
    }

    @Before
    public void setUp() {
        r = new Reverse();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testReverse() {
        int[] in = new int[]{1, 2, 3};
        assertArrayEquals(new int[]{3, 2, 1}, r.reverse(in));
    }

}
