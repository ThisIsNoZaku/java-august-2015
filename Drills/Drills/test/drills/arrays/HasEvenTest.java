/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import drills.arrays.HasEven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HasEvenTest {

    private HasEven he;

    public HasEvenTest() {
    }

    @Before
    public void setUp() {
        he = new HasEven();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHasEven1() {
        int in[] = new int[]{2, 5};
        assertTrue(he.hasEven(in));
    }

    @Test
    public void testHasEven2() {
        int in[] = new int[]{4, 3};
        assertTrue(he.hasEven(in));
    }

    @Test
    public void testHasEven3() {
        int in[] = new int[]{7, 5};
        assertFalse(he.hasEven(in));
    }

}
