/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class Fix23 {

    /**
     * Given an array, if there is a 2 in the array followed by a 3, the 3 is
     * replaced with a 0 and the changed array is returned.
     *
     * @param in
     * @return
     */
    int[] fix23(int[] in) {
        for (int i = 0; i < in.length - 1; i++) {
            if (in[i] == 2 && in[i + 1] == 3) {
                in[i + 1] = 0;
            }
        }
        return in;
    }

}
