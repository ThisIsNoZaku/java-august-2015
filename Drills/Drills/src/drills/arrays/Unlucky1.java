package drills.arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Unlucky1 {

    /**
     * Given an array, the array is unlucky if it contains a 1 followed by a 3
     * in the first or last two indexed of the array.
     *
     * @param in
     * @return
     */
    boolean isUnlucky(int[] in) {
        for (int i = 0; i < in.length - 1; i++) {
            if (i <= 2 || i >= in.length - 3) {
                if (in[i] == 1 && in[i + 1] == 3) {
                    return true;
                }
            }
        }
        return false;
    }

}
