package drills.arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class KeepLast {

    /**
     * Given an int array, return a new array which is twice as large and filled
     * with 0 values, except that the last value of the new array is the last
     * value of the input array.
     *
     * @param in
     * @return
     */
    int[] keepLast(int[] in) {
        int[] out = new int[in.length * 2];
        out[out.length - 1] = in[in.length - 1];
        return out;
    }

}
