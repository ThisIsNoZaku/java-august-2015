/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class GetMiddle {

    /**
     * Given 2 int arrays, each length 3, returns a new array containing the
     * second value of each input array.
     *
     * @param a
     * @param b
     * @return
     */
    public int[] getMiddle(int[] a, int[] b) {
        return new int[]{a[1], b[1]};
    }
}
