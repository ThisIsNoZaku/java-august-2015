package drills.arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SameFirstLast {

    /**
     * Given an array of ints, return true if the array is length 1 or more, and
     * the first element and the last element are equal.
     *
     * @param i
     * @return
     */
    boolean firstLastSame(int[] i) {
        if(i.length <= 0){
            return false;
        }
        return i[0] == i[i.length-1];
    }

}
