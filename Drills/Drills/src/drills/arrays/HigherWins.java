/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class HigherWins {

    int[] highestWins(int[] in) {
        int highest = in[0] > in[in.length - 1] ? in[0] : in[in.length - 1];
        for(int i = 0; i < in.length; i++){
            in[i] = highest;
        }
        return in;
    }

}
