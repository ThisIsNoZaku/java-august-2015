/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class MakePi {

    /**
     * Return an int array length n containing the first n digits of pi.
     *
     * @param digits
     * @return
     */
    int[] makePi(int digits) {
        int[] result = new int[digits];
        String pi = (Math.PI + "").replace(".", "");
        for (int i = 0; i < digits; i++) {
            result[i] = Integer.parseInt(pi.charAt(i) + "");
        }
        return result;
    }

}
