package drills.arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Make2 {

    /**
     * Given two int arrays, creates a new array 
     * @param a
     * @param b
     * @return 
     */
    int[] make2(int[] a, int[] b) {
        int[] out = new int[2];
        for (int i = 0; i < 2; i++) {
            if (i < a.length) {
                out[i] = a[i];
            } else if (1 < a.length + b.length) {
                out[i] = b[i - a.length];
            }
        }
        return out;
    }
}
