/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class RotateLeft {

    /**
     * Given an array of ints, return an array with the elements "rotated left"
     * so {1, 2, 3} yields {2, 3, 1}.
     *
     * @param a
     * @return
     */
    public int[] rotateLeft(int[] a) {
        int[] out = new int[a.length];
        for (int i = a.length - 1; i > 0; i--) {
            out[i - 1] = a[i];
        }
        out[out.length - 1] = a[0];
        return out;
    }
}
