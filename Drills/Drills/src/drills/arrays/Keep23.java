/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class Keep23 {

    /**
     * Given an int array, return true if the array contains 2 twice or 3 twice.
     *
     * @param numbers
     * @return
     */
    public boolean Double23(int[] numbers) {
        int count2 = 0;
        int count3 = 0;
        for (int number : numbers) {
            if (number == 2) {
                count2++;
            } else if (number == 3) {
                count3++;
            }
        }
        return count2 == 2 || count3 == 2;
    }
}
