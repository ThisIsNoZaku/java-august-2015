/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class ArraySum {

    /**
     * Given an array of ints, return the sum of all the elements.
     *
     * @param a
     * @return
     */
    public int sum(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }
}
