/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class HasEven {

    boolean hasEven(int[] in) {
        for (int i : in) {
            if (i % 2 == 0) {
                return true;
            }
        }
        return false;
    }

}
