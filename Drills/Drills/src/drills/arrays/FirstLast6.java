/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class FirstLast6 {

    /**
     * Given an array of ints , return true if 6 appears as either the first or
     * last element in the array.The array will be length 1 or more
     */
    public boolean firstLast6(int[] i) {
        if (i.length <= 0) {
            return false;
        }
        return i[0] == 6 || i[i.length - 1] == 6;
    }

}
