/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TakeOne {

    static String takeOne(String in, boolean front) {
        return front ? in.substring(0, 1) : in.substring(in.length() - 1);
    }

}
