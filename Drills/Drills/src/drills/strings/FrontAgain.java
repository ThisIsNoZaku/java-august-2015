/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class FrontAgain {

    static boolean frontAgain(String in) {
        return in.substring(0, 2).equals(in.substring(in.length() - 2));
    }

}
