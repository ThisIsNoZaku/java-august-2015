/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TakeTwoFromPosition {

    static Object take(String in, int index) {
        if (index > in.length() - 2) {
            return in.substring(0, 2);
        } else {
            return in.subSequence(index, index + 2);
        }
    }

}
