/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class AtFirst {

    static String atFirst(String in) {
        if (in.length() == 1) {
            return in + "@";
        } else {
            return in.substring(0, 2);
        }
    }

}
