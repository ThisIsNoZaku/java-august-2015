/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class StripX {

    static String strip(String in) {
        if (in.matches("x.*[^x]")) {
            return in.substring(1);
        } else if (in.matches("[^x].*x")) {
            return in.substring(0, in.length() - 1);
        } else if (in.matches("x.*x")) {
            return in.substring(1, in.length() - 1);
        } else {
            return in;

        }
    }

}
