/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MiddleTwo {

    public static String middleTwo(String in) {
        return in.substring(in.length() / 2 - 1, in.length() / 2 + 1);
    }
}
