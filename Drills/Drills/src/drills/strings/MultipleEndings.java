/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MultipleEndings {

    static String multipleEndings(String in) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            b.append(in.substring(in.length() - 2, in.length()));
        }
        return b.toString();
    }

}
