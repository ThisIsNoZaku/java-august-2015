/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class HasBad {

    static boolean has(String in) {
        return in.substring(0, 3).equals("bad") || in.substring(1, 4).equals("bad");
    }

}
