/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class EndsWithLy {

    private static Pattern p = Pattern.compile(".*ly$");

    static boolean endsWithLy(String in) {
        return p.matcher(in).matches();
    }

}
