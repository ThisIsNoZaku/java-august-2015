/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class LongInMiddle {

    static String longInMiddle(String a, String b) {
        String small = a.length() < b.length() ? a : b;
        String large = a.length() > b.length() ? a : b;
        return small + large + small;
    }

}
