/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TweakFront {

    static String tweak(String in) {
        if(in.substring(0, 2).equals("ab")){
            return in;
        } else if (in.substring(0, 1).equals("a")){
            return in.substring(0, 1) + in.substring(2);
        } else {
            return in.substring(2);
        }
    }
    
}
