/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MakeTags {

    static Object makeTags(String tag, String body) {
        return String.format("<%1$s>%2$s</%1$s>", tag, body);
    }
}
