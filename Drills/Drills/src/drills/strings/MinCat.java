/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MinCat {

    static Object cat(String a, String b) {
        return a.substring(Math.max(0, a.length() - b.length())) + b.substring(Math.max(0, b.length() - a.length()));
    }

}
