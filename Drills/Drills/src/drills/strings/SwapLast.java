/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class SwapLast {

    static String swap(String in) {
        StringBuilder b = new StringBuilder(in);
        b.replace(b.length() - 2, b.length(), new StringBuilder(b.substring(b.length() - 2)).reverse().toString());
        return b.toString();
    }

}
