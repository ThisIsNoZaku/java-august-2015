/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class RotateRight2 {

    static String rotate(String in) {
        StringBuilder b = new StringBuilder();
        b.append(in.substring(in.length() - 2)).append(in.substring(0, in.length() - 2));
        return b.toString();
    }

}
