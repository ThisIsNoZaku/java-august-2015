/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class InOrderEqual {

    static boolean inOrderEquals(int a, int b, int c, boolean equalsOkay) {
        return (a < b && b < c) || (equalsOkay && (a<=b && b<=c));
    }
    
}
