/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class AreInOrder {

    static boolean areInOrder(int a, int b, int c, boolean bOk) {
        return (a < b && b < c) || (a < c && bOk);
    }

}
