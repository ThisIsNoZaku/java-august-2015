/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class LoveSix {

    static boolean loveSix(int a, int b) {
        return a == 6 || b == 6 || a + b == 6 || (int) Math.abs(a - b) == 6;
    }

}
