/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class PlayOutside {

    static boolean playOutside(int temperature, boolean isSummer) {
        if (temperature >= 60) {
            if (isSummer) {
                return temperature <= 90;
            } else {
                return temperature <= 100;
            }
        }
        return false;
    }

}
