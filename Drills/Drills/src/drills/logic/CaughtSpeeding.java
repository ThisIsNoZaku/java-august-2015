/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class CaughtSpeeding {

    static Object caughtSpeeding(int speed, boolean isYourBirthday) {
        int birthdayBonus = isYourBirthday ? 5 : 0;
        if (speed <= 60 + birthdayBonus) {
            return 0;
        } else if (speed <= 80 + birthdayBonus) {
            return 1;
        }
        return 2;
    }

}
