/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class TwoIsOne {

    static boolean twoIsOne(int a, int b, int c) {
        return a + b == c || a + c == b || b + c == a;
    }

}
