/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class Mod20 {

    static boolean mod20(int i) {
        return i >= 20 && (i % 20 == 1 || i % 20 == 2);
    }
}
