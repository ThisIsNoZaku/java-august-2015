/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class RollDice {

    static int rollDice(int a, int b, boolean noDoubles) {
        int result = a == b && noDoubles ? (a % 6 + 1) + b : a + b;
        return result;
    }

}
