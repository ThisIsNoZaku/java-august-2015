/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class GreatParty {

    static boolean greatParty(int cigars, boolean isWeekend) {
        return (cigars >= 40 && cigars <= 60 && !isWeekend) || (cigars >= 40 && isWeekend);
    }

}
