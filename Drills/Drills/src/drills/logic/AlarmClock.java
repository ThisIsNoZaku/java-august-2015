/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class AlarmClock {

    static Object alarmClock(int day, boolean onVacation) {
        if (!onVacation) {
            if (day > 0 && day < 6) {
                return "7:00";
            } else {
                return "10:00";
            }
        } else {
            if (day > 0 && day < 6) {
                return "10:00";
            } else {
                return "Off";
            }

        }
    }

}
