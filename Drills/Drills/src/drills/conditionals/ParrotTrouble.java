/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class ParrotTrouble {

    /**
     * Returns true if isTalking is true and hour is less than 7 or greater than
     * 20.
     *
     * @param isTalking
     * @param hour
     * @return
     */
    boolean parrotTrouble(boolean isTalking, int hour) {
        return isTalking && (hour < 7 || hour > 20);
    }

}
