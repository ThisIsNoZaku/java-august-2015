/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class EndUp {

    static Object endUp(String in) {
        StringBuilder b = new StringBuilder(in);
        if (in.length() >= 3) {
            for (int i = b.length() - 1; i > b.length() - 4; i--) {
                b.replace(i, i + 1, b.substring(i, i + 1).toUpperCase());
            }
        } else {
            for (int i = 0; i < b.length(); i++) {
                b.replace(i, i + 1, b.substring(i, i + 1).toUpperCase());
            }
        }
        return b.toString();
    }

}
