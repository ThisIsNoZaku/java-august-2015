/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class FrontBack {

    static String frontBack(String in) {
        StringBuilder b = new StringBuilder(in);
        char swap = b.charAt(0);
        b.replace(0, 1, b.substring(b.length()-1));
        b.replace(b.length() - 1, b.length(), swap + "");
        return b.toString();
    }

}
