/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class StartOz {

    private static final Pattern O_PATTERN = Pattern.compile("^o.*");
    private static final Pattern Z_PATTERN = Pattern.compile("^.z.*");

    static String startOz(String in) {
        boolean omatch = O_PATTERN.matcher(in).matches();
        boolean zmatch = Z_PATTERN.matcher(in).matches();
        if (omatch && !zmatch) {
            return in.substring(0, 1);
        } else if (zmatch && !omatch) {
            return in.substring(1, 2);
        }
        return in.substring(0, 2);
    }

}
