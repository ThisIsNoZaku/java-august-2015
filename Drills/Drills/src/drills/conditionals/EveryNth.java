/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class EveryNth {

    static String everyNth(String in, int n) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < in.length(); i += n) {
            b.append(in.charAt(i));
        }
        return b.toString();
    }

}
