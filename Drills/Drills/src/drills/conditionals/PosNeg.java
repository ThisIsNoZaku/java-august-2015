/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class PosNeg {

    static boolean posNeg(int a, int b, boolean negative) {
        if (!negative) {
            return (a < 0 && b > 0) || (a > 0 && b < 0);
        } else {
            return a < 0 && b < 0;
        }
    }

}
