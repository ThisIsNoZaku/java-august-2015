/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Closer {

    static Object closer(int a, int b) {
        if (Math.abs(a - 10) > Math.abs(b - 10)) {
            return a;
        } else if (Math.abs(a - 10) < Math.abs(b - 10)) {
            return b;
        } else {
            return 0;
        }
    }

}
