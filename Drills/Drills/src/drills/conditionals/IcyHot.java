/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class IcyHot {

    static boolean icyHot(int a, int b) {
        return (a < 0 && b > 100) || (b < 0 && a > 100);
    }

}
