/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class NearHundred {

    static boolean nearHundred(int i) {
        if (i >= 90) {
            i %=  100;
            return i <= 10 || i >= 90;
        }
        return false;
    }

}
