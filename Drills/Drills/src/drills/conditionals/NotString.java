package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class NotString {

    static Object notString(String in) {
        if(in.length() >= 3 && in.subSequence(0, 3).equals("not")){
            return in;
        } else {
            return "not " + in;
        }
    }

}
