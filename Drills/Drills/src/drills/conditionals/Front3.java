/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Front3 {

    static String front3(String in) {
        StringBuilder b = new StringBuilder();
        if (in.length() >= 3) {
            for (int i = 0; i < 3; i++) {
                b.append(in.substring(0, 3));
            }
        } else {
            for (int i = 0; i < 3; i++) {
                b.append(in);
            }
        }
        return b.toString();
    }

}
