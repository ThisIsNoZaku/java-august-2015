/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Diff21 {

    public int diff21(int in) {
        return in <= 21 ? (int) Math.abs(in - 21) : 2 * (int) Math.abs(in - 21);
    }
}
