/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Multiple3or5 {

    static boolean multiple(int in) {
        return in % 3 == 0 || in % 5 == 0;
    }

}
