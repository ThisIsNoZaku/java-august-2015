/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class SleepingIn {

    /**
     * Determines if you can sleep in or not. Returns true if either isWeekday
     * or onVacation are true.
     *
     * @param isWeekday
     * @param onVacation
     * @return
     */
    boolean canSleepIn(boolean isWeekday, boolean onVacation) {
        return isWeekday || onVacation;
    }

}
