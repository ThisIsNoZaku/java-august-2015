/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class MischievousChildren {

    public boolean areWeInTrouble(boolean aSmile, boolean bSmile) {
        return !(aSmile ^ bSmile);
    }
}
