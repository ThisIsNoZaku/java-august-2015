/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class NoTriples {

    static boolean noTriples(int[] in) {
        for (int i = 0; i < in.length - 2; i++) {
            if (in[i] == in[i + 1] && in[i] == in[i + 2]) {
                return false;
            }
        }
        return true;
    }

}
