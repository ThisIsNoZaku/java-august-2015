/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class ArrayFront9 {

    public static boolean arrayFront9(int[] in) {
        int count = 0;
        while (count < 4) {
            if (in[count++] == 9) {
                return true;
            }
        }
        return false;
    }

}
