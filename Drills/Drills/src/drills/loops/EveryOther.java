/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class EveryOther {

    static Object everyOther(String in) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < in.length(); i += 2) {
            b.append(in.charAt(i));
        }
        return b.toString();
    }

}
