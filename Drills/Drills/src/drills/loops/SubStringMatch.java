/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class SubStringMatch {

    static Object subStringMatch(String a, String b) {
        int matches = 0;
        for (int i = 0; i < a.length()-1 && i < b.length()-1; i++) {
            if (a.substring(i, i + 2).equals(b.substring(i, i + 2))) {
                matches++;
            }
        }
        return matches;
    }

}
