/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class Array123 {

    static boolean array123(int[] in) {
        for (int i = 0; i < in.length - 1; i++) {
            if (in[i] == 1 && in[i + 1] == 2 && in[i + 2] == 3) {
                return true;
            }
        }
        return false;
    }

}
