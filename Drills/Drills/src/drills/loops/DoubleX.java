/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class DoubleX {

    private final static Pattern X_PATTERN = Pattern.compile("[^x]*xx.*");

    static boolean doubleX(String in) {
        return X_PATTERN.matcher(in).matches();
    }

}
