/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class Array667 {

    static Object array667(int[] in) {
        int count = 0;
        for (int i = 0; i < in.length - 1; i++) {
            if (in[i] == 6 && (in[i + 1] == 6 || in[i + 1] == 7)) {
                count++;
            }
        }
        return count;
    }

}
