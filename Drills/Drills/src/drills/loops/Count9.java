/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import java.util.Arrays;

/**
 *
 * @author apprentice
 */
public class Count9 {

    static Object count9(int[] in) {
        int count9 = 0;
        for (int i = 0; i < in.length; i++) {
            if (in[i] == 9) {
                count9++;
            }
        }
        return count9;
    }

}
