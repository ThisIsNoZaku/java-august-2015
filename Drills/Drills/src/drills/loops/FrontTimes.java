package drills.loops;

/**
 *
 * @author apprentice
 */
public class FrontTimes {

    static Object frontTime(String in, int count) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < count; i++) {
            if (in.length() >= 3) {
                b.append(in.subSequence(0, 3));
            } else {
                b.append(in);
            }

        }
        return b.toString();
    }

}
