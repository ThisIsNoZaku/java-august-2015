package drills.loops;

/**
 *
 * @author apprentice
 */
public class AltPairs {

    static Object altPairs(String in) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < in.length(); i += 4) {
            b.append(in.substring(i, i + Math.min(2, in.length() - i)));
        }
        return b.toString();
    }

}
