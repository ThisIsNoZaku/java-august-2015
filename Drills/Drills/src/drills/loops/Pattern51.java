package drills.loops;

public class Pattern51 {

    static boolean pattern51(int[] in) {
        for (int i = 0; i < in.length - 2; i++) {
            if (in[i + 1] == in[i] + 5 && in[i + 2] == in[i] - 1) {
                return true;
            }
        }
        return false;
    }

}
