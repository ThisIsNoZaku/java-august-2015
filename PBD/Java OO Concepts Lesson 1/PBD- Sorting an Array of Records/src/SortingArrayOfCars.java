
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SortingArrayOfCars {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Car[] cars = new Car[5];
        System.out.print("From which file do you want to load this information?");
        String filename = scan.nextLine();
        try {
            String path = Paths.get(new File("").getAbsolutePath(), "../", "PBD- Storing Data in a File", filename).normalize().toAbsolutePath().toString();
            Scanner in = new Scanner(new File(path));
            for (int i = 0; i < 5; i++) {
                String[] tokens = in.nextLine().split("\\s+");
                String make = tokens[0];
                String model = tokens[1];
                int year = Integer.parseInt(tokens[2]);
                String license = tokens[3];
                cars[i] = new Car(make, model, year, license);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SortingArrayOfCars.class.getName()).log(Level.SEVERE, null, ex);
        }

        int lowestYear = 0;
        int lowestIndex = -1;
        for (int i = 0; i < cars.length; i++) {
            lowestYear = cars[i].getYear();
            for (int j = i; j < cars.length; j++) {
                if (cars[j].getYear() < lowestYear) {
                    lowestYear = cars[j].getYear();
                    lowestIndex = j;
                }
            }
            if (lowestIndex > -1) {
                Car swap = cars[i];
                cars[i] = cars[lowestIndex];
                cars[lowestIndex] = swap;
            }
        }

        for (Car car : cars) {
            System.out.print(car.getMake() + " ");
            System.out.print(car.getModel() + " ");
            System.out.print(car.getYear() + " ");
            System.out.print(car.getLicense() + " ");
            System.out.println();
        }
    }
}
