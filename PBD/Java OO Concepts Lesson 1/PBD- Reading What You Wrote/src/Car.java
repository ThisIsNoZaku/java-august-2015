
public class Car {

    private final String make;
    private final String model;
    private final int year;
    private final String license;

    public Car(String make, String model, int year, String license) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.license = license;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public String getLicense() {
        return license;
    }
}
