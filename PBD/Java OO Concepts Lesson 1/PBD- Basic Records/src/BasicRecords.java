
import java.util.Scanner;

public class BasicRecords {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        StudentRecord first = new StudentRecord();
        System.out.print("Enter the first student's name: ");
        first.setName(scan.nextLine());
        System.out.print("Enter the first student's grade: ");
        first.setGrade(scan.nextInt());
        System.out.print("Enter the first student's average: ");
        first.setAverage(scan.nextDouble());
        scan.nextLine();

        StudentRecord second = new StudentRecord();
        System.out.print("Enter the second student's name: ");
        second.setName(scan.nextLine());
        System.out.print("Enter the second student's grade: ");
        second.setGrade(scan.nextInt());
        System.out.print("Enter the second student's average: ");
        second.setAverage(scan.nextDouble());
        scan.nextLine();

        StudentRecord third = new StudentRecord();
        System.out.print("Enter the third student's name: ");
        third.setName(scan.nextLine());
        System.out.print("Enter the third student's grade: ");
        third.setGrade(scan.nextInt());
        System.out.print("Enter the third student's average: ");
        third.setAverage(scan.nextDouble());

        System.out.println(String.format("The names are: %s %s %s", first.getName(), second.getName(), third.getName()));
        System.out.println(String.format("The names are: %s %s %s", first.getGrade(), second.getGrade(), third.getGrade()));
        System.out.println(String.format("The averages are: %s %s %s", first.getAverage(), second.getAverage(), third.getAverage()));

        System.out.println("The average for the three students is: " + (first.getAverage() + second.getAverage() + third.getAverage()) / 3);
    }
}
