
import java.util.Scanner;

public class ALittleDatabase {

    public static void main(String[] args) {
        StudentRecord[] records = new StudentRecord[3];
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < records.length; i++) {
            records[i] = new StudentRecord();

            System.out.print(String.format("Enter student %d's name: ", i + 1));
            records[i].setName(scan.nextLine());
            System.out.print(String.format("Enter student %d's grade: ", i + 1));
            records[i].setGrade(scan.nextInt());
            System.out.print(String.format("Enter student %d's average: ", i + 1));
            records[i].setAverage(scan.nextDouble());
            scan.nextLine();
        }

        System.out.println(String.format("The names are: %s %s %s", records[0].getName(), records[1].getName(), records[2].getName()));
        System.out.println(String.format("The grades are: %s %s %s", records[0].getGrade(), records[1].getGrade(), records[2].getGrade()));
        System.out.println(String.format("The averages are: %s %s %s", records[0].getAverage(), records[1].getAverage(), records[2].getAverage()));

        System.out.println("The average for the three students is: " + (records[0].getAverage() + records[1].getAverage() + records[2].getAverage()) / 3);
    }

}
