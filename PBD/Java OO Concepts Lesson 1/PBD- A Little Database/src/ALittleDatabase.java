
import java.util.Scanner;

public class ALittleDatabase {

    public static void main(String[] args) {
        StudentRecord[] records = new StudentRecord[3];
        Scanner scan = new Scanner(System.in);
        records[0] = new StudentRecord();
        System.out.print("Enter the first student's name: ");
        records[0].setName(scan.nextLine());
        System.out.print("Enter the first student's grade: ");
        records[0].setGrade(scan.nextInt());
        System.out.print("Enter the first student's average: ");
        records[0].setAverage(scan.nextDouble());
        scan.nextLine();

        records[1] = new StudentRecord();
        System.out.print("Enter the second student's name: ");
        records[1].setName(scan.nextLine());
        System.out.print("Enter the second student's grade: ");
        records[1].setGrade(scan.nextInt());
        System.out.print("Enter the second student's average: ");
        records[1].setAverage(scan.nextDouble());
        scan.nextLine();

        records[2] = new StudentRecord();
        System.out.print("Enter the third student's name: ");
        records[2].setName(scan.nextLine());
        System.out.print("Enter the third student's grade: ");
        records[2].setGrade(scan.nextInt());
        System.out.print("Enter the third student's average: ");
        records[2].setAverage(scan.nextDouble());

        System.out.println(String.format("The names are: %s %s %s", records[0].getName(), records[1].getName(), records[2].getName()));
        System.out.println(String.format("The grades are: %s %s %s", records[0].getGrade(), records[1].getGrade(), records[2].getGrade()));
        System.out.println(String.format("The averages are: %s %s %s", records[0].getAverage(), records[1].getAverage(), records[2].getAverage()));

        System.out.println("The average for the three students is: " + (records[0].getAverage() + records[1].getAverage() + records[2].getAverage()) / 3);
    }

}
