
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class GettingDataFromAFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        DogRecord pom = new DogRecord();
        DogRecord husky = new DogRecord();

        System.out.print("Which file to open: ");
        String filename = scan.nextLine();
        try {
            Scanner in = new Scanner(new File(filename));
            String[] tokens = in.nextLine().split("\\s*,\\s*");
            pom.setBreed(tokens[0]);
            pom.setAge(Integer.parseInt(tokens[1]));
            pom.setWeight(Double.parseDouble(tokens[2]));

            tokens = in.nextLine().split("\\s*,\\s*");
            husky.setBreed(tokens[0]);
            husky.setAge(Integer.parseInt(tokens[1]));
            husky.setWeight(Double.parseDouble(tokens[2]));
        } catch (IOException ex) {

        }
        System.out.println(String.format("First Dog: %s, %d, %.1f", pom.getBreed(), pom.getAge(), pom.getWeight()));
        System.out.println(String.format("Second Dog: %s, %d, %.1f", husky.getBreed(), husky.getAge(), husky.getWeight()));
    }

}
