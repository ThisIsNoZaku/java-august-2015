
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class StoringDataInAFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Car[] cars = new Car[5];
        for (int i = 0; i < cars.length; i++) {
            System.out.println("Car " + (i + 1));
            System.out.print("    Make: ");
            String make = scan.nextLine();
            System.out.print("    Model: ");
            String model = scan.nextLine();
            System.out.print("    Year: ");
            int year = scan.nextInt();
            scan.nextLine();
            System.out.print("    License: ");
            String license = scan.nextLine();
            System.out.println();
            
            cars[i] = new Car(make, model, year, license);
        }
        
        System.out.print("Save to what file? ");
        String filename = scan.nextLine();
        Writer out = null;
        try {
            out = new BufferedWriter(new FileWriter(new File(filename)));
            for (int i = 0; i < cars.length; i++) {
                out.write(cars[i].getMake() + " ");
                out.write(cars[i].getModel() + " ");
                out.write(cars[i].getYear() + " ");
                out.write(cars[i].getLicense() + " ");
                out.write(System.getProperty("line.separator"));
            }
            
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(StoringDataInAFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(StoringDataInAFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
