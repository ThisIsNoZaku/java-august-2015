/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Car {

    private final String make;
    private final String model;
    private final int year;
    private final String license;

    public Car(String make, String model, int year, String license) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.license = license;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public String getLicense() {
        return license;
    }
}
