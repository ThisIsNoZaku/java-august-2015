
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SortingRecordsOnTwoFields {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Open which file: ");
        String filename = scan.nextLine();
        StudentRecord[] records = new StudentRecord[30];

        Scanner in;
        try {
            in = new Scanner(new File(filename));
            int index = 0;
            while (in.hasNext()) {
                String[] tokens = in.nextLine().split("\\s+");
                records[index] = new StudentRecord();
                records[index].setId(Integer.parseInt(tokens[0]));
                records[index].setYear(Integer.parseInt(tokens[1]));
                records[index].setGrade(Double.parseDouble(tokens[2]));
                records[index].setLetterGrade(tokens[3]);
                index++;
            }

            for (int i = 0; i < records.length; i++) {
                int lowestIndex = i;
                for (int j = i; j < records.length; j++) {
                    if (records[j].compareTo(records[lowestIndex]) < 0) {
                        lowestIndex = j;
                    }
                }
                if (lowestIndex > i) {
                    StudentRecord swap = records[lowestIndex];
                    records[lowestIndex] = records[i];
                    records[i] = swap;
                }
            }

            System.out.println("Here are the sorted Grades");
            for (StudentRecord record : records) {
                System.out.println(String.format("  %d %d %.1f %s", record.getId(), record.getYear(), record.getGrade(), record.getLetterGrade()));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SortingRecordsOnTwoFields.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
