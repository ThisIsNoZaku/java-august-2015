
import java.net.URL;
import java.util.Scanner;

class Address {

    String street;
    String city;
    String state;
    int zip;

    @Override
    public String toString() {
        return street + ", " + city + ", " + state + "  " + zip;
    }
}

public class WebAddresses {

    public static void main(String[] args) throws Exception {
        URL addys = new URL("https://cs.leanderisd.org/txt/fake-addresses.txt");
        addys.openConnection().setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        Address[] addresses;
        try (Scanner fin = new Scanner(addys.openStream())) {
            addresses = new Address[6];
            int counter = 0;
            for (int i = 0; i < addresses.length; i++) {
                addresses[counter] = new Address();
                addresses[counter].street = fin.nextLine();
                addresses[counter].city = fin.nextLine();
                addresses[counter].state = fin.next();
                addresses[counter].zip = fin.nextInt();
                fin.skip("\n");
                counter++;
            }
        }
        for (int i = 0; i < addresses.length; i++) {
            Address address = addresses[i];
            System.out.println(address.toString());
        }
    }
}
