
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GettingMoreDataFromAFile {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Which file to open: ");
        String filename = scan.nextLine();
        PersonRecord[] records = new PersonRecord[5];

        try (Scanner in = new Scanner(new File(filename))) {
            for (int i = 0; i < records.length; i++) {
                String[] tokens = in.nextLine().split("\\s*,\\s*");
                records[i] = new PersonRecord();
                records[i].setName(tokens[0]);
                records[i].setAge(Integer.parseInt(tokens[1]));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GettingMoreDataFromAFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 0; i < records.length; i++) {
            System.out.println(String.format("%s is %d", records[i].getName(), records[i].getAge()));
        }
    }
}
