
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class TwentyQuestions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Think of an object, and I'll try to guess it.");
        System.out.println("Is it an animal, vegetable or mineral?");
        String type = scan.nextLine();
        System.out.println("Is it bigger than a breadbox?");
        String breadboxSize = scan.nextLine();
        String guess = "";
        if (type.equalsIgnoreCase("animal")) {
            if (breadboxSize.equalsIgnoreCase("yes")) {
                guess = "moose";
            } else {
                guess = "squirrel";
            }
        } else if (type.equalsIgnoreCase("vegetable")) {
            if (breadboxSize.equalsIgnoreCase("yes")) {
                guess = "watermelon";
            } else {
                guess = "carrot";
            }
        } else if (type.equalsIgnoreCase("mineral")) {
            if (breadboxSize.equalsIgnoreCase("yes")) {
                guess = "Camaro";
            } else {
                guess = "paper clip";
            }
        }

        System.out.println(String.format("My guess is that you are thinking of a %s.", guess));
        System.out.println("I would ask if I'm right but I don't actually care");
    }

}
