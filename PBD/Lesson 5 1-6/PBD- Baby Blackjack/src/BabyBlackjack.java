
import java.util.Random;

public class BabyBlackjack {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int handSize = 2;
        int[] playerHand = new int[handSize];
        int playerTotal = 0;
        int[] computerHand = new int[handSize];
        int computerTotal = 0;
        for (int i = 0; i < handSize; i++) {
            playerHand[i] = rollDice(1, 10);
            playerTotal += playerHand[i];
            computerHand[i] = rollDice(1, 10);
            computerTotal += computerHand[i];
        }
        System.out.println("You drew " + playerHand[0] + " and " + playerHand[1]);
        System.out.println("Your total is " + playerTotal);

        System.out.println("The dealer has " + computerHand[0] + " and " + computerHand[1]);
        System.out.println("Dealer's total is " + computerTotal);

        if (playerTotal > computerTotal) {
            System.out.println("YOU WIN");
        } else {
            System.out.println("DEALER WINS");
        }
    }

    public static int rollDice(int numberOfDice, int dieSize) {
        Random rand = new Random();
        int sum = 0;
        for (int i = 0; i < numberOfDice; i++) {
            sum += rand.nextInt(dieSize) + 1;
        }
        return sum;
    }
}
