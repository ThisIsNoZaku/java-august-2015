
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class NumberGuessingGame {

    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 10;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        int secret = MIN_VALUE + rand.nextInt(MAX_VALUE - MIN_VALUE);
        System.out.println(String.format("I'm thinking of a number between %d and %d. What is it?", MIN_VALUE, MAX_VALUE));
        int guess = scan.nextInt();

        if (guess == secret) {
            System.out.println("Good guess!");
        } else {
            System.out.println("Sorry, my number was " + secret + ".");
        }
    }
}
