
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDAlphabeticalOrder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("What is your last name?");
        String name = scan.nextLine();
        if (name.compareTo("Carswell") <= 0) {
            System.out.println("You don't have long to wait.");
        } else if (name.compareTo("Jones") <= 0) {
            System.out.println("That's not bad.");
        } else if (name.compareTo("Smith") <= 0) {
            System.out.println("Looks like a bit of a wait.");
        } else if (name.compareTo("Young") <= 0) {
            System.out.println("It's gonna be a while.");
        } else {
            System.out.println("Not going anywhere for a while?");
        }
    }

}
