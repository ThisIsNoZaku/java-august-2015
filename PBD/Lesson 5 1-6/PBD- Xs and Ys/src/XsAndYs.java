/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class XsAndYs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("x   y");
        System.out.println("------");
        for (double i = -10.0; i <= 10; i += .5) {
            System.out.println(String.format("%.2f  %.2f", i, Math.pow(i, 2)));
        }
    }

}
