
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class TwoMoreQuestions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Think of something and I'll guess it.");

        System.out.println("Question 1) Does it stay inside, outside or both?");
        String livesInside = scan.nextLine();
        System.out.println("Question 2) Is it alive?");
        String isAlive = scan.nextLine();

        if (livesInside.equalsIgnoreCase("inside") && isAlive.equalsIgnoreCase("alive")) {
            System.out.println("You're thinking of a house plant.");
        }
        if (livesInside.equalsIgnoreCase("inside") && isAlive.equalsIgnoreCase("not alive")) {
            System.out.println("That's a shower curtain.");
        }
        if (livesInside.equalsIgnoreCase("outside") && isAlive.equalsIgnoreCase("alive")) {
            System.out.println("Obviously you mean a bison.");
        }
        if (livesInside.equalsIgnoreCase("outside") && isAlive.equalsIgnoreCase("not alive")) {
            System.out.println("I guess a... billboard.");
        }
        if (livesInside.equalsIgnoreCase("both") && isAlive.equalsIgnoreCase("alive")) {
            System.out.println("You thinking about your pet dog, don't you?");
        }
        if (livesInside.equalsIgnoreCase("both") && isAlive.equalsIgnoreCase("not alive")) {
            System.out.println("It's your cellphone, right?");
        }
    }
}
