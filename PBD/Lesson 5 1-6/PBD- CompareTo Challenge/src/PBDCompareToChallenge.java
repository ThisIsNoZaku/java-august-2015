
/**
 *
 * @author apprentice
 */
public class PBDCompareToChallenge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        compareAndPrint("cat", "dog");
        compareAndPrint("car", "door");
        compareAndPrint("Burger King", "TGI Friday's");
        compareAndPrint("athlete's", "foot");
        compareAndPrint("alpha", "omega");
        
        compareAndPrint("con", "carne");
        compareAndPrint("hard", "charging");
        compareAndPrint("tit", "tat");
        compareAndPrint("zebra", "stripes");
        compareAndPrint("foot", "ball");
        
        compareAndPrint("can", "can");
        compareAndPrint("ti", "ti");
    }

    private static void compareAndPrint(String first, String second) {
        System.out.println(String.format("Comparing \"%s\" and \"%s\"", first, second));
        System.out.println(first.compareTo(second));
    }
}
