
import java.util.Scanner;

public class CountingMachine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Count to: ");
        int countTo = scan.nextInt();
        for (int i = 0; i <= countTo; i++) {
            System.out.print(i);
        }
    }

}
