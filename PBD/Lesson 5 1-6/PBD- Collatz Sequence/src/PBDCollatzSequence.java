
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class PBDCollatzSequence {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Starting number:");
        int number = scan.nextInt();
        int stepCount = 0;
        int highestValue = 0;
        int column = 1;
        while (number != 1) {
            if (number % 2 == 0) {
                number /= 2;
            } else {
                number = 3 * number + 1;
            }
            if (number > highestValue) {
                highestValue = number;
            }
            System.out.print(String.format("%-5d", number));
            column++;
            if ((column %= 10) == 0) {
                System.out.println();
                column = 1;
            }
            stepCount++;
        }
        System.out.println("Terminated after " + stepCount + " steps.");
    }
}
