
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgeMessage3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Your Name: ");
        String name = scan.nextLine();
        System.out.print("Your age: ");
        int age = scan.nextInt();
        if (age < 16) {
            System.out.println("You can't drive, " + name);
        }
        if (age >= 16 && age <= 17) {
            System.out.println("You can drive but not vote.");
        }
        if (age >= 18 && age <= 24) {
            System.out.println("You can vote but not rent a car.");
        }
        if (age >= 25) {
            System.out.println("You can do pretty much anything.");
        }
    }
}
