/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alittlequiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ALittleQuiz {

    private static final Scanner scan = new Scanner(System.in);
    private static final List<Question> questions = new ArrayList<>();

    static {
        Question.QuestionBuilder b = new Question.QuestionBuilder();
        b.setQuestion("What is the capital of Alaska?").addAnswer("Melbourne", false).addAnswer("Anchorage", true).addAnswer("Juneau", false).setWrongAnswerMessage("Anchorage is the capital of Alaska.");
        questions.add(b.build());

        b = new Question.QuestionBuilder();
        b.setQuestion("Can you store the value \"cat\" in a variable of type int?").addAnswer("Yes", false).addAnswer("No", true).setWrongAnswerMessage("String cannot be stored in a variable of type 'int'.");
        questions.add(b.build());

        b = new Question.QuestionBuilder();
        b.setQuestion("What is the result of 9+6/3?").addAnswer("5", false).addAnswer("11", true).addAnswer("15/3", false).setWrongAnswerMessage("9+6/3 = 9+2 = 11.");
        questions.add(b.build());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Would you like to take a quiz? y/n");
        String in = scan.nextLine();
        if (in.toLowerCase().equals("y")) {
            System.out.println("Great! Let's begin.");
        } else if (in.toLowerCase().equals("n")) {
            System.out.println("Too bad, we're doing it anyway.");
        }

        int correctAnswers = 0;
        int wrongAnswers = 0;

        int questionNumber = 1;
        for (Question q : questions) {
            Map<Integer, String> choices = new HashMap<>();
            System.out.println(String.format("Q%d) %s", questionNumber, q.question));
            int answerNumber = 1;
            for (String answer : q.getAnswerOptions().keySet()) {
                choices.put(answerNumber, answer);
                System.out.println(String.format("%d) %s", answerNumber, answer));
                answerNumber++;
            }
            int userChoice = Integer.parseInt(scan.nextLine());
            if (q.getAnswerOptions().get(choices.get(userChoice))) {
                System.out.println("That's right!");
                correctAnswers++;
            } else {
                System.out.println(q.getWrongAnswerMessage());
                wrongAnswers++;
            }
            questionNumber++;
            System.out.println();
            System.out.println();
        }

        System.out.println(String.format("Overall, you got %d out of %d correct.", correctAnswers, (correctAnswers + wrongAnswers)));
    }

    private static class Question {

        private final String question;
        private final Map<String, Boolean> answerOptions;
        private final String wrongAnswerMessage;

        private Question(String question, Map<String, Boolean> options, String wrongAnswerMessage) {
            this.question = question;
            int counter = 1;
            answerOptions = options;
            this.wrongAnswerMessage = wrongAnswerMessage;
        }

        public Map<String, Boolean> getAnswerOptions() {
            return Collections.unmodifiableMap(answerOptions);
        }

        public String getWrongAnswerMessage() {
            return wrongAnswerMessage;
        }

        public static class QuestionBuilder {

            private String question = "";
            private Map<String, Boolean> answers = new HashMap<>();
            private String wrongAnswerMessage = "";

            public QuestionBuilder setQuestion(String question) {
                this.question = question;
                return this;
            }

            public QuestionBuilder addAnswer(String answer, boolean isCorrect) {
                answers.put(answer, isCorrect);
                return this;
            }

            public QuestionBuilder setWrongAnswerMessage(String message) {
                this.wrongAnswerMessage = message;
                return this;
            }

            public Question build() {
                return new Question(question, answers, wrongAnswerMessage);
            }
        }
    }
}
