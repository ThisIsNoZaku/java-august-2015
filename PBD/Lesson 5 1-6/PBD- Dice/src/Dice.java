
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Dice {

    private static Random rand = new Random();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Here comes the dice!");

        int sum = 0;
        for (int i = 1; i <= 2; i++) {
            int roll = rollDice(1, 6);
            sum += roll;
            System.out.println(String.format("Roll %d: %d", i, roll));
        }
        System.out.println(String.format("The total is %d!", sum));
    }

    public static int rollDice(int numberOfDice, int dieSize) {
        int sum = 0;
        for (int i = 0; i < numberOfDice; i++) {
            sum += rand.nextInt(dieSize) + 1;
        }
        return sum;
    }

}
