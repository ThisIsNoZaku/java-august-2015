
import java.util.Scanner;

public class CountingMachine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Count from: ");
        int from = scan.nextInt();
        System.out.print("Count to: ");
        int to = scan.nextInt();
        System.out.print("Count by: ");
        int increment = scan.nextInt();

        for (int i = from; i <= to; i += increment) {
            System.out.print(i + " ");
        }
    }

}
