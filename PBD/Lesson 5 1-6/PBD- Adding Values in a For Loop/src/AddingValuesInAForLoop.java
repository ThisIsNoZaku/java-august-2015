
import java.util.Scanner;

public class AddingValuesInAForLoop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Number: ");
        int in = scan.nextInt();
        int sum = 0;
        for (int i = 1; i <= in; i++) {
            System.out.print(i + " ");
            sum += i;
        }
        System.out.println("The sum is " + sum);
    }

}
