
import java.util.Scanner;

public class Nim {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] piles = new int[3];
        for (int i = 0; i < 3; i++) {
            piles[i] = 3;
        }
        String[] playerNames = new String[2];
        System.out.println("Player 1, enter your name: ");
        playerNames[0] = scan.nextLine();
        System.out.println("Player 2, etner your name: ");
        playerNames[1] = scan.nextLine();
        int currentPlayer = 0;

        while (piles[0] != 0 || piles[1] != 0 || piles[2] != 0) {
            System.out.println(String.format("A:%d B:%d C:%d", piles[0], piles[1], piles[2]));
            System.out.print(playerNames[currentPlayer] + ", Choose a pile: ");
            String pile = scan.nextLine();
            int pileIndex = -1;
            switch (pile.toLowerCase()) {
                case "a":
                    pileIndex = 0;
                    break;
                case "b":
                    pileIndex = 1;
                    break;
                case "c":
                    pileIndex = 2;
                    break;
            }

            if (pileIndex < 0 || pileIndex > 2) {
                System.out.println("You have to pick an actual pile.");
                continue;
            }
            if (piles[pileIndex] == 0) {
                System.out.println("Sorry, that pile is already empty. Try again.");
                continue;
            }
            System.out.print("Choose how many to remove: ");
            int amount = scan.nextInt();
            scan.nextLine();
            if (amount < 1 || amount > piles[pileIndex]) {
                System.out.println("You must choose between 1 and " + piles[pileIndex] + ".");
                continue;
            }
            piles[pileIndex] -= amount;
            currentPlayer = ++currentPlayer % 2;
        }

        System.out.println(String.format("%s, there are no counters left, you win!", playerNames[currentPlayer]));
    }
}
