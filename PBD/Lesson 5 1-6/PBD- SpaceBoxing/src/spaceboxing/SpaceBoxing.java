/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceboxing;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 *
 * @author apprentice
 */
public class SpaceBoxing {

    private static final Map<Integer, String> planetNames = new HashMap<>();

    static {
        planetNames.put(1, "Venus");
        planetNames.put(2, "Mars");
        planetNames.put(3, "Jupiter");
        planetNames.put(4, "Saturn");
        planetNames.put(5, "Uranus");
        planetNames.put(6, "Neptune");
    }
    private static final Map<Integer, Double> planetGravity = new HashMap<>();

    static {
        planetGravity.put(1, .9);
        planetGravity.put(2, .37);
        planetGravity.put(3, 2.5);
        planetGravity.put(4, 1.06);
        planetGravity.put(5, .88);
        planetGravity.put(6, 1.14);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your weight on Earth:");
        double weight = sc.nextDouble();
        sc.nextLine();
        System.out.println();
        System.out.println("Which planet are you travelling to?");
        IntStream.range(1, planetNames.size()).forEach(i -> {
            System.out.print(String.format("%d. %s", i, planetNames.get(i)));
            System.out.print("   ");
            if (i % 3 == 0) {
                System.out.println();
            }
        });
        System.out.println();
        int planetSelection = sc.nextInt();
        sc.nextLine();

        weight *= planetGravity.get(planetSelection);
        System.out.println(String.format("Your weight on %s will be %.2f", planetNames.get(planetSelection), weight));
    }

}
