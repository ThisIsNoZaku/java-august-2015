
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDRightTriangleChecker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter three integer in ascending order.");
        int[] sideLengths = new int[3];
        int sides = 0;
        do {
            System.out.print(String.format("Side %d: ", sides + 1));
            int in = scan.nextInt();
            sideLengths[sides] = in;
            if (sides != 0 && sideLengths[sides - 1] > sideLengths[sides]) {
                System.out.println(String.format("%d is smaller than %d. Try again.", sideLengths[sides], sideLengths[sides - 1]));
                continue;
            }
            sides++;
        } while (sides != 3);
        System.out.println(String.format("Your sides are %d %d %d", sideLengths[0], sideLengths[1], sideLengths[2]));
        double difference = (Math.pow(sideLengths[0], 2) + Math.pow(sideLengths[1], 2)) - Math.pow(sideLengths[2], 2);
        if (Math.abs(difference) <= 0.001) {
            System.out.println("These sides DO make a right triangle.");
        } else {
            System.out.println("These sides DO NOT make a right triangle.");
        }
    }
}
