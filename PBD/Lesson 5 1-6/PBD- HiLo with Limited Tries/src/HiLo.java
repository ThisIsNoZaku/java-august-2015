
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HiLo {

    private static int MAX_GUESSES = 7;
    public static Random rand = new Random();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("I'm guess a number between 1 and 100. Can you guess it?");
        int random = rand.nextInt(100) + 1;
        int guess = scan.nextInt();
        int guessCount = 1;
        while (guess != random && guessCount < MAX_GUESSES) {
            if (guess > random) {
                System.out.println("Sorry, the number is lower than that");
            } else {
                System.out.println("Sorry, the number is higher than that");
            }
            System.out.println("Guess again.");
            guess = scan.nextInt();
            guessCount++;
        }
        System.out.println("Wow, you got it!");
        System.out.println("It took you " + guessCount + " tries.");
    }
}
