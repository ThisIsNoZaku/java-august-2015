/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weekdayname;

import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WeekdayName {

    public static String weekday_name(int in) {
        String weekDay = "";
        if (in == 1) {
            weekDay = "Monday";
        } else if (in == 2) {
            weekDay = "Tuesday";
        } else if (in == 3) {
            weekDay = "Wednesday";
        } else if (in == 4) {
            weekDay = "Thursday";
        } else if (in == 5) {
            weekDay = "Friday";
        } else if (in == 6) {
            weekDay = "Saturday";
        } else if (in == 7) {
            weekDay = "Sunday";
        } else if (in == 0) {
            weekDay = "Sunday";
        } else {
            weekDay = "error";
        }
        return weekDay;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Weekday 1: " + weekday_name(1));
        System.out.println("Weekday 2: " + weekday_name(2));
        System.out.println("Weekday 3: " + weekday_name(3));
        System.out.println("Weekday 4: " + weekday_name(4));
        System.out.println("Weekday 5: " + weekday_name(5));
        System.out.println("Weekday 6: " + weekday_name(6));
        System.out.println("Weekday 7: " + weekday_name(7));
        System.out.println("Weekday 0: " + weekday_name(0));
        System.out.println();
        System.out.println("Weekday 43: " + weekday_name(43));
        System.out.println("Weekday 17: " + weekday_name(17));
        System.out.println("Weekday -1: " + weekday_name(-1));

        GregorianCalendar cal = new GregorianCalendar();
        int dow = cal.get(GregorianCalendar.DAY_OF_WEEK);

        System.out.println("\nToday is a " + weekday_name(dow) + "!");
    }
}
