
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDGenderGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("What is your gender? (M or F)");
        String gender = scan.nextLine();
        System.out.println("What is your first name?");
        String firstName = scan.nextLine();
        System.out.println("What is your last name");
        String lastName = scan.nextLine();
        System.out.println("What is your age?");
        int age = scan.nextInt();
        boolean married = false;
        if (gender.equalsIgnoreCase("f") && age >= 20) {
            System.out.println("Are you married? (Y or N)");
            if (scan.nextLine().equalsIgnoreCase("y")) {
                married = true;
            }
        }
        String title = "";
        if (gender.equalsIgnoreCase("f")) {
            if (married) {
                title = "Mrs.";
            } else {
                title = "Ms.";
            }
        } else {
            if (age >= 20) {
                title = "Mr.";
            }
        }

        System.out.println(String.format("Then I shall call you %s %s %s", title, firstName, lastName));
    }

}
