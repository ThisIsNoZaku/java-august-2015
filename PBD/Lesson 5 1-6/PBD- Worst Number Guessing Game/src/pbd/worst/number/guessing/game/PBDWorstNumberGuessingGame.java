/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd.worst.number.guessing.game;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDWorstNumberGuessingGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int secret = 4;
        System.out.println("I'm guessing a random number 1 to 10.");
        int guess = scan.nextInt();
        if (secret == guess) {
            System.out.println("Good guess.");
        } else {
            System.out.println("Nah.");
        }
    }
}
