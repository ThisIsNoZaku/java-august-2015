
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDSafeSquareRoot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number;
        do {
            System.out.println("Enter a number to get the square root:");
            number = scan.nextInt();
            if (number < 0) {
                System.out.println("You must enter a positive number.");
            }
        } while (number < 0);
        System.out.println(String.format("The square root of %d is %.2f", number, Math.sqrt(number)));
    }

}
