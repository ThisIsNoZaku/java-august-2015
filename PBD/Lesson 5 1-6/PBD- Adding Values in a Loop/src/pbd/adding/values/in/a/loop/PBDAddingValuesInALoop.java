/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd.adding.values.in.a.loop;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBDAddingValuesInALoop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("I will add the numbers you give me");
        int in = -1;

        int sum = 0;
        while (in != 0) {
            System.out.println(String.format("The total so far is %d", sum));
            System.out.print("Number: ");
            in = scan.nextInt();
            sum += in;
        }
        System.out.println(String.format("The total is %d.", sum));
    }
}
