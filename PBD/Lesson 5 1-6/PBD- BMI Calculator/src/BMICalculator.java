
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BMICalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Your height in m: ");
        double height = scan.nextDouble();
        System.out.print("Your weight in kg: ");
        double weight = scan.nextDouble();
        double bmi = weight * Math.sqrt(height);
        String category = "";
        if (bmi < 15.0) {
            category = "Very severely underweight";
        } else if (bmi <= 16.0) {
            category = "Severely underweight";
        } else if (bmi <= 18.4) {
            category = "Underweight";
        } else if (bmi <= 24.9) {
            category = "Normal weight";
        } else if (bmi <= 29.9) {
            category = "Overweight";
        } else if (bmi <= 34.9) {
            category = "Moderately obese";
        } else if (bmi <= 39.9) {
            category = "Severely obese";
        } else if (bmi >= 40.0) {
            category = "Morbidly obese";
        }
    }

}
