
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HowOldAreYouExactly {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Hey, what is your name?");
        String name = sc.nextLine();

        System.out.println(String.format("Okay, %s, how old are you?", name));
        int age = sc.nextInt();
        sc.nextLine();
        if (age < 16) {
            System.out.println(String.format("You can't drive, %s.", name));
        } else if (age < 18) {
            System.out.println(String.format("You can't vote, %s.", name));
        } else if (age < 25) {
            System.out.println(String.format("You can't rent a car, %s", name));
        } else {
            System.out.println("You can do anything that's legal.");
        }
    }
}
