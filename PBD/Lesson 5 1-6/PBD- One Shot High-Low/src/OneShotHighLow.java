
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class OneShotHighLow {

    public static Random rand = new Random();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("I'm guess a number between 1 and 100. Can you guess it?");
        int random = rand.nextInt(100) + 1;
        int guess = scan.nextInt();
        if (guess == random) {
            System.out.println("Wow, you got it!");
        } else if (guess > random) {
            System.out.println("Sorry, the number is lower than that, " + random);
        } else {
            System.out.println("Sorry, your guess is too low, the number is " + random);
        }
    }
}
