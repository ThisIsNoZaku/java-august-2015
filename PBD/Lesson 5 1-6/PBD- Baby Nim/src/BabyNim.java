
import java.util.Scanner;

public class BabyNim {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] piles = new int[3];
        for (int i = 0; i < 3; i++) {
            piles[i] = 3;
        }
        String in;
        while (piles[0] != 0 || piles[1] != 0 || piles[2] != 0) {
            System.out.println(String.format("A:%d B:%d C:%d", piles[0], piles[1], piles[2]));
            System.out.print("Choose a pile: ");
            String pile = scan.nextLine();
            System.out.print("Choose how many to remove: ");
            int amount = scan.nextInt();
            scan.nextLine();
            switch (pile.toLowerCase()) {
                case "a":
                    piles[0] -= amount;
                    break;
                case "b":
                    piles[1] -= amount;
                    break;
                case "c":
                    piles[2] -= amount;
                    break;
            }
        }
    }
}
