
import java.util.Scanner;

public class ChooseYourOwnAdventure {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String nextLocation = "start";
        while (nextLocation.equalsIgnoreCase("start")) {
            System.out.println("You are in a creepy house! Would you like to go \"upstairs\" or into the \"kitchen\"?");
            nextLocation = scan.nextLine();
            while (nextLocation.equalsIgnoreCase("kitchen")) {
                System.out.println("There is a long countertop with dirty dishes everywhere. Off to one side there is, as you expected, a refrigerator. You may open the \"refrigerator\" or look in a \"cabinet\".");
                nextLocation = scan.nextLine();
                while (nextLocation.equalsIgnoreCase("refrigerator")) {
                    System.out.println("Inside the refrigerator you see food and stuff. It looks pretty nasty. Would you like to eat some of the food or go back to the \"kitchen\"? (\"yes\" or \"no\")");
                    nextLocation = scan.nextLine();
                    if (nextLocation.equalsIgnoreCase("yes")) {
                        System.out.println("You get food poisoning and die.");
                        break;
                    }
                    if (nextLocation.equalsIgnoreCase("no")) {
                        System.out.println("You die of starvation... eventually.");
                        break;
                    }
                }
                while (nextLocation.equalsIgnoreCase("cabinets")) {
                    System.out.println("Inside the cabinets you only find dishes and bleach. Use the \"plates\" or \"bleach\"or back to the \"kitchen\"?");
                    nextLocation = scan.nextLine();
                    if (nextLocation.equalsIgnoreCase("plates")) {
                        System.out.println("You burn the plates for heat, but the toxic chemicals released poisons and you suffocate.");
                        break;
                    } else if (nextLocation.equalsIgnoreCase("bleach")) {
                        System.out.println("You decide to use the bleach to clean and disinfect the kitchen. However, you get so tired that you lie down to take a nap but then a meteor strike destroys the earth.");
                        break;
                    }
                }
            }
            while (nextLocation.equalsIgnoreCase("upstairs")) {
                System.out.println("Upstairs you see a hallway. At the end of the hallway is a master \"bedroom\". There is also a \"bathroom\" off the main hallway. You can also go back downstairs to the \"start\". Where would you like to go?");
                nextLocation = scan.nextLine();
                while (nextLocation.equalsIgnoreCase("bedroom")) {
                    System.out.println("You are in a plush bedroom, with expensive-looking hardwood furniture. The bed is unmade. In the back of the room, the closet door is ajar. Would you like to open the door or go back into the \"upstairs\" hallway? (\"yes\" or \"no\")");
                    nextLocation = scan.nextLine();
                    if (nextLocation.equalsIgnoreCase("yes")) {
                        System.out.println("There's a treasure chest, you're rich now. Hooray.");
                        break;
                    } else {
                        System.out.println("You aren't interested in exploring any more so you go home.");
                        break;
                    }
                }
                while (nextLocation.equalsIgnoreCase("bathroom")) {
                    System.out.println("The bathroom is large, with a freestanding tub. Do you want to take a \"bath\" or look in the medicine \"cabinet\"? Perhaps go back into the \"upstairs\" hall?");
                    nextLocation = scan.nextLine();
                    if (nextLocation.equalsIgnoreCase("bath")) {
                        System.out.println("It turns out the bath tub is absoultely filled with gelatin. You taste it, but it's sugar free and gross.");
                        break;
                    } else if (nextLocation.equalsIgnoreCase("cabinet")) {
                        System.out.println("There's nothing interesting.");
                        break;
                    }
                }
            }
        }
    }
}
