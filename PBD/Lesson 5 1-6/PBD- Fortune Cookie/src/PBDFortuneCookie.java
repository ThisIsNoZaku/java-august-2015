
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class PBDFortuneCookie {

    private static final Map<Integer, String> fortunes = new HashMap<>();

    static {
        fortunes.put(0, "Hope you didn't accidentally lock the bathroom at home.");
        fortunes.put(1, "Good things come to those who lift weights.");
        fortunes.put(2, "The early bird gets worms.");
        fortunes.put(3, "One in the hand is worth planting a bush.");
        fortunes.put(4, "Don't count your chickens with a hatchet.");
        fortunes.put(5, "You can lead a horse to water but you can't ride while it's drunk.");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rand = new Random();
        System.out.println(String.format("Fortune Cookies says: %s", fortunes.get(rand.nextInt(6))));
    }

}
