
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ThreeCardMonte {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int acePosition = random.nextInt(3) + 1;
        System.out.println("Can you guess which of the three cards is the ace?");
        System.out.println("###   ###   ###");
        System.out.println("###   ###   ###");
        System.out.println("###   ###   ###");
        System.out.println(" 1     2     3 ");
        int guess = scan.nextInt();

        switch (acePosition) {
            case 1:
                System.out.println("A##   ###   ###");
                System.out.println("###   ###   ###");
                System.out.println("##A   ###   ###");
                System.out.println(" 1     2     3 ");
                break;
            case 2:
                System.out.println("###   A##   ###");
                System.out.println("###   ###   ###");
                System.out.println("###   ##A   ###");
                System.out.println(" 1     2     3 ");
                break;
            case 3:
                System.out.println("###   ###   A##");
                System.out.println("###   ###   ###");
                System.out.println("###   ###   ##A");
                System.out.println(" 1     2     3 ");
                break;
        }
        
        if (guess == acePosition) {
            System.out.println("Great guess!");
        } else {
            System.out.println("Better luck next time.");
        }
    }

}
