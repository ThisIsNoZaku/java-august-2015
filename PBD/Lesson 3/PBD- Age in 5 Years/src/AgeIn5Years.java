
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class AgeIn5Years {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Hello. What is your name? ");
        String name = scan.nextLine();
        System.out.println(String.format("Hi, %s! How old are you?", name));
        int age = scan.nextInt();
        System.out.println(String.format("Did you know that in 5 years you will be %d?", age+5));
        System.out.println(String.format("And 5 years ago, you were %d! Imagine that!", age-5));
    }
    
}
