
/**
 *
 * @author apprentice
 */
public class PBDYourSchedule {

    private String member;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] classes = new String[8];
        {
            classes[0] = "English III";
            classes[1] = "Precalculus";
            classes[2] = "Music Theory";
            classes[3] = "Biotechnology";
            classes[4] = "Principles of Technology I";
            classes[5] = "Latin II";
            classes[6] = "AP US History";
            classes[7] = "Business Computer Information Systems";
        }
        String[] teachers = new String[8];
        {
            teachers[0] = "Ms. Lapan";
            teachers[1] = "Mrs. Gideon";
            teachers[2] = "Mr. Davis";
            teachers[3] = "Ms. Palmer";
            teachers[4] = "Ms. Garcia";
            teachers[5] = "Mrs. Barnett";
            teachers[6] = "Ms. Johannessen";
            teachers[7] = "Mr. James";
        }
        int longestStringLength = 0;
        for (String s : classes) {
            if (s.length() > longestStringLength) {
                longestStringLength = s.length();
            }
        }
        for (String s : teachers) {
            if (s.length() > longestStringLength) {
                longestStringLength = s.length();
            }
        }

        System.out.println("+-----------------------------------------------------------------------------+");
        for (int i = 0; i < classes.length; i++) {
            System.out.println(String.format("|%d|%2$" + longestStringLength + "s|%3$" + longestStringLength + "s|", i, classes[i], teachers[i]));
        }
        System.out.println("+-----------------------------------------------------------------------------+");
    }
}
