
import java.util.Scanner;

public class AskingQuestions {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int age;
        int feet;
        int inches;
        double weight;

        System.out.print("How old are you? ");
        age = keyboard.nextInt();
        keyboard.nextLine();

        System.out.print("How many feet tall are you? ");
        feet = keyboard.nextInt();
        keyboard.nextLine();

        System.out.println("How many inches tall are you? ");
        inches = keyboard.nextInt();
        keyboard.nextLine();

        System.out.print("How much do you weigh? ");
        weight = keyboard.nextDouble();
        keyboard.nextLine();

        System.out.println("So you're " + age + " old, " + feet + "'" + inches + "\" tall and " + weight + " heavy.");
    }
}
