
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class PBDNameAgeAndSalary {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Hello. What is your name?");
        String name = scan.nextLine();
        System.out.println(String.format("Hi %s! How old are you?", name));
        int age = scan.nextInt();
        scan.nextLine();
        System.out.println(String.format("So you're %d, eh? That's not old at all!", age));
        System.out.println(String.format("How much do you make, %s?", name));
        double wage = scan.nextDouble();
        scan.nextLine();
        System.out.println(String.format("%.2f! I hope that's per hour and not per year! LOL!", wage));
    }

}
