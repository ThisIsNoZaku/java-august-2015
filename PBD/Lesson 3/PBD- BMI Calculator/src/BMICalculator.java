
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BMICalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Your height in m: ");
        double height = Double.parseDouble(scan.nextLine());
        System.out.println("Your weight in kg: ");
        double weight = Double.parseDouble(scan.nextLine());

        double bmi = weight / Math.sqrt(height);
        System.out.println(String.format("Your BMI is %f", bmi));
    }

}
