
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ADumbCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("What is your first number?");
        double first = scan.nextDouble();
        scan.nextLine();
        System.out.println("What is your second number?");
        double second = scan.nextDouble();
        scan.nextLine();
        System.out.println("What is your third number?");
        double third = scan.nextDouble();

        System.out.println(String.format("(%f + %f + %f) /2 is ... %f", first, second, third, (first + second + third) / 2));
    }

}
