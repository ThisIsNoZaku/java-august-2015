
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class PBDMoreUserInputOfData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Please enter the following information so I can sell it for a profit!");
        System.out.println();

        Scanner scan = new Scanner(System.in);
        System.out.print("First Name: ");
        String firstName = scan.nextLine();
        System.out.print("Last Name: ");
        String lastName = scan.nextLine();
        System.out.print("Grade (9-12): ");
        int grade = scan.nextInt();
        scan.nextLine();
        System.out.print("Student Id: ");
        int id = scan.nextInt();
        scan.nextLine();
        System.out.print("Login: ");
        String login = scan.nextLine();
        System.out.print("GPA: ");
        double gpa = scan.nextDouble();

        System.out.println("Your information:");
        System.out.println(String.format("%-8s: %s", "Login:", login));
        System.out.println(String.format("%-8s: %d", "ID:", id));
        System.out.println(String.format("%-8s: %s", "Name:", lastName + ", " + firstName));
        System.out.println(String.format("%-8s: %s", "GPA:", gpa));
        System.out.println(String.format("%-8s: %d", "Grade:", grade));
    }
}
