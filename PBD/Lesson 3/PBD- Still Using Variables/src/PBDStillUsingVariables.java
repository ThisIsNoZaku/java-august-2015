
/**
 *
 * @author apprentice
 */
public class PBDStillUsingVariables {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name = "Damien Marble";
        int graduationYear = 2013;

        System.out.println(String.format("My name is %s and I graduated in %d", name, graduationYear));
    }

}
