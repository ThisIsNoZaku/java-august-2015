
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SummingSeveralNumbersFromAnyFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Which file would you like to reaad numbers from: ");
        String fileName = scan.nextLine();

        System.out.println("Reading from \"" + fileName + "\"");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(new File(fileName)));
            int sum = 0;
            String input = "";
            while ((input = in.readLine()) != null && input.length() != 0) {
                System.out.print(input + " ");
                sum += Integer.parseInt(input);
            }
            System.out.println();
            System.out.println("Total is " + sum);
        } catch (IOException ex) {

        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(SummingSeveralNumbersFromAnyFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
