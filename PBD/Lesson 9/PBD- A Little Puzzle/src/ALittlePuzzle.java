
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ALittlePuzzle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Open which file: ");
        String fileName = scan.nextLine();
        Scanner in = null;
        try {
            in = new Scanner(new FileReader(new File(fileName)));
            String input = in.nextLine();
            for (int i = 0; i < input.length(); i++) {
                if ((i + 1) % 3 == 0) {
                    System.out.print(input.charAt(i));
                }
            }
        } catch (IOException ex) {

        } finally {
            in.close();
        }
    }

}
