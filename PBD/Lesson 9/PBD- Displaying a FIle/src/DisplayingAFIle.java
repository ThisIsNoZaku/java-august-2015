
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.jfr.events.FileReadEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class DisplayingAFIle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a file to read.");
        String fileName = scan.nextLine();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(new File(fileName)));
            String input = "";
            while ((input = in.readLine()) != null && input.length() != 0) {
                System.out.println(input);
            }
        } catch (IOException ex) {
            Logger.getLogger(DisplayingAFIle.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(DisplayingAFIle.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
