
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SummingThreeNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader in = null;
        int[] values = new int[3];
        System.out.print("Reading numbers from file \"3nums.txt\"... ");
        try {

            for (int i = 0; i < 3; i++) {
                in = new BufferedReader(new FileReader("3nums" + (i + 1) + ".txt"));
                values[i] = Integer.parseInt(in.readLine());
            }
            System.out.println("Done.");
        } catch (IOException ex) {
            Logger.getLogger(SummingThreeNumbers.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(SummingThreeNumbers.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        System.out.println(String.format("%d + %d + %d = %d", values[0], values[1], values[2], values[0] + values[1] + values[2]));
    }

}
