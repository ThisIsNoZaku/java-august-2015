/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd.letter.revisited;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 *
 * @author apprentice
 */
public class PBDLetterRevisited {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(new File("Letter.txt"));
        out.println("+----------------------------------------+");
        out.println("|                                     ###|");
        out.println("|                                     ###|");
        out.println("|                                     ###|");
        out.println("|                                        |");
        out.println("|                      Damien Marble     |");
        out.println("|                      401 S Main St     |");
        out.println("|                      Akron, Ohio 411322|");
        out.println("|                                        |");
        out.println("+----------------------------------------+");
    }

}
