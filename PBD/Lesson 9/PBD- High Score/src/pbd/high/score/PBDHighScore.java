/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd.high.score;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class PBDHighScore {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("You got a high score!");

        System.out.print("Please enter your score: ");
        int score = scan.nextInt();
        System.out.print("Please enter your name: ");
        String name = scan.nextLine();
        PrintWriter out;
        try {
            out = new PrintWriter(new File("score.txt"));
            out.print(name);
            out.print(score);
            out.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PBDHighScore.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
        System.out.println("Data stored into score.txt");
    }

}
