
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VowelCapitalization {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Open which file: ");
        String fileName = scan.nextLine();

        System.out.println();
        Scanner in = null;
        try {
            in = new Scanner(new File(fileName));
            String input;
            while (in.hasNext()) {
                input = in.nextLine();
                input = input.replace("a", "A");
                input = input.replace("e", "E");
                input = input.replace("i", "I");
                input = input.replace("o", "O");
                input = input.replace("u", "U");
                System.out.println(input);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VowelCapitalization.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            in.close();
        }
    }
}
