
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SimpleFileInput {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader reader = null;
        String name = "";
        try {
            File f = Paths.get("names.txt").toFile();
            System.out.println(Paths.get("").toAbsolutePath().toString());
            reader = new BufferedReader(new FileReader(new File("name.txt")));
            name = reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(SimpleFileInput.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(SimpleFileInput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println(name);
    }
}
