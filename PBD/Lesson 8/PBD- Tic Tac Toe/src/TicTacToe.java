
import java.util.Scanner;

public class TicTacToe {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char[][] gameboard = new char[3][3];

        char[] players = new char[]{
            'x', 'o'
        };
        int currentPlayer = 0;

        for (int i = 0; i < 9; i++) {
            gameboard[(int) Math.ceil(i / 3)][i % 3] = ((i + 1) + "").charAt(0);
        }
        while (determineWinner(gameboard) == ' ') {
            printBoard(gameboard);
            System.out.println("");
            System.out.print("Player " + players[currentPlayer] + ", choose a space: ");
            int selectedPosition = scan.nextInt();
            if (selectedPosition < 1 || selectedPosition > 9) {
                System.out.println("Select an unoccupied space between 1 and 9.");
                continue;
            }

            int[] position = positionToCoordinate(selectedPosition);

            if (gameboard[position[0]][position[1]] != 'x' && gameboard[position[0]][position[1]] != 'o') {
                gameboard[position[0]][position[1]] = players[currentPlayer];
                currentPlayer = (++currentPlayer) % 2;
            } else {
                System.out.println("That space is already take.");
            }
        }
        System.out.println("Player " + players[(++currentPlayer) % 2] + " won!");
    }

    private static void printBoard(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                System.out.print(board[row][column] + " ");
            }
            System.out.println();
        }
    }

    private static char determineWinner(char[][] board) {
        char winner = ' ';
        for (int i = 0; i < 3; i++) {
            if ((board[i][0] == board[i][1] && board[i][1] == board[i][2]) || (board[0][i] == board[1][i] && board[1][i] == board[2][i])) {
                winner = board[i][0];
            }
        }

        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2]) || (board[0][2] == board[1][1] && board[1][1] == board[0][2])) {
            winner = board[1][1];
        }

        return winner;
    }

    private static int[] positionToCoordinate(int position) {
        position -= 1;
        return new int[]{
            (int) Math.floor(position / 3), position % 3
        };
    }

}
