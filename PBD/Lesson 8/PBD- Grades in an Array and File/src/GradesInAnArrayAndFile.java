
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class GradesInAnArrayAndFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        System.out.print("Name (first last): ");
        String name = scan.nextLine();
        System.out.print("Filename: ");
        String filename = scan.nextLine();
        int[] grades = new int[5];
        Random rand = new Random();
        
        System.out.println("Here are your randomly-selected grades:");
        for (int i = 0; i < grades.length; i++) {
            grades[i] = rand.nextInt(100) + 1;
            System.out.println(String.format("Grade %d: %d", i, grades[i]));
        }
        PrintWriter out = new PrintWriter(new File(filename));
        try {
            out.println(name);
            for (int i = 0; i < grades.length; i++) {
                out.print(grades[i] + " ");
            }
            out.flush();
        } finally {
            out.close();
        };
    }
}
