/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd.basic.arrays.pkg3;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class PBDBasicArrays3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rand = new Random();
        int[] array = new int[1000];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(90) + 10;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(String.format("%-2d  ", array[i]));
            if ((i + 1) % 20 == 0) {
                System.out.println();
            }
        }
    }

}
