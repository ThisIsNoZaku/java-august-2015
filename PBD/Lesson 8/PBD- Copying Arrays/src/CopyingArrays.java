
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CopyingArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] array = new int[10];
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(100) + 1;
        }

        int[] second = new int[array.length];
        for (int i = 0; i < second.length; i++) {
            second[i] = array[i];
        }
        array[array.length - 1] = -7;
        System.out.print("Array 1: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(String.format("%-2d ", array[i]));
        }

        System.out.print("Array 2: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(String.format("%-2d ", second[i]));
        }
    }

}
