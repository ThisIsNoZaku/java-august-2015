
public class SortingValues {

    public static void main(String[] args) {
        int[] arr = {45, 87, 39, 32, 93, 86, 12, 44, 75, 50};

        // Display the original (unsorted values)
        System.out.print("before: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < arr.length; i++) {
            int smallestIndex = -1;
            int smallestValue = arr[i];
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < smallestValue) {
                    smallestValue = arr[j];
                    smallestIndex = j;
                }
            }
            if (smallestIndex > -1) {
                int swap = arr[i];
                arr[i] = arr[smallestIndex];
                arr[smallestIndex] = swap;
            }
        }
		// Swap the values around to put them ascending order.
		/*
         for ( ; ; )
         {
         for ( ; ; )
         {
         if ( )
         {
         // swap the values in two slots
         }
         }
         }
         */
        // Display the values again, now (hopefully) sorted.
        System.out.print("after : ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
