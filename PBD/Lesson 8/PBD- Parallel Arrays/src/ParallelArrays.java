
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ParallelArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] names = new String[]{
            "Adam", "Bob", "Charlie", "Doug", "Elmer"
        };
        double[] grades = new double[]{
            10, 20, 30, 40, 50
        };
        int[] ids = new int[]{
            5367, 7574, 8456, 2375, 8879
        };
        for (int i = 0; i < 5; i++) {
            System.out.println(String.format("%s %.2f %d", names[i], grades[i], ids[i]));
        }

        System.out.print("ID number to find: ");
        Scanner scan = new Scanner(System.in);
        int searchId = scan.nextInt();
        for (int i = 0; i < 5; i++) {
            if (ids[i] == searchId) {
                System.out.println("Found in slot " + i);
                System.out.println(String.format("      Name: %s", names[i]));
                System.out.println(String.format("      Average: %f", grades[i]));
                System.out.println(String.format("      ID: %d", ids[i]));
            }
        }
    }
}
