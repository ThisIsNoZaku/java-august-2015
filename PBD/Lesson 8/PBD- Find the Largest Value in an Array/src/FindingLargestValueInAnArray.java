
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class FindingLargestValueInAnArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] data = new int[10];
        Random rand = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = rand.nextInt(50) + 1;
        }

        System.out.print("Array: ");
        for (int i = 0; i < data.length; i++) {
            System.out.print(String.format("%-2d ", data[i]));
        }
        System.out.println();

        System.out.print("Value to find: ");
        int find = scan.nextInt();
        int largest = 0;

        for (int i = 0; i < data.length; i++) {
            if (data[i] > largest) {
                largest = data[i];
            }
        }

        System.out.println("The largest value is " + largest);
    }

}
