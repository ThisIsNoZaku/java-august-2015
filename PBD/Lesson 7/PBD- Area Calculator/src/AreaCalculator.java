
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AreaCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int selection = 0;
        double area = 0.0;
        while (selection != 5) {
            System.out.println("1) Triangle");
            System.out.println("2) Rectangle");
            System.out.println("3) Square");
            System.out.println("4) Circle");
            System.out.println("5) Quit");

            switch (selection) {
                case 1: {
                    System.out.print("Base:");
                    int base = scan.nextInt();
                    System.out.print("Height:");
                    int height = scan.nextInt();
                    area = area_triangle(base, height);
                    break;
                }
                case 2: {
                    System.out.print("Length:");
                    int length = scan.nextInt();
                    System.out.print("Height:");
                    int height = scan.nextInt();
                    area = area_rectangle(length, height);
                    break;
                }
                case 3: {
                    System.out.print("Length:");
                    int length = scan.nextInt();
                    area = area_square(length);
                    break;
                }
                case 4: {
                    System.out.print("Radius:");
                    int radius = scan.nextInt();
                    area = area_circle(radius);
                    break;
                }
            }
            System.out.println(String.format("The area is %.2f", area));
        }
        System.out.println("Goodbye");
    }

    public static double area_circle(int radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    public static double area_rectangle(int length, int width) {
        return length * width;
    }

    public static double area_square(int length) {
        return area_rectangle(length, length);
    }

    public static double area_triangle(int base, int height) {
        return (base * height) / 2;
    }
}
