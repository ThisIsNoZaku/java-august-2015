
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class KeychainsForSale {

    private static int keychainCount = 0;
    private static int keychainPrice = 10;
    private static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int choice = 0;
        while (choice != 4) {
            System.out.println("Ye Olde Keychain Shoppe");
            System.out.println("1. Add Keychains to your order");
            System.out.println("2. Remove keychains from your order");
            System.out.println("3. View Current Order");
            System.out.println("4. Checkout");
            System.out.print("Please enter your choice: ");
            choice = scan.nextInt();
            scan.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("How many keychains to add?");
                    int amountToAdd = scan.nextInt();
                    add_keychains(amountToAdd);
                    break;
                case 2:
                    System.out.println("How many keychains to remove?");
                    int amountToRemove = scan.nextInt();
                    add_keychains(amountToRemove);
                    break;
                case 3:
                    view_order(keychainCount, keychainPrice);
                    break;
                case 4:
                    check_out(keychainCount, keychainPrice);
            }
        }
    }

    public static int add_keychains(int amountToAdd) {
        keychainCount += amountToAdd;
        return keychainCount;
    }

    public static int remove_keychains(int amountToRemove) {
        keychainCount -= amountToRemove;
        return keychainCount;
    }

    public static void view_order(int keychainAmount, int keychainPrice) {
        System.out.println("You have " + keychainCount + " keychains.");
        System.out.println("Keychains cost $" + keychainPrice + ".");
        System.out.println("Total cost is $" + (keychainPrice * keychainCount));
    }

    public static void check_out(int keychainAmount, int keychainPrice) {
        System.out.print("What is your name? ");
        String name = scan.nextLine();
        view_order(keychainAmount, keychainPrice);
        System.out.println("Thank's for your order, " + name);
    }
}
