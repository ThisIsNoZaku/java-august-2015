
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class KeychainsForSale {

    private static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int keychainCount = 0;
        int keychainPrice = 10;
        double salesTax = 8.25;
        int PER_ORDER_SHIPPING_COST = 5;
        int PER_ITEM_SHIPPING_COST = 1;
        int choice = 0;
        while (choice
                != 4) {
            System.out.println("Ye Olde Keychain Shoppe");
            System.out.println("1. Add Keychains to your order");
            System.out.println("2. Remove keychains from your order");
            System.out.println("3. View Current Order");
            System.out.println("4. Checkout");
            System.out.print("Please enter your choice: ");
            choice = scan.nextInt();
            scan.nextLine();
            switch (choice) {
                case 1:
                    add_keychains(keychainCount);
                    break;
                case 2:
                    add_keychains(keychainCount);
                    break;
                case 3:
                    view_order(keychainCount, keychainPrice, salesTax, PER_ITEM_SHIPPING_COST, PER_ORDER_SHIPPING_COST);
                    break;
                case 4:
                    check_out(keychainCount, keychainPrice, salesTax, PER_ITEM_SHIPPING_COST, PER_ORDER_SHIPPING_COST);
                    break;
                default:
                    System.out.println("You must enter a number between 1 and 4.");
            }
        }
    }

    public static int add_keychains(int startingAmount) {
        System.out.println("How many keychains to add?");
        int amountToAdd = scan.nextInt();
        return startingAmount + amountToAdd;
    }

    public static int remove_keychains(int startingAmount) {
        System.out.println("How many keychains to remove?");
        int amountToRemove = scan.nextInt();
        if (amountToRemove > startingAmount) {
            System.out.println("You can only remove up to " + startingAmount);
            return 0;
        } else {
            return startingAmount - amountToRemove;
        }

    }

    public static void view_order(int keychainAmount, int keychainPrice, double salesTax, int perItemCost, int perOrderCost) {
        System.out.println("You have " + keychainAmount + " keychains.");
        System.out.println("Keychains cost $" + keychainPrice + ".");
        int shippingCost = perOrderCost + perItemCost * keychainAmount;
        System.out.println("Shipping total $" + shippingCost);
        double subtotal = keychainAmount * keychainPrice;
        System.out.println("Subtotal $" + subtotal);
        double tax = subtotal * salesTax;
        System.out.println("Sales Tax $" + tax);
        System.out.println("Total cost is $" + (subtotal + tax));
    }

    public static void check_out(int keychainAmount, int keychainPrice, double salesTax, int perItemCost, int perOrderCost) {
        System.out.print("What is your name? ");
        String name = scan.nextLine();
        view_order(keychainAmount, keychainPrice, salesTax, perItemCost, perOrderCost);
        System.out.println("Thank's for your order, " + name);
    }
}
