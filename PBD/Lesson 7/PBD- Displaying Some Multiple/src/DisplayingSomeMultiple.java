
import java.util.Scanner;

public class DisplayingSomeMultiple {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Choose a number: ");
        int number = scan.nextInt();
        for (int i = 1; i <= 12; i++) {
            System.out.println(String.format("%dx%d = %d", number, i, number * i));
        }
    }

}
