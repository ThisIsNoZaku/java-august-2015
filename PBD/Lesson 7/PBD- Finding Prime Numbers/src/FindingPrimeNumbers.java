
import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class FindingPrimeNumbers {

    private static Set<Integer> primes = new HashSet<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for (int i = 2; i <= 20; i++) {
            boolean isPrime = true;
            for (Integer prime : primes) {
                if (i % prime == 0) {
                    isPrime = false;
                    break;
                }
            }
            System.out.print(i + "");
            if (isPrime) {
                System.out.print("<");
                primes.add(i);
            }
            System.out.println();
        }
    }

}
