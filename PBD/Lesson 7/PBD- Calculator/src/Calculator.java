
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        double a, b, c;
        String op;

        do {
            System.out.print("> ");
            a = keyboard.nextDouble();
            op = keyboard.next();
            b = keyboard.nextDouble();

            if (a == 0) {
                break;
            }

            switch (op) {
                case "+":
                    c = a + b;
                    break;
                case "-":
                    c = a - b;
                    break;
                case "*":
                    c = a * b;
                    break;
                case "/":
                    c = a / b;
                    break;
                case "^":
                    c = Math.pow(a, b);
                    break;
                case "%":
                    c = a % b;
                    break;
                case "!":
                    c = 0;
                    for (int i = 1; i <= a; i++) {
                        c *= i;
                    }
                default:
                    System.out.println("Error: Invalid operator.");
                    continue;
            }

            if (op.equals("+")) {

            } else {
                System.out.println("Undefined operator: '" + op + "'.");
                c = 0;
            }

            System.out.println(c);

        } while (true);
        System.out.println(
                "Goodbye");
    }
}
