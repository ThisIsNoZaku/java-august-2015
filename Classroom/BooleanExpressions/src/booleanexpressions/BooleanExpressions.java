/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booleanexpressions;

/**
 *
 * @author apprentice
 */
public class BooleanExpressions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean op1 = false;
        boolean op2 = true;
        if (op1 == op2) {

        }
        if (op1 != op2) {

        }
        if (!op2) {

        }
        if (op1 == true && op2 == true) {

        }
        if (op1 || op2) {

        }

        if (op1 ^ op2) {

        }

        int int1 = 5;
        int int2 = 10;
        if (int1 < int2) {

        }

        if (op1 && op2) {

        } else if (op1) {

        } else if (op2) {

        } else {

        }

        int1 = 3;

        String testString = "test";

        switch (testString) {
            case "test":
                break;
            case "something else":
                break;
            case "football":
                break;
        }

        int day = 4;
        String dayName = "";
        if (day == 1) {
            dayName = "Monday";
        } else if (day == 2) {
            dayName = "Tuesday";
        } else if (day == 3) {
            dayName = "Wednesday";
        } else if (day == 4) {
            dayName = "Thursday";
        } else if (day == 5) {
            dayName = "Friday";
        } else if (day == 6) {
            dayName = "Saturday";
        } else if (day == 7) {
            dayName = "Sunday";
        } else {
            throw new Error("What is going on");
        }
        switch (day) {
            case 1:
                dayName = "Monday";
                break;
            case 2:
                dayName = "Tuesday";
                break;
            case 3:
                dayName = "Wednesday";
                break;
            case 4:
                dayName = "Thursday";
                break;
            case 5:
                dayName = "Friday";
                break;
            case 6:
                dayName = "Saturday";
                break;
            case 7:
                dayName = "Sunday";
            default:
                throw new Error("What is going on");
        }

        String typeOfDay = "";

        switch (day) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                typeOfDay = "Weekday.";
                break;
            case 6:
            case 7:
                typeOfDay = "Weekend.";
                break;
            default:
                typeOfDay = "Invalid day.";
        }
    }
}
