/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowmaster;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMaster {

    private static final Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float height;
        float width;

        String stringHeight;
        String stringWidth;

        float windowArea;
        float cost;
        float windowPerimeter;

        stringHeight = promptInput("Please enter window height: ");

        stringWidth = promptInput("Please enter window width: ");

        String areaPrice = promptInput("Please enter glass price: ");

        String perimeterPrice = promptInput("Please enter trim price: ");

        height = Float.parseFloat(stringHeight);
        width = Float.parseFloat(stringWidth);

        windowArea = height * width;
        windowPerimeter = 2 * (height + width);

        cost = (Float.parseFloat(areaPrice) * windowArea) + (Float.parseFloat(perimeterPrice) * windowPerimeter);

        System.out.println("Window height = " + stringHeight);
        System.out.println("Window width = " + stringWidth);
        System.out.println("Window area = " + String.format("%.2f", windowArea));
        System.out.println("Window perimeter = " + windowPerimeter);
        System.out.println("Total cost = $" + String.format("%.2f", cost));
    }

    private static String promptInput(String prompt) {
        System.out.println(prompt);
        return sc.nextLine();
    }
}
