/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adder;

/**
 *
 * @author apprentice
 */
public class Adder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Adder adder = new Adder();
        
        int sum = adder.add(1, 2);

        System.out.println("Sum is: " + sum);
    }

    public int add(int a, int b) {
        return a + b;
    }
}
