/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prime.finder;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class PrimeFinder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Set<Integer> primes = new LinkedHashSet<>();
        {
            primes.add(1);
            primes.add(2);
        }
        String in = "";
        boolean go = true;
        int loopCounter = 0;
        while (go) {
            loopCounter++;
            System.out.println("Enter a number to find the largest prime number less than that number.");
            int limit = Integer.parseInt(scan.nextLine());
            if (limit < 1) {
                System.out.println("You must enter a number greater than 0.");
                continue;
            }

            for (int i = 3; i < limit; i += 2) {
                boolean isPrime = true;
                for (int prime : primes) {
                    if (i % prime == 0 && prime != 1) {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime) {
                    primes.add(i);
                }
            }

            System.out.println(String.format("There were %d primes.", primes.size()));
            for (int prime : primes) {
                System.out.println(prime);
            }

            while (!in.equals("y") && !in.equals("n")) {
                System.out.println("Go again? y/n");
                in = scan.nextLine().toLowerCase();
                if (in.equals("y")) {
                    continue;
                } else {
                    go = false;
                }
            }
        }
        System.out.println(String.format("Thank you. You looked for primes %d times", loopCounter));
    }
}
