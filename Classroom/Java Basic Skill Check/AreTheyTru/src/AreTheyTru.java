
public class AreTheyTru {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AreTheyTru tester = new AreTheyTru();
        System.out.println(String.format("%b %b : %s", true, true, tester.howTrue(true, true)));
        System.out.println(String.format("%b %b : %s", true, false, tester.howTrue(true, false)));
        System.out.println(String.format("%b %b : %s", false, true, tester.howTrue(false, true)));
        System.out.println(String.format("%b %b : %s", false, false, tester.howTrue(false, false)));
    }

    public String howTrue(boolean a, boolean b) {
        if (a && b) {
            return "BOTH";
        } else if (a ^ b) {
            return "ONLY ONE";
        } else {
            return "NEITHER";
        }
    }
}
