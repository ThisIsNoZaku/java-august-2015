/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Counter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Counter c = new Counter();
        c.to10();
        System.out.println();
        c.toN(3);
        System.out.println();
        c.toN(8);
        System.out.println();
        c.toN(200);
    }

    public void to10() {
        toN(10);
    }

    public void toN(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.println(i);
        }
    }
}
