/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adder2;

import java.time.Clock;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Adder2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int sum = 0;
        int op1 = 0;
        int op2 = 0;
        
        Scanner sc = new Scanner(System.in);
        
        String stringOperand1 = "";
        String stringOperand2 = "";
        
        System.out.println("Enter the first number to be added:");
        stringOperand1 = sc.nextLine();
        
        System.out.println("Enter the second number to be added:");
        stringOperand2 = sc.nextLine();
        
        op1 = Integer.parseInt(stringOperand1);
        op2 = Integer.parseInt(stringOperand2);
        
        sum = op1+op2;
        
        System.out.println("Sum is: " + sum);
    }
    
}
