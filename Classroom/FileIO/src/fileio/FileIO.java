/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileio;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class FileIO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            PrintWriter out = new PrintWriter(new FileWriter("out.txt"));
            out.println("This is the first line.");
            out.println("This is the second line.");
            out.println("This is the third line.");
            PrintStream o;

            out.flush();
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        try {
            Scanner scan = new Scanner(new BufferedReader(new FileReader("out.txt")));
            String in;
            while(scan.hasNext()){
                in = scan.nextLine();
                System.out.println(in);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
