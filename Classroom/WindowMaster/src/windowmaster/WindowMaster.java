/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowmaster;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMaster {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float height;
        float width;
        
        String stringHeight;
        String stringWidth;
        
        float windowArea;
        float cost;
        float windowPerimeter;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Please enter window height: ");
        stringHeight = sc.nextLine();
        
        System.out.print("Please enter window width: ");
        stringWidth = sc.nextLine();
        
        height = Float.parseFloat(stringHeight);
        width = Float.parseFloat(stringWidth);
        
        windowArea = height * width;
        windowPerimeter = 2 * height + 2 * width;
        
        cost = (3.50f * windowArea) + (2.25f * windowPerimeter);
        
        System.out.println("Window height = " + stringHeight);
        System.out.println("Window width = " + stringWidth);
        System.out.println("Window area = " + String.format("%.2f", windowArea));
        System.out.println("Window perimeter = " + windowPerimeter);
        System.out.println("Total cost = $" + String.format("%.2f", cost));
    }
}
