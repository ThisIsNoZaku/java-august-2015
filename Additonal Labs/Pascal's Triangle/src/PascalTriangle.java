
import com.sun.xml.internal.ws.util.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class PascalTriangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int input = 0;
        while (input <= 0) {
            System.out.print("Enter number to calculate Pascal's Triangle for: ");
            input = scan.nextInt();
        }
        Map<Integer, List<Integer>> triangle = generateTriangleForNumber(input);
        int width = triangle.keySet().stream().mapToInt(Integer::intValue).max().getAsInt() * 2 - 1;
        int largestDigitWidth = Integer.toString(triangle.values().stream().mapToInt(v -> v.stream().mapToInt(Integer::intValue).max().getAsInt()).max().getAsInt()).length();
        final StringBuilder b = new StringBuilder();

        IntStream.range(0, largestDigitWidth).limit(largestDigitWidth).forEach((i) -> b.append(" "));
        if (b.length() % 2 != 0) {
            b.append(" ");
        }
        final String separator = b.toString();
        for (int row : triangle.keySet()) {
            for (int i = 0; i < Math.floor((width - (row * 2 - 1)) / 2.0); i++) {
                System.out.print(separator);
            }
            System.out.print(triangle.get(row).stream().map(i -> org.apache.commons.lang3.StringUtils.center(Integer.toString(i), separator.length())).collect(Collectors.joining(separator)));
            for (int i = 0; i < Math.floor((width - (row * 2 - 1)) / 2.0); i++) {
                System.out.print(separator);
            }
            System.out.println();
        }
    }

    public static Map<Integer, List<Integer>> generateTriangleForNumber(int number) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 1; i <= number; i++) {
            map.put(i, new ArrayList<>());
            for (int j = 1; j <= i; j++) {
                if (j == 1 || j == i) {
                    map.get(i).add(1);
                } else {
                    map.get(i).add(map.get(i - 1).get(j - 2) + map.get(i - 1).get(j - 1));
                }
            }
        }
        return map;
    }
}
