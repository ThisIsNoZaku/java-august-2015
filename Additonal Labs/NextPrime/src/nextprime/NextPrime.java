package nextprime;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class NextPrime {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Integer> primes = new ArrayList<>();
        primes.add(1);
        primes.add(2);
        primes.add(3);
        System.out.print("Enter a number to find the prime number after it:");
        Scanner sc = new Scanner(System.in);
        int val = sc.nextInt();
        int largestPrime = 0;
        int primeCandidate = 3;
        while (val > largestPrime){
            primeCandidate += 2;
            for (Integer prime : primes){
                if(primeCandidate % prime == 0){
                    break;
                }
            }
            largestPrime = primeCandidate;
            primes.add(largestPrime);
        }
        System.out.println("Largest prime after " + val + " is " + largestPrime);
    }
    
}
