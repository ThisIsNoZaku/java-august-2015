/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibboniacci;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Fibboniacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<Integer, Integer> fibbonacciNumbers = new HashMap<Integer, Integer>();
        fibbonacciNumbers.put(0, 0);
        fibbonacciNumbers.put(1, 1);
        while (true) {
            System.out.print("Enter term to calculate Fibbonacci number for:");
            String termString = sc.nextLine();
            try {
                int term;
                try {
                    term = Integer.parseInt(termString);
                } catch (NumberFormatException ex) {
                    throw new IllegalArgumentException(ex);
                }
                if (term <= 0) {
                    throw new IllegalArgumentException();
                }
                int previous = 0;
                int secondPrevious = 0;
                if (term >= 2) {
                    for (int i = 2; i <= term; i++) {
                        fibbonacciNumbers.put(i, fibbonacciNumbers.get(i - 1) + fibbonacciNumbers.get(i - 2));
                    }
                }
                int value = fibbonacciNumbers.get(term);
                System.out.println("Fibbonacci number for " + term + " is " + value);
            } catch (IllegalArgumentException ex) {
                System.out.println("Error: Input must be a positive integer.");
            }
        }
    }
}