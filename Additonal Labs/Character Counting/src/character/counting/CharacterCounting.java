/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package character.counting;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class CharacterCounting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Enter a string:");
        Scanner sc = new Scanner(System.in);
        String in = sc.nextLine().toLowerCase().replaceAll("[^a-z]", "");
        Matcher matcher = Pattern.compile("[aeiou]").matcher(in);
        int vowels = 0;
        while (matcher.find()) {
            vowels++;
        }
        int consonants = in.length() - vowels;

        System.out.println(vowels + " vowels and " + consonants + " consonants.");
    }

}
