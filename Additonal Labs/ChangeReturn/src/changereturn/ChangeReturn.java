/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package changereturn;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author apprentice
 */
public class ChangeReturn extends JPanel {
    private final JList itemsList;
    private static final Map<String, Double> items = new HashMap<String, Double>();
    private static final Map<Integer, String> indices = new HashMap<Integer, String>();

    static {
        items.put("Skittles", 1.0);
        items.put("Sprite", 2.5);
    }
    final JTextField itemNumberField = new JTextField();
    private JButton confirmButton = new JButton("Confirm");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ChangeReturn change = new ChangeReturn();
        frame.getContentPane().add(change);

        frame.pack();
        frame.getRootPane().setDefaultButton(change.confirmButton);
        frame.setVisible(true);
    }

    public ChangeReturn() {
        setLayout(new BorderLayout());
        Object[] listItems = new Object[items.size()];
        int counter = 0;
        for (Map.Entry<String, Double> entry : items.entrySet()) {
            listItems[counter] = String.format("%s - $%.2f", entry.getKey(), entry.getValue());
            indices.put(counter, entry.getKey());
            counter++;
        }
        itemsList = new JList(listItems);
        itemsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        itemsList.setFocusable(false);
        itemsList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    SwingUtilities.invokeLater(() -> {
                        itemNumberField.setText(""+itemsList.getSelectedIndex());
                    });
                } else {
                    itemNumberField.setText(""+itemsList.getSelectedIndex());
                }
            }
        });

        add(itemsList, BorderLayout.WEST);
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));

        centerPanel.add(new JLabel("Selected Item"));
        centerPanel.add(itemNumberField);

        centerPanel.add(new JLabel("Enter amount tendered:"));
        final JTextField inputField = new JTextField();
        inputField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (Pattern.compile("\\A\\d+\\z|\\A\\d*\\.\\d{1,2}\\z").matcher(inputField.getText()).matches()) {
                    confirmButton.setEnabled(true);
                    inputField.setBackground(Color.WHITE);
                } else {
                    confirmButton.setEnabled(false);
                    inputField.setBackground(Color.RED);
                }
            }
        });
        centerPanel.add(inputField);
        centerPanel.add(confirmButton);

        confirmButton.setEnabled(false);
        confirmButton.setFocusable(false);

        centerPanel.add(new JLabel("Change:"));
        JTextArea changeField = new JTextArea();
        changeField.setFocusable(false);
        changeField.setEditable(false);
        centerPanel.add(changeField);

        add(centerPanel, BorderLayout.CENTER);

        confirmButton.addActionListener((ActionEvent e) -> {
            BigDecimal price = new BigDecimal(items.get(indices.get(Integer.parseInt(itemNumberField.getText()))));
            BigDecimal amount = new BigDecimal(inputField.getText());

            amount = amount.subtract(price);
            BigDecimal quarters = amount.divide(new BigDecimal(".25")).setScale(0, RoundingMode.FLOOR);
            amount = amount.subtract(new BigDecimal(".25").multiply(quarters));
            BigDecimal dimes = amount.divide(new BigDecimal(".10")).setScale(0, RoundingMode.FLOOR);
            amount = amount.subtract(new BigDecimal(".10").multiply(dimes));
            BigDecimal nickels = amount.divide(new BigDecimal(".05")).setScale(0, RoundingMode.FLOOR);
            amount = amount.subtract(new BigDecimal(".05").multiply(nickels));
            BigDecimal pennies = amount.divide(new BigDecimal(".01")).setScale(0, RoundingMode.FLOOR);
            changeField.setText(String.format("%s Quarters, %s Dimes, %s Nickels and %s Pennies.", quarters.toPlainString(), dimes.toPlainString(), nickels.toPlainString(), pennies.toPlainString()));
            SwingUtilities.getWindowAncestor(this).pack();
        });
    }
}