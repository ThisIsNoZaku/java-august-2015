/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CreditCardValidatorTest {

    List<Long> visaNumbers = new ArrayList<>();
    List<Long> discoverNumbers = new ArrayList<>();
    List<Long> amexNumbers = new ArrayList<>();
    List<Long> mastercardNumbers = new ArrayList<>();

    @Before
    public void setUp() {
        visaNumbers.add(4929394778456126L);
        visaNumbers.add(4716322328578005L);
        visaNumbers.add(4649092253521401L);
        visaNumbers.add(4716161105959727L);
        visaNumbers.add(4532670147349442L);
        visaNumbers.add(4539166170802442L);

        discoverNumbers.add(6011042435888553L);
        discoverNumbers.add(6011083545075996L);
        discoverNumbers.add(6011819797241194L);
        discoverNumbers.add(6011617417737256L);
        discoverNumbers.add(6011089447456762L);
        discoverNumbers.add(6011035956530406L);

        mastercardNumbers.add(5453785106901421L);
        mastercardNumbers.add(5510006071093875L);
        mastercardNumbers.add(5134025520117644L);
        mastercardNumbers.add(5150840891258259L);
        mastercardNumbers.add(5308169822464559L);
        mastercardNumbers.add(5504964502883710L);

        amexNumbers.add(340090401673116L);
        amexNumbers.add(376290713564337L);
        amexNumbers.add(372449773355333L);
        amexNumbers.add(343635750984592L);
        amexNumbers.add(344928632151350L);
        amexNumbers.add(371367415916505L);
    }

    @Test
    public void testForVisaNumbers() {
        CreditCardValidator.CardType type = CreditCardValidator.CardType.VISA;
        for (long number : visaNumbers) {
            assertEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : discoverNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : amexNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : mastercardNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
    }

    @Test
    public void testForDiscoverNumbers() {
        CreditCardValidator.CardType type = CreditCardValidator.CardType.DISCOVER;
        for (long number : visaNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : discoverNumbers) {
            assertEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : amexNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : mastercardNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
    }

    @Test
    public void testForMastercardNumbers() {
        CreditCardValidator.CardType type = CreditCardValidator.CardType.MASTERCARD;
        for (long number : visaNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : discoverNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : amexNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : mastercardNumbers) {
            assertEquals(type, CreditCardValidator.getCardType(number));
        }
    }

    @Test
    public void testForAmexNumbers() {
        CreditCardValidator.CardType type = CreditCardValidator.CardType.AMERICAN_EXPRESS;
        for (long number : visaNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : discoverNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : amexNumbers) {
            assertEquals(type, CreditCardValidator.getCardType(number));
        }
        for (long number : mastercardNumbers) {
            assertNotEquals(type, CreditCardValidator.getCardType(number));
        }
    }

    @Test
    public void testForValidNumbers() {
        for (long number : discoverNumbers) {
            CreditCardValidator.CardType issuer = CreditCardValidator.getCardType(number);
            assertTrue(CreditCardValidator.validateNumber(number, issuer));
        }
        for (long number : amexNumbers) {
            CreditCardValidator.CardType issuer = CreditCardValidator.getCardType(number);
            assertTrue(CreditCardValidator.validateNumber(number, issuer));
        }
        for (long number : mastercardNumbers) {
            CreditCardValidator.CardType issuer = CreditCardValidator.getCardType(number);
            assertTrue(CreditCardValidator.validateNumber(number, issuer));
        }
        for (long number : visaNumbers) {
            CreditCardValidator.CardType issuer = CreditCardValidator.getCardType(number);
            assertTrue(CreditCardValidator.validateNumber(number, issuer));
        }
    }

}
