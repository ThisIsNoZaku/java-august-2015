
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CreditCardValidator {

    public static boolean validateNumber(long creditCardNumber, CardType issuer) {
        if (issuer != null && getCardType(creditCardNumber) != issuer) {
            return false;
        }
        int[] digits = new int[Long.toString(creditCardNumber).length()];

        for (int i = 0; i < Long.toString(creditCardNumber).length(); i++) {
            digits[i] = Integer.parseInt(Long.toString(creditCardNumber).charAt(i) + "");
        }
        for (int i = 1; i < digits.length; i += 2) {
            digits[i] *= 2;
            if (digits[i] > 9) {
                digits[i] = digits[i] / 10 + digits[i] % 10;
            }
        }
        int sum = 0;
        for (int i = 0; i < digits.length - 1; i++) {
            sum += digits[i];
        }
        sum *= 9;
        return sum % 10 == 0;
    }

    public static CardType getCardType(long creditCardNumber) {
        String cardString = Long.toString(creditCardNumber);
        for (CardType card : CardType.values()) {
            if (card.pattern.matcher(cardString).matches()) {
                return card;
            }
        }
        return null;
    }

    public static enum CardType {

        AMERICAN_EXPRESS("(34|37)\\d{13}"),
        DISCOVER("6011\\d{12}|622[1-9]2[1-2]\\d{10}|64[4-9]\\d{13}|65\\{14}"),
        MASTERCARD("5[1-5]\\d{14,17}"),
        VISA("4\\d{12,15}");

        private final Pattern pattern;

        private CardType(String pattern) {
            this.pattern = Pattern.compile(pattern);
        }
    }
}
