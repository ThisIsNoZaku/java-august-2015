
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class MorseCodeMaker {

    private static final Map<String, String> charToMorse = new TreeMap<>();
    private static final Map<String, String> morseToChar = new TreeMap<>();

    static {
        charToMorse.put("A", ".-");
        charToMorse.put("B", "-...");
        charToMorse.put("C", "-.-.");
        charToMorse.put("D", "-..");
        charToMorse.put("E", ".");
        charToMorse.put("F", "..-.");
        charToMorse.put("G", "--.");
        charToMorse.put("H", "....");
        charToMorse.put("I", "..");
        charToMorse.put("J", ".---");
        charToMorse.put("K", "-.-");
        charToMorse.put("L", ".-..");
        charToMorse.put("M", "--");
        charToMorse.put("N", "-.");
        charToMorse.put("O", "---");
        charToMorse.put("P", ".--.");
        charToMorse.put("Q", "--.-");
        charToMorse.put("R", ".-.");
        charToMorse.put("S", "...");
        charToMorse.put("T", "-");
        charToMorse.put("U", "..-");
        charToMorse.put("V", "...-");
        charToMorse.put("W", ".--");
        charToMorse.put("X", "-..-");
        charToMorse.put("Y", "-.--");
        charToMorse.put("Z", "--..");

        morseToChar.put(".-", "A");
        morseToChar.put("-...", "B");
        morseToChar.put("-.-.", "C");
        morseToChar.put("-..", "D");
        morseToChar.put(".", "E");
        morseToChar.put("..-.", "F");
        morseToChar.put("--.", "G");
        morseToChar.put("....", "H");
        morseToChar.put("..", "I");
        morseToChar.put(".---", "J");
        morseToChar.put("-.-", "K");
        morseToChar.put(".-..", "L");
        morseToChar.put("--", "M");
        morseToChar.put("-.", "N");
        morseToChar.put("---", "O");
        morseToChar.put(".--.", "P");
        morseToChar.put("--.-", "Q");
        morseToChar.put(".-.", "R");
        morseToChar.put("...", "S");
        morseToChar.put("-", "T");
        morseToChar.put("..-", "U");
        morseToChar.put("...-", "V");
        morseToChar.put(".--", "W");
        morseToChar.put("-..-", "X");
        morseToChar.put("-.--", "Y");
        morseToChar.put("--..", "Z");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.print("Enter a string to convert to morse code:");
            String in = scan.nextLine();
            String out = convertToMorse(in);
            System.out.println(out);
            String back = convertFromMorse(out);
            System.out.println(back);
        }
    }

    public static String convertToMorse(String in) {
        in = in.toUpperCase().replace(" ", "/");
        for (Entry<String, String> entry : charToMorse.entrySet()) {
            in = in.replace(entry.getKey(), entry.getValue() + " ");
        }
        return in;
    }

    public static String convertFromMorse(String in) {
        String[] tokens = in.split(" ");
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = tokens[i].replace(tokens[i].replace("/", ""), morseToChar.get(tokens[i].replace("/", "")));
            tokens[i] = tokens[i].replace("/", " ");
        }
        in = Arrays.asList(tokens).stream().collect(Collectors.joining());
        return in;
    }
}
