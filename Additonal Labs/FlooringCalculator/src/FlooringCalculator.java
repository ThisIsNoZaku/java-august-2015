
import javax.swing.JPanel;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class FlooringCalculator extends JPanel {

    private static final BigDecimal laborPerHour = new BigDecimal("86.00");
    private static BigDecimal width = null;
    private static BigDecimal length = null;
    private static BigDecimal cost = null;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String nextInput = "";
        while (!nextInput.equals("q")) {
            if (width == null) {
                System.out.println("Enter the width or 'q' to exit");
            } else if (length == null) {
                System.out.println("Enter the length  or 'q' to exit");
            } else if (cost == null) {
                System.out.println("Enter the cost per unit or 'q' to exit");
            } else {
                BigDecimal materialArea = width.multiply(length);
                BigDecimal materialCost = materialArea.multiply(cost);
                BigDecimal laborCost = materialArea.divide(new BigDecimal(5)).setScale(0, RoundingMode.CEILING).multiply(laborPerHour.divide(new BigDecimal(4)));
                System.out.println("Material Cost: $" + materialCost.toPlainString());
                System.out.println("Labor Cost: $" + laborCost.toPlainString());
                System.out.println("Total cost: $" + materialCost.add(laborCost).toPlainString());
                width = null;
                length = null;
                cost = null;
                continue;
            }
            nextInput = sc.nextLine();
            try {
                if (width == null) {
                    width = new BigDecimal(nextInput);
                } else if (length == null) {
                    length = new BigDecimal(nextInput);
                } else if (cost == null) {
                    cost = new BigDecimal(nextInput);
                }
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, you entered an invalid value. Try again.");
                nextInput = "";
                continue;
            }
        }
    }
}
