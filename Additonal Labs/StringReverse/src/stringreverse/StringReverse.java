/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringreverse;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StringReverse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a string to reverse:");
        String in = sc.nextLine();
        char[] tokens = in.toCharArray();
        for (int i = 0; i < in.length() / 2; i++) {
            char swap = tokens[i];
            tokens[i] = tokens[tokens.length - 1 - i];
            tokens[tokens.length - 1 - i] = swap;
        }
        System.out.println(new String(tokens));
    }
}
