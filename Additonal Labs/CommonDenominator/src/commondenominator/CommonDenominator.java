package commondenominator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import org.apache.commons.math3.util.ArithmeticUtils;

/**
 *
 * @author apprentice
 */
public class CommonDenominator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Fraction> fractions = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String input = " ";
        while (!input.equals("")) {
            System.out.println("Enter a fraction or just press enter to end: ");
            input = sc.nextLine();
            if (!input.equals("")) {
                String[] tokens = input.split("/");
                fractions.add(new Fraction(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1])));
            }
        }

        int gcd = greatestCommonDenominator(fractions.stream().map(f -> f.reduce()).collect(Collectors.toList()));
        int lcd = leastCommonDenominator(fractions.stream().map(f -> f.reduce()).collect(Collectors.toList()));
        System.out.println(String.format("The greatest common denominator is %d", gcd));
        System.out.println(String.format("The least common denominator is %d", lcd));
    }

    /**
     * Returns the greatest common denominator among the given fractions.
     *
     * If the array is length 0, returns 0. If the
     *
     * @param fractions
     * @return
     */
    public static int greatestCommonDenominator(List<Fraction> fractions) {
        if (0 == fractions.size()) {
            return 0;
        } else if (fractions.size() == 1) {
            return fractions.get(0).denominator;
        } else if (fractions.size() == 2) {
            return ArithmeticUtils.gcd(fractions.get(0).denominator, fractions.get(1).denominator);
        } else {
            return ArithmeticUtils.gcd(fractions.get(0).denominator, greatestCommonDenominator(fractions.subList(1, fractions.size())));
        }
    }

    public static int leastCommonDenominator(List<Fraction> fractions) {
        if (fractions.isEmpty()) {
            return 0;
        }
        if (fractions.size() == 1) {
            return fractions.get(0).denominator;
        } else if (fractions.size() == 2) {
            return ArithmeticUtils.lcm(fractions.get(0).denominator, fractions.get(1).denominator);
        } else {
            return ArithmeticUtils.lcm(fractions.get(0).denominator, greatestCommonDenominator(fractions.subList(1, fractions.size())));
        }
    }

}
