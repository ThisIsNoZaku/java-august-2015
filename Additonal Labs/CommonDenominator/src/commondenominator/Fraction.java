/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commondenominator;

/**
 *
 * @author apprentice
 */
public class Fraction {

    private final int numerator;
    final int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    public Fraction reduce() {
        int n = numerator;
        int d = denominator;
        for (int i = 2; i <= numerator && i <= denominator; i += 2) {
            while (n % i == 0 && d % 1 == 0) {
                n /= i;
                d /= i;
            }
        }
        if (n == numerator && d == denominator) {
            return this;
        }
        return new Fraction(n, d);
    }
}
