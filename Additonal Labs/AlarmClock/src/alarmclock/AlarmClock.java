/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarmclock;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class AlarmClock {

    private static final Pattern militaryTimePattern = Pattern.compile("\\d{1,2}:\\d{2}");
    private static final Pattern standardTimePattern = Pattern.compile("\\d{1,2}:\\d{2} am|pm");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("The current time is " + new SimpleDateFormat("hh:mm").format(Calendar.getInstance().getTime()));
        Scanner in = new Scanner(System.in);
        final Queue<Calendar> alarmQueue = new PriorityQueue<>();
        final Thread alarmThread = new Thread() {
            public void run() {
                while (!alarmQueue.isEmpty()) {
                    if (alarmQueue.peek() != null && alarmQueue.peek().compareTo(Calendar.getInstance()) < 0) {
                        System.out.println("Alarm went off at " + new SimpleDateFormat("hh:mm").format(alarmQueue.poll().getTime()));
                        for (int i = 0; i < 3; i++) {                            java.awt.Toolkit.getDefaultToolkit().beep();
                        }
                    } else {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                System.out.println("All alarms have triggered.");
            }
        };
        while (true) {
            System.out.println("Enter a time to set an alarm.");
            System.out.println("Enter in format hh:mm. Use 24-hour clock or put 'am' or 'pm' at the end.");
            System.out.println("If the current time is past the entered time, the alarm will be set for tomorrow.");
            String nextIn = in.nextLine();
            boolean error = false;

            String[] tokens = nextIn.split(":|\\s+");
            int hour = Integer.parseInt(tokens[0]);
            int minute = Integer.parseInt(tokens[1]);
            if (militaryTimePattern.matcher(nextIn).matches()) {

                if (hour < 0 || hour > 23) {
                    System.out.println("Hour has to be between 0 and 23 for 24-hour clock.");
                    error = true;
                }
                if (minute < 0 || minute > 59) {
                    System.out.println("Minute has to be between 0 and 59");
                    error = true;
                }
            } else if (standardTimePattern.matcher(nextIn).matches()) {
                if (hour < 1 || hour > 12) {
                    System.out.println("Hour has to be between 1 and 12 for standard clock.");
                    error = true;
                }
                if (minute < 0 || minute > 59) {
                    System.out.println("Minute has to be between 0 and 59.");
                    error = true;
                }
                //If time is 1 pm or later, add 12 hours to match 24-hour time.
                if (tokens[2].toLowerCase().equals("pm")) {
                    if (hour < 12) {
                        hour += 12;
                    }
                } //If time is 12 am, set hours to 0 to match 24-hour time.
                else {
                    if (hour == 12) {
                        hour = 0;
                    }
                }
            }

            if (error) {
                error = false;
                continue;
            }

            Calendar nextAlarm = Calendar.getInstance();
            nextAlarm.set(Calendar.HOUR, hour);
            nextAlarm.set(Calendar.MINUTE, minute);
            if (nextAlarm.compareTo(Calendar.getInstance()) < 0) {
                nextAlarm.add(Calendar.DATE, 1);
            }
            alarmQueue.add(nextAlarm);
            System.out.println("Alarm has been set for " + new SimpleDateFormat("E, hh:mm").format(nextAlarm.getTime()));
            if (!alarmThread.isAlive()){
                alarmThread.start();
            }
        }
    }

}
