/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FractionTest {
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testReduce() {
        assertTrue(new Fraction(2, 4).reduce().equals(new Fraction(1, 2)));
    }
    
    @Test
    public void testAdd() {
        assertTrue(new Fraction(1, 2).add(new Fraction(1, 2)).equals(new Fraction(1, 1)));
    }
    
    @Test
    public void testAddInt() {
        Fraction f = new Fraction(1, 2).add(2);
        assertTrue(f.equals(new Fraction(5, 2)));
    }
    
    @Test
    public void testSubtract() {
        assertTrue(new Fraction(2, 3).subtract(new Fraction(1, 3)).equals(new Fraction(1, 3)));
    }
    
    @Test
    public void testSubtractInt() {
        assertTrue(new Fraction(3, 2).subtract(1).equals(new Fraction(1, 2)));
    }
    
    @Test
    public void testDivide() {
        assertTrue(new Fraction(3, 4).divide(new Fraction(1, 2)).equals(new Fraction(3, 2)));
    }
    
    @Test
    public void testDivideInt() {
        assertTrue(new Fraction(3, 4).divide(2).equals(new Fraction(3, 8)));
    }
    
    @Test
    public void testMultiply() {
        assertTrue(new Fraction(1, 2).multiply(new Fraction(1, 2)).equals(new Fraction(1, 4)));
    }
    
    @Test
    public void testMultiplyInt() {
        assertTrue(new Fraction(1, 2).multiply(2).equals(new Fraction(1, 1)));
    }
    
    @Test
    public void testEquals() {
        assertTrue(new Fraction(1, 1).equals(new Fraction(3, 3)));
    }
    
    @Test
    public void testParseFraction() {
        assertTrue(Fraction.parseFraction("1/2").equals(new Fraction(1, 2)));
    }
    
    @Test
    public void testParseMixedFraction() {
        assertTrue(Fraction.parseFraction("1 1/2").equals(new Fraction(3, 2)));
    }
    
    @Test
    public void testParseWholeNumber() {
        assertTrue(Fraction.parseFraction("1").equals(new Fraction(1, 1)));
    }
    
    @Test(expected = NumberFormatException.class)
    public void testParseMissingDenominator() {
        assertTrue(Fraction.parseFraction("1/").equals(new Fraction(1, 1)));
    }
    
    @Test(expected = NumberFormatException.class)
    public void testParseMissingNumerator() {
        assertTrue(Fraction.parseFraction("/2").equals(new Fraction(1, 1)));
    }
    
    @Test
    public void testParseComplexFraction() {
        assertTrue(Fraction.parseFraction("1/2/3").equals(new Fraction(3, 2)));
    }
}
