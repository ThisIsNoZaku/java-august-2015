/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.math3.util.ArithmeticUtils;

/**
 *
 * @author apprentice
 */
public class Fraction {

    private static final Pattern FRACTION_PATTERN = Pattern.compile("^(\\d+)$|^(\\d*?) ?(\\d*)/(\\d*)$");
    private final int numerator;
    private final int denominator;
    private Integer hash = null;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction(int whole, int numerator, int denominator) {
        this(whole * denominator + numerator, denominator);
    }

    public Fraction(int whole) {
        this(whole, whole);
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    public Fraction reduce() {
        int n = numerator;
        int d = denominator;
        for (int i = 2; i <= numerator && i <= denominator; i += 2) {
            while (n % i == 0 && d % 1 == 0) {
                n /= i;
                d /= i;
            }
        }
        if (n == numerator && d == denominator) {
            return this;
        }
        return new Fraction(n, d);
    }

    public Fraction add(Fraction other) {
        int gcd = ArithmeticUtils.gcd(this.denominator, other.denominator);
        Fraction a = this, b = other;
        if (a.denominator != gcd) {
            a = new Fraction(numerator * gcd / numerator, denominator * gcd / denominator);
        }
        if (b.denominator != gcd) {
            b = new Fraction(numerator * gcd / numerator, denominator * gcd / denominator);
        }
        return new Fraction(a.numerator + b.numerator, a.denominator);
    }

    public Fraction add(int other) {
        return new Fraction(numerator + (other * denominator), denominator);
    }

    public Fraction subtract(Fraction other) {
        int gcd = ArithmeticUtils.gcd(this.denominator, other.denominator);
        Fraction a = this, b = other;
        if (a.denominator != gcd) {
            a = new Fraction(numerator * gcd / numerator, denominator * gcd / denominator);
        }
        if (b.denominator != gcd) {
            b = new Fraction(numerator * gcd / numerator, denominator * gcd / denominator);
        }
        return new Fraction(a.numerator - b.numerator, a.denominator);
    }

    public Fraction subtract(int other) {
        return new Fraction(numerator - (other * denominator), denominator + (other * denominator));
    }

    public Fraction divide(Fraction other) {
        return new Fraction(numerator * other.denominator, denominator * other.numerator);
    }

    public Fraction divide(int other) {
        int gcd = ArithmeticUtils.gcd(this.denominator, other);
        Fraction a = this, b = new Fraction(other, other);
        if (a.denominator < gcd) {
            a = new Fraction(a.numerator * gcd, a.denominator * gcd);
        }
        if (b.denominator < gcd) {
            b = new Fraction(b.numerator * gcd / numerator, b.denominator * gcd / denominator);
        }
        return new Fraction((a.numerator * b.denominator) / gcd, (a.denominator * b.numerator) / gcd);
    }

    public Fraction multiply(Fraction other) {
        return new Fraction(this.numerator * other.numerator, this.denominator * other.denominator).reduce();
    }

    public Fraction multiply(int other) {
        return new Fraction(this.numerator * other, this.denominator * other);
    }

    @Override
    public int hashCode() {
        if (this.hash == null) {

            int n = numerator;
            int d = denominator;
            for (int i = 2; i <= numerator && i <= denominator; i += 2) {
                while (n % i == 0 && d % 1 == 0) {
                    n /= i;
                    d /= i;
                }
            }
            int hash = 5;
            hash = 89 * hash + n;
            hash = 89 * hash + d;
            this.hash = hash;
        }
        return hash.intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fraction other = ((Fraction) obj).reduce();
        int gcd = ArithmeticUtils.gcd(denominator, other.denominator);

        int a = this.numerator * gcd, b = other.numerator * gcd;
        if (Math.max(a, b) % Math.min(a, b) != 0) {
            return false;
        }
        return this.denominator % gcd == other.denominator % gcd;
    }

    public static Fraction parseFraction(String in) {
        if (in.length() - in.replace("/", "").length() > 1) {
            return parseFraction(in.substring(0, in.indexOf("/"))).divide(Fraction.parseFraction(in.substring(in.indexOf("/") + 1)));
        }
        String whole = "";
        String numerator = "0";
        String denominator = "1";
        Matcher match;
        if ((match = FRACTION_PATTERN.matcher(in)).matches()) {
            //Is a whole number
            if (match.group(1) != null) {
                whole = match.group(1);
            } else //Is a fraction
            {
                whole = "0";
                numerator = match.group(3).equals("") ? "0" : match.group(3);
                denominator = match.group(4).equals("") ? "1" : match.group(4);
            }
            //If we matched fraction pattern and one has a match and the other doesn't, we've got bad input
            if (match.group(3) != null && match.group(4) != null && (match.group(3).equals("") ^ match.group(4).equals(""))) {
                throw new NumberFormatException("for input string '" + in + "' Should be x, y/z, or y/z.");
            }
        } else {
            throw new NumberFormatException("for input string '" + in + "' Should be x, y/z, or y/z.");
        }
        int w = Integer.parseInt(whole);
        int n = Integer.parseInt(numerator);
        int d = Integer.parseInt(denominator);

        return new Fraction(w, n, d);
    }
}
