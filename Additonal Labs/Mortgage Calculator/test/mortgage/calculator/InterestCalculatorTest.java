/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mortgage.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InterestCalculatorTest {

    InterestCalculator calc;
    double principal;
    double interestRate;
    InterestCalculator.CompoundingRates compoundRate;

    @Before
    public void setUp() {
        principal = 1000.0;
        interestRate = 5.0;
        compoundRate = InterestCalculator.CompoundingRates.MONTHLY;
        calc = new InterestCalculator(principal, compoundRate, interestRate);
    }

    @Test
    public void testGetTotalInterestPaid() {
        calc.applyInterest();
        assertEquals(0, calc.getTotalInterestPaid(), 0.0001);
        calc.makePayment(10);
        assertEquals(4.17, calc.getTotalInterestPaid(), 0.0001);
    }

    @Test
    public void testApplyInterest() {
        calc.applyInterest();
        assertEquals(1004.17, calc.getPrincipal(), 0.0001);
    }

    @Test
    public void testGetPrincipal() {
        assertEquals(1000, calc.getPrincipal(), 0.0001);
    }

    @Test
    public void testGetAnnualCompoundPeriods() {
        assertEquals(12, calc.getAnnualCompoundPeriods());
    }

    @Test
    public void testGetInterestRate() {
        assertEquals(5, calc.getInterestRate(), 0.0001);
    }

    @Test
    public void testMakePayment() {
        calc.makePayment(100);
        assertEquals(900, calc.getPrincipal(), 0.0001);
    }

    @Test
    public void testGetInterestSinceLastPayment() {
        calc.applyInterest();
        assertEquals(4.17, calc.getInterestSinceLastPayment(), 0.0001);
    }

}
