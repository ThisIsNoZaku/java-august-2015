/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mortgage.calculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MortgageCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter initial principal: $");
        int principal = scan.nextInt();
        System.out.print("Enter interest rate: %");
        double rate = scan.nextDouble();
        System.out.println("Enter payment interest compund periods: ");
        System.out.println("1- Annual");
        System.out.println("2- Quarterly");
        System.out.println("3- Monthly");
        System.out.println("4- Daily");
        int period = scan.nextInt();
        System.out.print("Enter additonal payment amount: $");
        double additionalPayment = scan.nextDouble();
        System.out.println();
        InterestCalculator.CompoundingRates compound = null;
        switch (period) {
            case 1:
                compound = InterestCalculator.CompoundingRates.ANNUALLY;
                break;
            case 2:
                compound = InterestCalculator.CompoundingRates.QUARTERLY;
                break;
            case 3:
                compound = InterestCalculator.CompoundingRates.MONTHLY;
                break;
            case 4:
                compound = InterestCalculator.CompoundingRates.DAILY;
                break;
        }
        InterestCalculator calc = new InterestCalculator(principal, compound, rate);
        int paymentCount = 0;
        while (calc.getPrincipal() > 0) {
            paymentCount++;
            calc.applyInterest();
            double interest = calc.getInterestSinceLastPayment();
            calc.makePayment(Math.min(additionalPayment, calc.getPrincipal()) + interest);
            System.out.println(String.format("Principal after %d payments: $%.2f", paymentCount, calc.getPrincipal()));
            System.out.println(String.format("Made payment of $%.2f", additionalPayment + interest));
            System.out.println(String.format("Interest accrued was $%.2f", interest));
            System.out.println(String.format("Total interest paid is $%.2f", calc.getTotalInterestPaid()));
            System.out.println();
        }

        double yearsToPay = (double) paymentCount / calc.getAnnualCompoundPeriods();
        String compounding = "";
        switch (compound) {
            case ANNUALLY:
                compounding = "annually";
                break;
            case DAILY:
                compounding = "daily";
                break;
            case MONTHLY:
                compounding = "monthly";
                break;
            case QUARTERLY:
                compounding = "quarterly";
                break;
        }
        System.out.println(String.format("It will take %.1f years paying $%.2f extra to pay off $%d at %.2f%% compounded %s.", yearsToPay, additionalPayment, principal, rate, compounding));
        System.out.println(String.format("The total amount paid will be $%.2f", principal + calc.getTotalInterestPaid()));
    }

}
