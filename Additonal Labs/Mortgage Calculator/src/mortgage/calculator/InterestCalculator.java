package mortgage.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {

    private BigDecimal principal;
    private final int annualCompoundPeriods;
    private final BigDecimal interestRate;
    private BigDecimal accruedInterest = BigDecimal.ZERO;
    private BigDecimal totalInterestPaid = BigDecimal.ZERO;

    public double getTotalInterestPaid() {
        return totalInterestPaid.doubleValue();
    }

    public InterestCalculator(double startingPrincipal, CompoundingRates compoundPeriods, double interestRate) {
        principal = new BigDecimal(startingPrincipal);
        annualCompoundPeriods = compoundPeriods.annualCompoundPeriods;
        this.interestRate = new BigDecimal(interestRate);
    }

    public double applyInterest() {
        //period adjusted rate / (periods / years) /100
        BigDecimal interestPercent = interestRate.divide(new BigDecimal(annualCompoundPeriods), 3, RoundingMode.HALF_EVEN).divide(new BigDecimal("100"));
        // principal * rate
        BigDecimal interestAmount = principal.multiply(interestPercent);
        principal = principal.add(interestAmount);
        accruedInterest = interestAmount;
        return interestAmount.doubleValue();
    }

    public double getPrincipal() {
        return principal.doubleValue();
    }

    public int getAnnualCompoundPeriods() {
        return annualCompoundPeriods;
    }

    public double getInterestRate() {
        return interestRate.doubleValue();
    }

    public static enum CompoundingRates {

        DAILY(365), MONTHLY(12), QUARTERLY(4), ANNUALLY(1);
        int annualCompoundPeriods;

        private CompoundingRates(int periods) {
            annualCompoundPeriods = periods;
        }
    }

    public void makePayment(double amount) {
        principal = principal.subtract(new BigDecimal(amount));
        totalInterestPaid = totalInterestPaid.add(new BigDecimal(amount).min(accruedInterest));
        accruedInterest = BigDecimal.ZERO;
    }

    public double getInterestSinceLastPayment() {
        return accruedInterest.doubleValue();
    }
}
