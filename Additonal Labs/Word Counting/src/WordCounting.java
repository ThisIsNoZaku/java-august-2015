
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class WordCounting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter some text to count the words:");
        String in = scan.nextLine();
        int wordCount = getWordCount(in);
        System.out.println(String.format("Contains %d words.", wordCount));
    }

    public static int getWordCount(String in) {
        return in.split("\\s+").length;
    }
}
