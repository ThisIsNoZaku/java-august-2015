
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class DecimalToRomanNumerals {

    private static final NavigableMap<Integer, String> romanNumerals = new TreeMap<>();

    static {
        romanNumerals.put(1, "I");
        romanNumerals.put(5, "V");
        romanNumerals.put(10, "X");
        romanNumerals.put(50, "L");
        romanNumerals.put(100, "C");
        romanNumerals.put(500, "D");
        romanNumerals.put(1000, "M");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter number to convert: ");
        int in = scan.nextInt();
        StringBuilder out = new StringBuilder();
        char[] digits = Integer.toString(in).toCharArray();
        for (int i = digits.length - 1; i >= 0; i--) {
            int number = Integer.parseInt(digits[i] + "");
            if (number == 4) {
                out.append(romanNumerals.get((int) Math.pow(10, i))).append(romanNumerals.get((int) Math.pow(10, i) * 5));
            } else if (number == 9) {
                out.append(romanNumerals.get((int) Math.pow(10, i))).append(romanNumerals.get((int) Math.pow(10, i) * 10));
            } else {
                for (int j = 0; j < number; j++) {
                    out.append(romanNumerals.get((int) Math.pow(10, i)));
                }
            }
        }
        System.out.println(out);
    }

}
