
import java.util.Map;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class WriteOutNumber {
    
    private static final Map<Integer, String> conversions = new TreeMap<>();
    
    static {
        conversions.put(1, "One");
        conversions.put(2, "Two");
        conversions.put(3, "Three");
        conversions.put(4, "Four");
        conversions.put(5, "Five");
        conversions.put(6, "Six");
        conversions.put(7, "Seven");
        conversions.put(8, "Eight");
        conversions.put(9, "Nine");
        conversions.put(10, "Ten");
        conversions.put(11, "Eleven");
        conversions.put(12, "Twelve");
        conversions.put(13, "Thirteen");
        conversions.put(14, "Fourteen");
        conversions.put(15, "Fifteen");
        conversions.put(16, "Sixteen");
        conversions.put(17, "Seventeen");
        conversions.put(18, "Eighteen");
        conversions.put(19, "Ninteen");
        conversions.put(20, "Twenty");
        conversions.put(30, "Thirty");
        conversions.put(40, "Forty");
        conversions.put(50, "Fifty");
        conversions.put(60, "Sixty");
        conversions.put(70, "Seventy");
        conversions.put(80, "Eighty");
        conversions.put(90, "Ninety");
        conversions.put(100, "Hundred");
        conversions.put(1_000, "Thousand");
        conversions.put(1_000_000, "Million");
    }
    
    public static String writeOut(int in) {
        if (in <= 0) {
            throw new IllegalArgumentException("Input must be positive.");
        }
        //If in is between 1 and 20, 1 token
        if (in <= 20) {
            return conversions.get(in);
        }
        String[] tokens;
        int scale = (int) Math.pow(10, Integer.toString(in).length() - 1);
        // If less than 100
        if (in < 100) {
            tokens = new String[2];
            tokens[0] = conversions.get((in / 10) * 10);
            tokens[1] = conversions.get(in % 10);
        } else if (in < 1000) {
            tokens = new String[3];
            tokens[0] = writeOut(in / 100);
            tokens[1] = conversions.get(scale);
            tokens[2] = in % 100 != 0 ? writeOut(in % 100) : null;
        } else {
            int[] segments = new int[(int) Math.ceil(Integer.toString(in).length() / 3.0)];
            tokens = new String[segments.length * 2];
            for (int i = segments.length - 1; i >= 0; i--) {
                scale = (int) Math.pow(1000, i);
                segments[i] = (int) (in / Math.pow(1000, i));
                if (segments[i] != 0) {
                    tokens[tokens.length - (i + 1) * 2] = writeOut(segments[i]);
                    tokens[tokens.length - (i + 1) * 2 + 1] = scale > 1 ? conversions.get(scale) : null;
                }
                in %= Math.pow(1000, i);
            }
        }
        
        StringBuilder out = new StringBuilder();
        for (String token : tokens) {
            if (token != null) {
                out.append(token).append(" ");
            }
        }
        return out.toString().replaceAll("\\s+", " ");
    }
    
    public static void main(String[] args) {
        for (int i = 1; i < 100_000_000; i++) {
            System.out.println(writeOut(i));
        }
    }
}
