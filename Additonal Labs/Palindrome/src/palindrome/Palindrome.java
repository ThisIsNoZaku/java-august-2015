/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palindrome;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Palindrome {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a string to test:");
        String in = sc.nextLine();
        String original = in.toLowerCase().replaceAll("\\s", "");

        StringBuilder reverse = new StringBuilder(original);
        String message;
        if (original.equals(reverse.reverse().toString())) {
            message = in + " is a palindrome.";
        } else {
            message = in + " is not a palindrome.";
        }
        System.out.println(message);
    }

}
