/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounts;

/**
 *
 * @author apprentice
 */
public class SavingsAccount extends Account {

    private double interestRate;
    private double accruedInterest;

    public SavingsAccount(int accountID, int ownerId) {
        super(accountID, ownerId);
    }

    public void applyInterest() {
        accruedInterest = funds * interestRate;
        funds += accruedInterest;
        availableFunds += accruedInterest;
    }
}
