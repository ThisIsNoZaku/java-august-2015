/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounts;

/**
 *
 * @author apprentice
 */
public class Account {

    private final int accountId;
    private final int ownerId;
    protected double funds;
    protected double availableFunds;
    private static final double AUTOMATIC_CLEAR_LIMIT = 10_000.0;

    public Account(int accountID, int ownerId) {
        this.accountId = accountID;
        this.ownerId = ownerId;
    }

    public int getAccountId() {
        return accountId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public double getTotalFunds() {
        return funds;
    }

    public double clearFunds(double amount) {
        double toClear = Double.min(funds - availableFunds, amount);
        availableFunds += toClear;
        return toClear;
    }

    public double widthdrawFunds(double withdrawalAmount) {
        withdrawalAmount = Double.min(withdrawalAmount, availableFunds);
        funds -= withdrawalAmount;
        availableFunds -= withdrawalAmount;
        return withdrawalAmount;
    }

    public boolean depositFunds(double depositAmount) {
        boolean fundsCleared = false;
        if (depositAmount < AUTOMATIC_CLEAR_LIMIT) {
            availableFunds += depositAmount;
            fundsCleared = true;
        }
        funds += depositAmount;
        return fundsCleared;
    }

    public double getAvailableFunds() {
        return availableFunds;
    }

    public static double getAUTOMATIC_CLEAR_LIMIT() {
        return AUTOMATIC_CLEAR_LIMIT;
    }

}
