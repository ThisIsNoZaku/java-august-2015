/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounts;

/**
 *
 * @author apprentice
 */
public class CheckingAccount extends Account {

    private static final double overdraftCharge = 10.00;

    public CheckingAccount(int accountID, int ownerId) {
        super(accountID, ownerId);
    }

    @Override
    public double widthdrawFunds(double withdrawalAmount) {
        withdrawalAmount = Double.min(availableFunds - 100, withdrawalAmount);
        availableFunds -= withdrawalAmount;
        funds -= withdrawalAmount;
        if (availableFunds < 0) {
            availableFunds -= overdraftCharge;
            funds -= overdraftCharge;
        }
        return withdrawalAmount;
    }

}
