/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rockpaperscissors;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaperScissors {

    private static final int MIN_ROUNDS = 1;
    private static final int MAX_ROUNDS = 10;

    private final static Random rand = new Random();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("How many rounds would you like to play? (1-10)");
        int rounds = sc.nextInt();
        if (rounds < MIN_ROUNDS || rounds > MAX_ROUNDS) {
            System.out.println("Sorry, you have to choose between 1 and 10.");
        }

        int playerWins = 0;
        int computerWins = 0;

        for (int i = 0; i < rounds; i++) {
            System.out.println("Choose your move:");
            System.out.println("1- Rock, 2- Paper, 3- Scissors");
            int userSelect = sc.nextInt();
            String userSelectName = "";
            switch (userSelect) {
                case 1:
                    userSelectName = "Rock";
                    break;
                case 2:
                    userSelectName = "Paper";
                    break;
                case 3:
                    userSelectName = "Scissors";
                    break;
            }
            int computerSelect = rand.nextInt(3) + 1;
            String computerSelectName = "";
            switch (computerSelect) {
                case 1:
                    computerSelectName = "Rock";
                    break;
                case 2:
                    computerSelectName = "Paper";
                    break;
                case 3:
                    computerSelectName = "Scissors";
                    break;
            }
            System.out.println("The computer chose " + computerSelectName);
            //If a side chooses Rock, change value of scissors to 0
            if (userSelect == 1) {
                computerSelect %= 3;
            }
            if (computerSelect == 1) {
                userSelect %= 3;
            }
            //Above allows all results to be calculated from these three numeric comparisons.
            if (userSelect == computerSelect) {
                System.out.println(String.format("Tie - Both players chose %s.", userSelectName));
            } else if (userSelect > computerSelect) {
                System.out.println(String.format("You Win - %s beats %s", userSelectName, computerSelectName));
                playerWins++;
            } else if (userSelect < computerSelect) {
                System.out.println(String.format("Computer Wins - %s beats %s", computerSelectName, userSelectName));
                computerWins++;
            }
        }

        System.out.println("You won " + playerWins + " times, lost " + computerWins + " times and tied " + (rounds - playerWins - computerWins) + " times.");
        if (playerWins > computerWins) {
            System.out.println("You beat the computer.");
        } else if (playerWins < computerWins) {
            System.out.println("Sorry, the computer beat you.");
        } else {
            System.out.println("Looks like a tie.");
        }
    }
}
