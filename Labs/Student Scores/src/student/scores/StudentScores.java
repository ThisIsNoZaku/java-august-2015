/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student.scores;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author apprentice
 */
public class StudentScores {

    private int idCounter = 0;
    private final Map<String, List<Integer>> students = new HashMap<>();

    public Map<String, List<Integer>> getStudentList() {
        return Collections.unmodifiableMap(students);
    }

    public void addStudent(String name) {
        idCounter++;
        students.put(name, new ArrayList<>());
    }

    public void removeStudent(String name) {
        students.remove(name);
    }

    public double getStudentAverage() {
        if (students.isEmpty()) {
            return 0.0;
        }
        double total = 0;
        for (List<Integer> scores : students.values()) {
            if (!scores.isEmpty()) {
                total += scores.stream().mapToDouble(i -> i).average().getAsDouble();
            }
        }
        return total / students.size();
    }

    public List<Entry<String, List<Integer>>> getLowestScoringStudents() {
        if (students.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Double lowestAverage = null;
        List<Entry<String, List<Integer>>> lowestAvgKeys = new LinkedList<>();
        for (Entry<String, List<Integer>> entry : students.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                double avg = entry.getValue().stream().mapToDouble(Integer::intValue).average().getAsDouble();
                if (lowestAverage == null || avg < lowestAverage) {
                    lowestAverage = avg;
                    lowestAvgKeys.clear();
                }
                if (avg == lowestAverage) {
                    lowestAvgKeys.add(entry);
                }
            }
        }

        return lowestAvgKeys;
    }

    public List<Entry<String, List<Integer>>> getHighestScoringStudents() {
        if (students.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Double highestAverage = null;
        List<Entry<String, List<Integer>>> highestAvgKeys = new LinkedList<>();
        for (Entry<String, List<Integer>> entry : students.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                double avg = entry.getValue().stream().mapToDouble(Integer::intValue).average().getAsDouble();
                if (highestAverage == null || avg > highestAverage) {
                    highestAverage = avg;
                    highestAvgKeys.clear();
                }
                if (avg == highestAverage) {
                    highestAvgKeys.add(entry);
                }
            }
        }

        return highestAvgKeys;
    }
}
