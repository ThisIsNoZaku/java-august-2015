package student.scores;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map;
import java.util.Map.Entry;

public class App {
    
    private static final Map<Integer, String> menuOptions = new HashMap<>();
    private static final Map<Integer, Runnable> menuMethods = new HashMap<>();
    
    private static void addMenuOption(String message, Runnable method) {
        int key = menuOptions.size() + 1;
        menuOptions.put(key, key + " - " + message);
        menuMethods.put(key, method);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final Random rand = new Random(0);
        
        ConsoleIO io = new ConsoleIO();
        StudentScores students = new StudentScores();
        
        students.addStudent("Adam");
        students.addStudent("Brittney");
        students.addStudent("Charles");
        students.addStudent("Donna");
        students.addStudent("Edwars");
        for (Entry<String, List<Integer>> entry : students.getStudentList().entrySet()) {
            for (int i = 0; i < 5; i++) {
                entry.getValue().add((int) (Math.random() * 100));
            }
        }
        
        addMenuOption("Display Student Scores", (Runnable) () -> {
            for (Entry<String, List<Integer>> entry : students.getStudentList().entrySet()) {
                io.print(String.format("%s  - ", entry.getKey()));
                List<Integer> scores = entry.getValue();
                for (int score : scores) {
                    io.print(String.format("%-4d", score));
                }
                io.print(String.format(" - Avg: %.2f ", entry.getValue().stream().mapToInt(Integer::intValue).average().orElse(0.0)));
                
                io.println("");
            }
            io.println(String.format("Class Avg: %.2f", students.getStudentAverage()));
            io.promptForString("Press enter to continue...");
        }
        );
        addMenuOption(
                "Display Lowest Scoring Students", (Runnable) () -> {
                    for (Entry<String, List<Integer>> student : students.getLowestScoringStudents()) {
                        io.print(String.format("%s ", student));
                        for (Integer grade : student.getValue()) {
                            io.print(String.format("%-4d", grade));
                        }
                        io.print(" Avg. " + student.getValue().stream().mapToInt(Integer::intValue).average().orElse(0.0));
                    }
                    io.println("");
                    io.promptForString("Press enter to continue...");
                }
        );
        addMenuOption(
                "Display Highest Scoring Students", (Runnable) () -> {
                    for (Entry<String, List<Integer>> student : students.getHighestScoringStudents()) {
                        io.print(String.format("%s ", student));
                        for (Integer grade : student.getValue()) {
                            io.print(String.format("%-4d", grade));
                        }
                        io.print(" Avg. " + student.getValue().stream().mapToInt(Integer::intValue).average().orElse(0.0));
                    }
                    io.println("");
                    io.promptForString("Press enter to continue...");
                }
        );
        addMenuOption(
                "Add Student", (Runnable) () -> {
                    String name = io.promptForString("Enter new student name: ");
                    String confirm = "";
                    while (name.equals("") && (!confirm.equals("y") && !confirm.equals("n"))) {
                        if (students.getStudentList().containsKey(name)) {
                            confirm = io.promptForString("Student " + name + " already exists. Do you want to overwrite? y/n");
                        } else {
                            confirm = "y";
                        }
                    }
                    if (confirm.equals("y")) {
                        io.promptForString("New student added. Press enter to continue...");
                    }
                }
        );
        addMenuOption(
                "Remove Student", (Runnable) () -> {
                    String name = io.promptForString("Enter name of student to remove: ");
                    String confirm = "";
                    while (!confirm.equalsIgnoreCase("y") && !confirm.equalsIgnoreCase("n")) {
                        if (students.getStudentList().containsKey(name)) {
                            
                            confirm = io.promptForString(String.format("Remove student %s? Are you sure? y/n", students.getStudentList().get(name)));
                            if (confirm.equalsIgnoreCase("y")) {
                                students.removeStudent(name);
                            }
                        } else {
                            io.println("No student name " + name + ". Try again? y/n");
                        }
                    }
                });
        addMenuOption(
                "Exit", (Runnable) () -> {
                    System.exit(0);
                }
        );
        int in = 0;
        
        while (true) {
            StringBuilder menuPrompt = new StringBuilder("Choose an option: ");
            menuOptions.keySet().stream().forEach((menuIndex) -> {
                menuPrompt.append(System.getProperty("line.separator")).append(menuOptions.get(menuIndex));
            });
            in = io.promtForIntInRange(menuPrompt.toString(), 1, menuOptions.keySet().size());
            menuMethods.get(in).run();
        }
    }
}
