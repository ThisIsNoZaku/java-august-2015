package student.scores;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StudentScoresTest {

    StudentScores scores;

    @Before
    public void setUp() {
        scores = new StudentScores();
        scores.addStudent("Adam");
        scores.addStudent("Brittney");
        scores.addStudent("Chad");
        scores.addStudent("Danielle");
        scores.addStudent("Edward");
        scores.addStudent("Francine");
        for (List<Integer> student : scores.getStudentList().values()) {
            for (int i = 0; i < 5; i++) {
                student.add(i * 5);
            }
        }
    }

    @Test
    public void testGetStudentList() {
        Map<String, List<Integer>> target = new HashMap<String, List<Integer>>();
        target.put("Adam", new ArrayList<>());
        target.put("Brittney", new ArrayList<>());
        target.put("Chad", new ArrayList<>());
        target.put("Danielle", new ArrayList<>());
        target.put("Edward", new ArrayList<>());
        target.put("Francine", new ArrayList<>());
        for (List<Integer> student : target.values()) {
            for (int i = 0; i < 5; i++) {
                student.add(i * 5);
            }
        }

        for (String name : target.keySet()) {
            assertTrue(
                    scores.getStudentList().containsKey(name) && scores.getStudentList().get(name).equals(target.get(name))
            );
        }
    }

    @Test
    public void testAddStudent() {
        scores.addStudent("Gregory");

        assertTrue(scores.getStudentList().containsKey("Gregory"));
    }

    @Test
    public void testRemoveStudent() {
        Map<String, List<Integer>> target = new HashMap<>();
        target.put("Brittney", new ArrayList<>());
        target.put("Chad", new ArrayList<>());
        target.put("Danielle", new ArrayList<>());
        target.put("Edward", new ArrayList<>());
        target.put("Francine", new ArrayList<>());
        for (List<Integer> student : target.values()) {
            for (int i = 0; i < 5; i++) {
                student.add(i * 5);
            }
        }

        scores.removeStudent("Adam");
        for (String name : Stream.concat(target.keySet().stream(), scores.getStudentList().keySet().stream()).collect(Collectors.toList())) {
            assertTrue(target.containsKey(name));
            assertTrue(scores.getStudentList().containsKey(name));
            assertTrue(scores.getStudentList().get(name).equals(target.get(name)));
        }
    }

    @Test
    public void testGetStudentAverage() {
        assertEquals(10.0, scores.getStudentAverage(), .0001);
    }
}
