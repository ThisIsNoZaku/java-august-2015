package com.thesoftwareguild.luckysevens;

import java.util.Map;
import luckysevens.LuckySevens;
import luckysevens.Player;
import luckysevens.RollResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/"})
public class HelloController {

    @RequestMapping(value = "/play", method = RequestMethod.POST, consumes = {"application/json"})
    @ResponseBody
    public Map<Integer, RollResult> sayHi(@RequestBody Integer money, Model model) {
        LuckySevens ls = new LuckySevens(money);
        ls.start();

        return ls.getRolls();
    }
}
