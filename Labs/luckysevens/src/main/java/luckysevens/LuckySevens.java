package luckysevens;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    private int playerMoney;
    private int playerBestMoney = 0;
    private int bestMoneyRoll = 0;
    private final Map<Integer, RollResult> rolls = new HashMap<>();
    Die d6 = Die.D6;

    public Map<Integer, RollResult> getRolls() {
        return Collections.unmodifiableMap(rolls);
    }

    public int getPlayerMoney() {
        return playerMoney;
    }

    public int getPlayerBestMoney() {
        return playerBestMoney;
    }

    public int getBestMoneyRoll() {
        return bestMoneyRoll;
    }

    public int getNumRolls() {
        return rolls.size();
    }

    public LuckySevens(int startingMoney) {
        this.playerMoney = startingMoney;
    }

    public void start() {
        d6 = new Die(1, 6);

        int rollCount = 0;
        while (playerMoney > 0) {
            roll(++rollCount);
        }
    }

    public void roll(int rollNumber) {
        int roll = d6.roll(2);
        if (roll == 7) {
            playerMoney += 4;
        } else {
            playerMoney--;
        }

        if (playerMoney > playerBestMoney) {
            playerBestMoney = playerMoney;
            bestMoneyRoll = rollNumber;
        }
        rolls.put(rollNumber, new RollResult(rollNumber, roll, playerMoney));
    }
}
