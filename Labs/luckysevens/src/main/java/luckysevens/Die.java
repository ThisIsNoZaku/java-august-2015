/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Die {

    private static final Random rand = new Random();
    //Predefined dice.
    public static final Die D4 = new Die(1, 4);
    public static final Die D6 = new Die(1, 6);
    public static final Die D8 = new Die(1, 8);
    public static final Die D10 = new Die(1, 10);
    public static final Die D12 = new Die(1, 12);
    public static final Die D20 = new Die(1, 20);

    private final int[] values;

    /**
     * Creates a die whose values are all integers between [code]min[/code] and
     * [code]max[/code].
     *
     * @param min
     * @param max
     */
    public Die(int min, int max) {
        values = new int[max - min + 1];
        for (int i = 0; i < values.length; i++) {
            values[i] = min + i;
        }
    }

    /**
     * Creates a die whose possible values are contained in the given array.
     *
     * @param values
     */
    public Die(int[] values) {
        this.values = values;
    }

    /**
     * Shorthand equivalent of roll(1).
     *
     * @return the die result
     */
    public int roll() {
        return roll(1);
    }

    /**
     * Simulates rolling the die the given number of time, selecting one random
     * value from the die values each time and returning the sum.
     *
     * @param rollCount
     * @return the combined dice values
     */
    public int roll(int rollCount) {
        int sum = 0;
        for (int i = 0; i < rollCount; i++) {
            sum += values[rand.nextInt(values.length)];
        }
        return sum;
    }
}
