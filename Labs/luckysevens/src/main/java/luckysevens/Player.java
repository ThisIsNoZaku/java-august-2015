/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

/**
 *
 * @author apprentice
 */
public class Player {

    private int money;
    private int bestMoney;

    public void setMoney(int money) {
        this.money = money;
        if (money > bestMoney) {
            bestMoney = money;
        }
    }

    public int getMoney() {
        return money;
    }

    public int getBestMoney() {
        return bestMoney;
    }
}
