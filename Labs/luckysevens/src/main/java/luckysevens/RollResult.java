/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

/**
 *
 * @author apprentice
 */
public class RollResult {

    private final int rollNumber;
    private final int rollResult;
    private final int remainingMoney;

    public RollResult(int rollNumber, int rollResult, int remainingMoney) {
        this.rollNumber = rollNumber;
        this.rollResult = rollResult;
        this.remainingMoney = remainingMoney;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public int getRollResult() {
        return rollResult;
    }

    public int getRemainingMoney() {
        return remainingMoney;
    }
    
    
}
