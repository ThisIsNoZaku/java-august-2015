$().ready(function () {
    $("#play-button").on("click", function (e) {
        e.preventDefault();

        $.ajax("play", {
            method: "POST",
            data: JSON.stringify($("#in").val()),
            contentType: "application/json",
            accepts: "application/json"
        }).done(function (data) {
            $.each(data, function (index, element) {
                var $out = $("#out");
                var $entry = $("<div>", {
                    class: "row"
                });
                var rollNumText = "Roll #:" + element.rollNumber;
                var $rollNumRow = $("<div>", {
                    class: "row"
                }).append($("<div>", {
                    class: "md-col-2 md-col-offset-4"
                }).append(rollNumText));
                $entry.append($rollNumRow);

                var rollResultText = "Roll:" + element.rollResult;
                var $rollResultRow = $("<div>", {
                    class: "row"
                }).append($("<div>", {
                    class: "md-col-2 md-col-offset-4"
                }).append(rollResultText));
                $entry.append($rollResultRow);

                var rollMoneyText = "Remaining Money:" + element.remainingMoney;
                var $rollMoneyRow = $("<div>", {
                    class: "row"
                }).append($("<div>", {
                    class: "md-col-2 md-col-offset-4"
                }).append(rollMoneyText));
                $entry.append($rollMoneyRow);

                $out.append($entry).append("<br/>");
            });
        });
    });
});
