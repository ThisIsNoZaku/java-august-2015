/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorizer;

import org.apache.commons.math3.primes.Primes;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class FactorizerTest {

    public FactorizerTest() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFactorize() {
        for (int i = 2; i <= 10_000; i++) {
            Factorizer f = new Factorizer(i);
            //Test factors
            Assert.assertThat(f.getFactors(), is(equalTo(Primes.primeFactors(i))));
            //Test is prime
            Assert.assertThat(f.isPrime(), is(Primes.isPrime(i)));
            //Test is perfect
            int sum = Primes.primeFactors(i).stream().mapToInt(n -> n).sum() + 1;
            Assert.assertThat(f.isPerfect(), is(sum == i));
        }
    }
}
