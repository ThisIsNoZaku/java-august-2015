/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author apprentice
 */
public class Factorizer {

    private final int number;
    private final List<Integer> factors;
    private final boolean isPerfect;
    private final boolean isPrime;

    public Factorizer(int number) {
        this.number = number;
        List<Integer> factors = calculateFactors(number);
        Collections.sort(factors);
        this.factors = factors;
        isPrime = factors.size() == 1 && factors.contains(number);
        int sumOfFactors = factors.stream().mapToInt(i -> i).sum() + 1;
        isPerfect = sumOfFactors == number;
    }

    private static List<Integer> calculateFactors(int in) {
        List<Integer> factors = new ArrayList<>();

        int counter = 2;
        while (counter <= in) {
            while (in % counter == 0) {
                factors.add(counter);
                in /= counter;
            }
            if (counter == 2) {
                counter++;
            } else {
                counter += 2;
            }
        }
        return new ArrayList(factors);
    }

    /**
     * Returns all of the prime factors of the number. May contain duplicates.
     *
     * @return the prime factors
     */
    public List<Integer> getFactors() {
        Collections.sort(factors);
        return factors;
    }

    /**
     * Returns if this number is perfect or nor.
     *
     * @return if number is perfect
     */
    public boolean isPerfect() {
        return isPerfect;
    }

    /**
     * Returns if this number is prime or not.
     *
     * @return if numer is prime
     */
    public boolean isPrime() {
        return isPrime;
    }

    /**
     * Returns the original number.
     *
     * @return the number
     */
    public int getNumber() {
        return number;
    }
}
