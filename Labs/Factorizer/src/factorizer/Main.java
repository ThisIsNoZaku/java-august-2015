package factorizer;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number to factorize: ");

        int original = sc.nextInt();
        Factorizer f = new Factorizer(original);

        System.out.println("The factors of " + original + " are:");
        f.getFactors().stream().forEach((factor) -> {
            System.out.println(factor);
        });
        System.out.println();

        if (f.isPerfect()) {
            System.out.println(original + " is a perfect number.");
            System.out.println();
        } else {
            System.out.println(original + " is not a perfect number.");
            System.out.println();
        }

        if (f.isPrime()) {
            System.out.println(original + " is a prime number.");
            System.out.println("");
        } else {
            System.out.println(original + " is not a prime number.");
            System.out.println("");
        }
    }
}
