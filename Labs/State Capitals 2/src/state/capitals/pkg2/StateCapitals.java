package state.capitals.pkg2;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class StateCapitals {

    private static final Map<String, Capital> stateCapitals = new TreeMap<>();

    static {
        stateCapitals.put("Alabama", new Capital("Montgomery", 400000, 200));
        stateCapitals.put("Alaska", new Capital("Juneau", 75000, 75));
        stateCapitals.put("Arizona", new Capital("Phoenix", 200000, 100));
        stateCapitals.put("Arkansas", new Capital("Little Rock", 75000, 60));
        stateCapitals.put("California", new Capital("Sacramento", 500000, 125));
        stateCapitals.put("Colorado", new Capital("Denver", 600000, 250));
        stateCapitals.put("Connecticut", new Capital("Hartford", 250000, 90));
        stateCapitals.put("Delaware", new Capital("Dover", 200000, 50));
        stateCapitals.put("Florida", new Capital("Tallahassee", 150000, 65));
        stateCapitals.put("Georgia", new Capital("Atlanta", 750000, 210));
        stateCapitals.put("Hawaii", new Capital("Honolulu", 200000, 80));
        stateCapitals.put("Idaho", new Capital("Boise", 100000, 50));
        stateCapitals.put("Illinois", new Capital("Springfield", 175000, 100));
        stateCapitals.put("Indiana", new Capital("Indianapolis", 250000, 120));
        stateCapitals.put("Iowa", new Capital("Des Moines", 90000, 75));
        stateCapitals.put("Kansas", new Capital("Topeka", 100000, 130));
        stateCapitals.put("Kentucky", new Capital("Frankfort", 120000, 90));
    }

    public static void main(String[] args) {
        System.out.println("STATES:");
        System.out.println("=======");
        for (String state : stateCapitals.keySet()) {
            System.out.println(state);
        }
        System.out.println();
        System.out.println("CAPITALS:");
        System.out.println("=========");
        for (String state : stateCapitals.keySet()) {
            System.out.println(stateCapitals.get(state));
        }
        System.out.println();
        System.out.println("Get all capitals with population at least: ");
        Scanner in = new Scanner(System.in);
        int population = in.nextInt();

        System.out.println("STATE AND CAPITAL:");
        System.out.println("==================");
        System.out.println("ALL CAPITALS WITH POPULATION GREATER THAN " + population + ":");
        for (String state : stateCapitals.keySet()) {
            if (stateCapitals.get(state).getPopulation() >= population) {
                System.out.println(String.format("%s - %s", state, stateCapitals.get(state)));
            }
        }
    }
}
