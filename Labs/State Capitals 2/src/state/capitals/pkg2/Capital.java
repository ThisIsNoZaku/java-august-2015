/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state.capitals.pkg2;

/**
 *
 * @author apprentice
 */
class Capital {

    private final String name;
    private int population;
    private int squareMileage;

    public Capital(String name, int population, int squareMileage) {
        this.name = name;
        this.population = population;
        this.squareMileage = squareMileage;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void setSquareMileage(int squareMileage) {
        this.squareMileage = squareMileage;
    }

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    public int getSquareMileage() {
        return squareMileage;
    }

    @Override
    public String toString() {
        return name + " | Pop: " + population + " | Area: " + squareMileage + " sq mi.";
    }
}
