package gamebot.luckysevens;

import gamebot.Game;
import gamebot.Gamebot;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens implements Game {

    private static final String name = "Lucky Sevens";
    int playerMoney;
    int playerBestMoney = 0;
    int bestMoneyRoll = 0;
    int numRolls = 0;
    Die d6 = Die.D6;

    public int getPlayerMoney() {
        return playerMoney;
    }

    public int getPlayerBestMoney() {
        return playerBestMoney;
    }

    public int getBestMoneyRoll() {
        return bestMoneyRoll;
    }

    public int getNumRolls() {
        return numRolls;
    }

    public LuckySevens() {

    }

    public void run() {
        System.out.println("Enter starting money.");
        playerMoney = Gamebot.in.nextInt();
        Gamebot.in.nextLine();
        d6 = new Die(1, 6);

        while (playerMoney > 0) {
            roll();
            System.out.println(String.format("You have $%d left.", playerMoney));

        }
        System.out.println("Sorry, you're out of money after " + numRolls + " rolls.");
        System.out.println("You should have quit after " + bestMoneyRoll + ", when you had $" + playerBestMoney + ".");
    }

    public void roll() {
        numRolls++;
        int roll = d6.roll(2);
        System.out.print(String.format("You rolled a %d - ", roll));
        if (roll == 7) {
            System.out.print("You won $4!    ");
            playerMoney += 4;
        } else {
            System.out.print("You lost $1... ");
            playerMoney--;
        }

        if (playerMoney > playerBestMoney) {
            playerBestMoney = playerMoney;
            bestMoneyRoll = numRolls;
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
