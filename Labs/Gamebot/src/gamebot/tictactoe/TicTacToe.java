package gamebot.tictactoe;

import gamebot.Game;
import java.util.Scanner;

public class TicTacToe implements Game {

    private static final char[] players = new char[]{
        'x', 'o'
    };
    private static final String name = "Tic Tac Toe";

    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        char[][] gameboard = new char[3][3];

        int currentPlayer = 0;

        for (int i = 0; i < 9; i++) {
            gameboard[(int) Math.ceil(i / 3)][i % 3] = ((i + 1) + "").charAt(0);
        }
        while (determineWinner(gameboard) == ' ' && hasRemainingMoves(gameboard, 'x', 'o')) {
            printBoard(gameboard);
            System.out.println("");
            System.out.print("Player " + players[currentPlayer] + ", choose a space: ");
            int selectedPosition = scan.nextInt();
            if (selectedPosition < 1 || selectedPosition > 9) {
                System.out.println("Select an unoccupied space between 1 and 9.");
                continue;
            }

            int[] position = positionToCoordinate(selectedPosition);

            if (gameboard[position[0]][position[1]] != 'x' && gameboard[position[0]][position[1]] != 'o') {
                gameboard[position[0]][position[1]] = players[currentPlayer];
                currentPlayer = (++currentPlayer) % 2;
            } else {
                System.out.println("That space is already take.");
            }
        }
        if (determineWinner(gameboard) != ' ') {
            System.out.println("Player " + players[(++currentPlayer) % 2] + " won!");
        } else {
            System.out.println("Tie!");
        }
    }

    private static void printBoard(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                System.out.print(board[row][column] + " ");
            }
            System.out.println();
        }
    }

    private static char determineWinner(char[][] board) {
        char winner = ' ';
        for (int i = 0; i < 3; i++) {
            //Check vertical
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                winner = board[i][0];
            } //Check horizontal
            else if (board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                winner = board[0][i];
            } //Check left to right
            else if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
                winner = board[0][0];
            } else if (board[2][0] == board[1][1] && board[1][1] == board[0][2]) {
                winner = board[1][1];
            }
        }

        return winner;
    }

    private static int[] positionToCoordinate(int position) {
        position -= 1;
        return new int[]{
            (int) Math.floor(position / 3), position % 3
        };
    }

    @Override
    public String getName() {
        return name;
    }

    private boolean hasRemainingMoves(char[][] board, char player1, char player2) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != player1 && board[i][j] != player2) {
                    return true;
                }
            }
        }
        return false;
    }
}
