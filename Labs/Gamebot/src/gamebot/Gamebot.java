/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamebot;

import gamebot.blackjack.Blackjack;
import gamebot.hangman.Hangman;
import gamebot.luckysevens.LuckySevens;
import gamebot.rps.RockPaperScissors4;
import gamebot.tictactoe.TicTacToe;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Gamebot {

    public static final Scanner in = new Scanner(System.in);
    private List<Game> games = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Gamebot bot = new Gamebot();
        bot.games.add(new RockPaperScissors4());
        bot.games.add(new Hangman());
        bot.games.add(new LuckySevens());
        bot.games.add(new Blackjack());
        bot.games.add(new TicTacToe());

        boolean run = true;
        while (run) {
            bot.printGameMenu();
            Game game = bot.selectGame();
            if (game != null) {
                game.run();
            } else {
                run = false;
            }
        }
    }

    public void printGameMenu() {
        for (int i = 0; i <= games.size(); i++) {
            if (i < games.size()) {
                System.out.println(String.format("%d- %s", i + 1, games.get(i).getName()));
            } else {
                System.out.println(String.format("%d- Exit", i + 1));
            }
        }
    }

    public Game selectGame() {
        System.out.println("Select a game to start");
        int selection = -1;
        while (selection < 0 || selection > games.size()) {
            selection = in.nextInt() - 1;
            if (selection == games.size()) {
                return null;
            }
        }
        in.nextLine();
        return games.get(selection);
    }
}
