package gamebot.blackjack;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {

    private final List<String> cards = new ArrayList<>();

    public String draw() {
        Random rand = new Random();
        return cards.remove(rand.nextInt(cards.size()));
    }

    public Deck shuffle() {
        cards.clear();
        for (int i = 2; i <= 13; i++) {
            for (int count = 0; count < 4; count++) {
                switch (i) {
                    case 1:
                        cards.add("A");
                        break;
                    case 11:
                        cards.add("J");
                        break;
                    case 12:
                        cards.add("Q");
                        break;
                    case 13:
                        cards.add("K");
                        break;
                    default:
                        cards.add(i + "");
                }
            }
        }
        return this;
    }

}
