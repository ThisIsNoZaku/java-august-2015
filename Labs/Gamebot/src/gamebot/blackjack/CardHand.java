package gamebot.blackjack;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CardHand {

    private final List<String> cards = new ArrayList<>();
    private int handValue = 0;

    public void addCard(String card) {
        cards.add(card);
        calculateHandValue();
    }

    public void removeCard(String card) {
        cards.remove(card);
        calculateHandValue();
    }

    private void calculateHandValue() {
        int value = 0;
        int aceCount = 0;
        for (String card : cards) {
            switch (card) {
                case "A":
                    aceCount++;
                    break;
                case "J":
                case "Q":
                case "K":
                    value += 10;
                    break;
                default:
                    value += Integer.parseInt(card);
            }
        }
        for (int i = 1; i <= aceCount; i++) {
            if (value < (Blackjack.BUST_LIMIT - (11 + aceCount - i))) {
                value += 11;
            } else {
                value += 1;
            }
        }
        handValue = value;
    }

    public int getHandValue() {
        return handValue;
    }

    public List<String> getCards() {
        return Collections.unmodifiableList(cards);
    }
}
