package gamebot.blackjack;

import gamebot.Game;
import gamebot.Gamebot;
import java.util.Collection;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Blackjack implements Game {

    private static final String name = "Blackjack";
    public static final int STARTING_HAND_SIZE = 2;
    public static final int BUST_LIMIT = 21;
    public static final int DEALER_HIT_LIMIT = 16;

    @Override
    public void run() {

        Scanner scan = Gamebot.in;
        Deck deck = new Deck();
        deck.shuffle();
        CardHand playerHand = new CardHand();
        CardHand dealerHand = new CardHand();

        for (int i = 0; i < STARTING_HAND_SIZE; i++) {
            playerHand.addCard(deck.draw());
            dealerHand.addCard(deck.draw());
        }

        System.out.println("Dealer has " + dealerHand.getCards().get(0) + " showing.");

        while (playerHand.getHandValue() <= BUST_LIMIT) {
            displayHand("Your", playerHand);
            System.out.println("Your total is " + playerHand.getHandValue());
            System.out.println("Do you want to \"hit\" or \"stay\"?");

            String move = scan.nextLine();
            if (move.equalsIgnoreCase("hit")) {
                String drawnCard = deck.draw();
                System.out.println("You drew a " + drawnCard);
                playerHand.addCard(drawnCard);
            } else {
                break;
            }
        };

        displayHand("Dealer", dealerHand);

        while (dealerHand.getHandValue() <= DEALER_HIT_LIMIT && playerHand.getHandValue() <= BUST_LIMIT) {
            System.out.println("Dealer will hit.");

            String drawnCard = deck.draw();
            dealerHand.addCard(drawnCard);

            displayHand("Dealer", dealerHand);
            System.out.println("Dealer total is " + dealerHand.getHandValue());
        }
        System.out.println("Dealer stays.");
        System.out.println();

        System.out.println("Your total is " + playerHand.getHandValue());
        System.out.println("Dealer total is " + dealerHand.getHandValue());
        boolean playerWin = false;
        boolean dealerWin = false;
        if (playerHand.getHandValue() <= BUST_LIMIT) {
            if (dealerHand.getHandValue() > BUST_LIMIT || playerHand.getHandValue() > dealerHand.getHandValue()) {
                playerWin = true;
            } else if (dealerHand.getHandValue() > playerHand.getHandValue()) {
                dealerWin = true;
            }
        } else {
            dealerWin = true;
        }
        if (playerWin) {
            System.out.println("YOU WIN");
        } else if (dealerWin) {
            System.out.println("DEALER WINS");
        } else {
            System.out.println("PUSH!");
        }
    }

    private static void displayHand(String player, CardHand hand) {
        System.out.print(player + " hand is: ");
        for (String card : hand.getCards()) {
            System.out.print(card + " ");
        }
        System.out.println();
    }

    @Override
    public String getName() {
        return name;
    }
}
