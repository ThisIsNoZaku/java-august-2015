package gamebot.hangman;

import gamebot.Game;
import gamebot.Gamebot;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Hangman implements Game {

    private static final String name = "Hangman";
    private static final int MAX_MISSED_GUESSES = 7;
    private static final String[] words = new String[]{
        "Guitar", "Centaur", "Cultivar", "Scimitar", "Sitar"
    };

    @Override
    public void run() {
        Random rand = new Random();
        boolean play = true;
        while (play) {

            String word = words[rand.nextInt(words.length)];
            Set<Character> guessedLetters = new HashSet<>();
            List<Boolean> shown = new ArrayList<>(word.length());
            int guessCount = 0;

            for (int i = 0; i < word.length(); i++) {
                shown.add(false);
            }

            while (shown.contains(false) && guessCount <= MAX_MISSED_GUESSES) {
                System.out.print("Word: ");
                for (int i = 0; i < word.length(); i++) {
                    if (guessedLetters.contains(word.toLowerCase().charAt(i))) {
                        System.out.print(word.charAt(i) + " ");
                    } else {
                        System.out.print("_ ");
                    }
                }
                System.out.println();

                System.out.print("Misses: ");
                guessedLetters.stream().filter((guess) -> (!word.contains(guess + ""))).forEach((guess) -> {
                    System.out.print(guess + " ");
                });
                System.out.println();

                System.out.print("Guess: ");
                char guess = Gamebot.in.nextLine().charAt(0);
                if (!guessedLetters.contains(guess)) {
                    guessedLetters.add(guess);
                } else {
                    guessCount++;
                }
                for (int i = 0; i < word.toCharArray().length; i++) {
                    if (guessedLetters.contains(word.toLowerCase().toCharArray()[i])) {
                        shown.set(i, Boolean.TRUE);
                    }
                }
            }
            if (guessCount > MAX_MISSED_GUESSES) {
                System.out.println("You've run out of guesses.");
            }

            System.out.print("Play again? y/n ");
            String in = Gamebot.in.nextLine();

            if (in.equals("n")) {
                play = false;
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
