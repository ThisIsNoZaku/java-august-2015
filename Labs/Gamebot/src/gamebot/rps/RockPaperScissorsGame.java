package gamebot.rps;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class RockPaperScissorsGame {

    private RockPaperScissorsGame() {

    }

    /**
     * Returns a new RockPaperScissorsGame.
     *
     * @return
     */
    public static RockPaperScissorsGame startNewGame() {
        return new RockPaperScissorsGame();
    }

    public int getPlayerAWins() {
        return playerAWins;
    }

    public int getPlayerBWins() {
        return playerBWins;
    }

    private int playerAWins = 0;
    private int playerBWins = 0;

    public GameResult playRound(Move playerAMove, Move PlayerBMove) {
        if (playerAMove.defeats(PlayerBMove)) {
            playerAWins++;
            return GameResult.PLAYER_A_WIN;
        } else if (PlayerBMove.defeats(playerAMove)) {
            playerBWins++;
            return GameResult.PLAYER_B_WIN;
        } else {
            return GameResult.TIE;
        }
    }

    public static enum GameResult {

        PLAYER_A_WIN, PLAYER_B_WIN, TIE
    }

    public static class Move {

        public static final Map<String, Move> moves;

        static {
            Map<String, Move> theMoves = new HashMap<>();
            Move rock = new Move("Rock");
            theMoves.put(rock.name, rock);
            Move paper = new Move("Paper");
            theMoves.put(paper.name, paper);
            Move scissors = new Move("Scissors");
            theMoves.put(scissors.name, scissors);

            rock.defeats.add(scissors);
            paper.defeats.add(rock);
            scissors.defeats.add(paper);

            moves = Collections.unmodifiableMap(theMoves);
        }

        private final String name;
        private final Set<Move> defeats = new HashSet<>();

        private Move(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        /**
         * Returns true if this move defeats the given move.
         *
         * @param other
         * @return
         */
        public boolean defeats(Move other) {
            return this.defeats.contains(other);
        }
    }
}
