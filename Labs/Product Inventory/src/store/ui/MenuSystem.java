package store.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class MenuSystem {

    private Map<Integer, MenuItem> menuItems = new HashMap<>();

    public MenuSystem(Map<String, Runnable> menu) {
        int count = 0;
        for (Entry<String, Runnable> entry : menu.entrySet()) {
            menuItems.put(count++, new MenuItem(entry.getKey(), entry.getValue()));
        }
    }

    public Map<String, Integer> getItemKeys() {
        return menuItems.entrySet().stream().collect(Collectors.toMap(e -> e.getValue().getMessage(), e -> e.getKey()));
    }

    public void performMenuAction(int menuItemId) {
        menuItems.get(menuItemId).getAction().run();
    }

    private class MenuItem {

        private final String message;
        private final Runnable action;

        public String getMessage() {
            return message;
        }

        public Runnable getAction() {
            return action;
        }

        public MenuItem(String message, Runnable action) {
            this.message = message;
            this.action = action;
        }
    }
}
