/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store.serialization;

import store.Product;

/**
 *
 * @author apprentice
 * @param <T>
 */
public interface StringDeserializer<T extends Product> {

    public T deserialize(String textForm, String delimiter);
}
