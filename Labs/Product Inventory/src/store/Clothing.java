/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

/**
 *
 * @author apprentice
 */
public class Clothing extends Product {

    private final String size;
    private final String color;

    public Clothing(String name, String brand, String color, String size, double purchaseCost, double salePrice) {
        super(name, brand, purchaseCost, salePrice);
        this.size = size;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public String getSize() {
        return size;
    }
}
