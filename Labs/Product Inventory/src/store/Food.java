/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

/**
 *
 * @author apprentice
 */
public class Food extends Product {

    private final boolean perishable;
    private final String expiration;

    public Food(String name, String brand, double purchaseCost, double salePrice, boolean perishable, String expiration) {
        super(name, brand, purchaseCost, salePrice);
        this.perishable = perishable;
        this.expiration = expiration;
    }

    public boolean isPerishable() {
        return perishable;
    }

    public String getExpiration() {
        return expiration;
    }

}
