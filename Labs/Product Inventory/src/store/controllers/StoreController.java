/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import store.Clothing;
import store.Food;
import store.Product;
import store.ProductType;
import static store.ProductType.Clothing;
import store.Store;
import store.dao.InventoryDao;
import store.serialization.StringDeserializer;
import store.serialization.StringSerializer;
import store.ui.ConsoleIO;

public class StoreController {

    private final InventoryDao dao = new InventoryDao();
    private Store store = new Store();
    private final ConsoleIO io = new ConsoleIO();

    {
        dao.registerDeserializer(Product.class, (String textForm, String delimiter) -> {
            String[] tokens = textForm.split(delimiter);
            Product p = new Product(tokens[1], tokens[2], Double.parseDouble(tokens[3]), Double.parseDouble(tokens[4]));
            return p;
        });
        dao.registerSerializer(Product.class, (Product product, String delimiter) -> {
            StringBuilder b = new StringBuilder();
            b.append(product.getClass().getName()).append(delimiter);
            b.append(product.getName()).append(delimiter);
            b.append(product.getPurchaseCost()).append(delimiter);
            b.append(product.getSalePrice());
            return b.toString();
        });
        dao.registerDeserializer(Food.class, new StringDeserializer<Food>() {

            @Override
            public Food deserialize(String textForm, String delimiter) {
                String[] tokens = textForm.split(delimiter);
                return new Food(tokens[0], tokens[1], Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]), Boolean.parseBoolean(tokens[4]), tokens[5]);
            }
        });
        dao.registerSerializer(Food.class, new StringSerializer<Food>() {

            @Override
            public String serialize(Food product, String delimiter) {
                StringBuilder b = new StringBuilder();
                b.append(product.getClass().getCanonicalName()).append(delimiter);
                b.append(product.getName()).append(delimiter);
                b.append(product.getBrand()).append(delimiter);
                b.append(product.getPurchaseCost()).append(delimiter);
                b.append(product.getSalePrice()).append(delimiter);
                b.append(product.isPerishable()).append(delimiter);
                b.append(product.getExpiration()).append(delimiter);
                return b.toString();
            }

        });
        dao.registerDeserializer(Clothing.class, (String textForm, String delimiter) -> {
            String[] tokens = textForm.split(delimiter);
            Clothing p = new Clothing(tokens[0], tokens[1], tokens[2], tokens[3], Double.parseDouble(tokens[4]), Double.parseDouble(tokens[5]));
            return p;
        });
        dao.registerSerializer(Clothing.class, (Clothing product, String delimiter) -> {
            StringBuilder b = new StringBuilder();
            b.append(product.getClass().getName()).append(delimiter);
            b.append(product.getName()).append(delimiter);
            b.append(product.getBrand()).append(delimiter);
            b.append(product.getColor()).append(delimiter);
            b.append(product.getSize()).append(delimiter);
            b.append(product.getPurchaseCost()).append(delimiter);
            b.append(product.getSalePrice());
            return b.toString();
        });
    }

    public void run() {
        try {
            store = new Store(dao.load());
        } catch (IOException ex) {
            io.println("There was a problem trying to read from the file.");
        }
        int userIn = 0;
        while (userIn != 5) {
            printMenu();
            userIn = io.promptForIntInRange("Select: ", 1, 5);
            switch (userIn) {
                case 1:
                    addItem();
                    break;
                case 2:
                    removeItem();
                    break;
                case 3:
                    adjustInventory();
                    break;
                case 4:
                    viewInventory();
                    break;
            }
        }
    }

    private void printMenu() {
        io.println("Choose an option");
        io.println("1- Add a new product to inventory.");
        io.println("2- Remove a product from inventory.");
        io.println("3- Change stock of a product.");
        io.println("4- View inventory.");
        io.println("5- Exit.");
    }

    private void addItem() {
        ProductType category = selectCategory();
        Product product;
        switch (category) {
            case Clothing:
                product = createClothing();
                break;
            case Food:
                product = createFood();
                break;
            default:
                product = createProduct();
        }
        int initialQuantity = io.promptForInt("Enter initial inventory.");

        store.addNewProduct(product, initialQuantity);
        try {
            dao.write(store.getInventory());
        } catch (IOException ex) {
            io.println("Something went wrong while trying to write the inventory to the file.");
        }
    }

    private Product createProduct() {
        String name = io.promptForString("Enter new item name:");
        String brand = io.promptForString("Enter product brand:");
        double purchasePrice = io.promptForDouble("Enter purchase price of item:");
        double salePrice = io.promptForDouble("Enter sale price of item:");
        return new Product(name, brand, purchasePrice, salePrice);
    }

    private void removeItem() {
        {
            String name = io.promptForString("Enter the name of the item to remove.");
            List<Product> options = store.getProductsByName(name);
            if (options.size() > 0) {
                for (int i = 0; i < options.size(); i++) {
                    io.println((i + 1) + " " + options.get(i).getName());
                }
                int selection = io.promptForIntInRange("Enter the number of the item to remove, or 0 to cancel.", 0, options.size()) - 1;
                if (selection >= 0) {
                    store.removeProductFromInventory(options.get(selection));
                }
                io.promptForString("Product removed.");

                try {
                    dao.write(store.getInventory());
                } catch (IOException ex) {
                    io.println("Something went wrong while trying to write the inventory to the file.");
                }
            } else {
                io.println("No items matching " + name + " could be found.");
            }
            io.promptForString("Press Enter to continue.");
        }

    }

    private void adjustInventory() {
        String name = io.promptForString("Enter the name of the item to modify inventory for.");
        List<Product> options = store.getProductsByName(name);
        if (options.size() > 0) {
            for (int i = 0; i < options.size(); i++) {
                io.println((i + 1) + " " + options.get(i).getName());
            }
            int selection = io.promptForIntInRange("Enter the number of the item to select, or 0 to cancel.", 0, options.size()) - 1;
            if (selection >= 0) {
                Product selectedItem = options.get(selection);
                io.println(selectedItem.getName() + " has a current stock of " + store.getInventory().get(selectedItem));
                int change = io.promptForInt("Enter amount to adjust inventory by (negative to remove inventory).");
                store.adjustInventory(selectedItem, change);
                io.println(selectedItem.getName() + " stock is now " + store.getInventory().get(selectedItem));
                if (store.getInventory().get(selectedItem) < store.getMinimumStockLevel()) {
                    io.println("Inventory is now below " + store.getMinimumStockLevel());
                }
                try {
                    dao.write(store.getInventory());
                } catch (IOException ex) {
                    io.println("Something went wrong while trying to write the inventory to the file.");
                }
            }
        } else {
            io.println("No items matching " + name + " could be found.");
        }
        io.promptForString("Press Enter to continue.");
    }

    private void viewInventory() {
        io.println(String.format("Total inventory value: $%.2f", store.getValueOfInventory()));
        io.println("Value by category: ");
        for (String category : store.getCategories()) {
            io.println(String.format("%s: $%.2f", category, store.getValueOfCategoryInventory(category)));
        }
        io.println("");
        io.println("Value by item:");
        for (Product p : store.getProducts()) {
            io.println(String.format("%s: $%.2f / %d units", p.getName(), store.getValueOfProductInventory(p), store.getInventory().get(p)));
        }
        io.promptForString("Press enter to continue.");
    }

    private ProductType selectCategory() {
        List<ProductType> categories = Arrays.asList(ProductType.values());
        for (int i = 0; i < categories.size(); i++) {
            io.println(String.format("%d- %s", i + 1, categories.get(i)));
        }
        int selection = io.promptForIntInRange("Choose a category:", 1, categories.size()) - 1;
        return categories.get(selection);
    }

    private Clothing createClothing() {
        String name = io.promptForString("Enter new item name:");
        String brand = io.promptForString("Enter clothing brand:");
        String color = io.promptForString("Enter color:");
        String size = io.promptForString("Enter size:");
        double purchasePrice = io.promptForDouble("Enter purchase price of item:");
        double salePrice = io.promptForDouble("Enter sale price of item:");

        return new Clothing(name, brand, color, size, purchasePrice, salePrice);
    }

    private Product createFood() {
        String name = io.promptForString("Enter new item name:");
        String brand = io.promptForString("Enter product brand:");
        double purchasePrice = io.promptForDouble("Enter purchase price of item:");
        double salePrice = io.promptForDouble("Enter sale price of item:");
        boolean perishable = io.promptForString("Food perishable? y/n").equalsIgnoreCase("y");
        String expiration = io.promptForString("Enter expiration date");

        return new Food(name, brand, purchasePrice, salePrice, perishable, expiration);
    }
}
