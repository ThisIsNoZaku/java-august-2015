package store;

public enum ProductType {

    Product("Product"), Clothing("Clothing"), Food("Food");
    final String name;

    ProductType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
