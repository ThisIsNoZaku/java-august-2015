package store;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class Store {

    private final Map<Product, Integer> inventory;
    private final Map<Class<? extends Product>, Set<Product>> productsByCategory = new HashMap<>();
    private int minimumStockLevel = 0;

    public Store(Map<Product, Integer> inventory) {
        this.inventory = new HashMap<>(inventory);
        for (Product p : inventory.keySet()) {
            productsByCategory.putIfAbsent(p.getClass(), new HashSet<>());
            productsByCategory.get(p.getClass()).add(p);
        }
    }

    public Store() {
        this(Collections.EMPTY_MAP);
    }

    public void addNewProduct(Product product, int initialQuantity) {
        if (inventory.keySet().contains(product)) {
            throw new IllegalArgumentException(product.getName() + " is already present in the system, can't add it again.");
        }
        inventory.put(product, initialQuantity);
    }

    public List<Product> getProducts() {
        return Collections.unmodifiableList(new ArrayList<>(inventory.keySet()));
    }

    public List<Product> getProductsByName(String name) {
        return inventory.keySet().stream().filter(p -> p.getName().matches(name)).collect(Collectors.toList());
    }

    public Map<Product, Integer> getInventory() {
        return Collections.unmodifiableMap(inventory);
    }

    public Collection<String> getCategories() {
        return productsByCategory.keySet().stream().map(c -> c.getSimpleName()).collect(Collectors.toList());
    }

    public void removeProductFromInventory(Product p) {
        inventory.remove(p);
        for (Entry<Class<? extends Product>, Set<Product>> category : productsByCategory.entrySet()) {
            category.getValue().remove(p);
        }
        inventory.remove(p);
    }

    public int adjustInventory(Product p, int quantity) {
        //If trying to remove more than is in stock, remove all stock.
        if (inventory.get(p) < Math.abs(quantity)) {
            quantity = -inventory.get(p);
        }
        inventory.put(p, inventory.get(p) + quantity);
        return quantity;
    }

    public double getValueOfProductInventory(Product p) {
        return inventory.get(p) * p.getSalePrice();
    }

    public double getValueOfCategoryInventory(String category) {
        if (productsByCategory.containsKey(category)) {
            return productsByCategory.get(category).stream().mapToDouble(p -> inventory.get(p) * p.getSalePrice()).sum();
        } else {
            return 0;
        }
    }

    public int getMinimumStockLevel() {
        return minimumStockLevel;
    }

    public void setMinimumStockLevel(int minimumStockLevel) {
        this.minimumStockLevel = minimumStockLevel;
    }

    public double getValueOfInventory() {
        return inventory.keySet().stream().mapToDouble(p -> p.getSalePrice() * inventory.get(p)).sum();
    }
}
