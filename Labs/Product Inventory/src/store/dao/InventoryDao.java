/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store.dao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import store.Product;
import store.serialization.StringDeserializer;
import store.serialization.StringSerializer;

/**
 *
 * @author apprentice
 */
public class InventoryDao {

    private static final String delimiter = ",";
    private static final String inventoryPath = "inventory.txt";
    private static final String productsPath = "products.txt";
    private final Map<Class<?>, StringDeserializer<? extends Product>> deserializers = new HashMap<>();
    private final Map<Class<?>, StringSerializer<? extends Product>> serializers = new HashMap<>();

    public Map<Product, Integer> load() throws FileNotFoundException {
        File productFile = new File(productsPath);
        File quantityFile = new File(inventoryPath);
        Map<Product, Integer> inventory = new HashMap<>();

        if (!productFile.exists()) {
            System.out.println("Couldn't find product listings file.");
        } else if (!quantityFile.exists()) {
            System.out.println("Couldn't find inventory quantities file.");
        } else {
            Map<String, Product> products = new HashMap<>();
            Map<String, Integer> quantities = new HashMap<>();

            Scanner in = new Scanner(productFile);
            in.useDelimiter("[,\n]");

            while (in.hasNext()) {
                try {
                    String className = in.next();
                    Class<?> type = Class.forName(className);
                    StringDeserializer deserializer = deserializers.get(type);
                    if (deserializer == null) {
                        throw new IllegalStateException("No deserializer for " + className + " was found.");
                    }
                    //Taking substring from 1 eliminates the leading delimiter left in the buffer.
                    String entry = in.nextLine().substring(1);
                    Product product = deserializer.deserialize(entry, delimiter);

                    products.put(product.getName(), product);

                } catch (ClassNotFoundException | IllegalStateException ex) {
                    Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            in = new Scanner(quantityFile);
            in.useDelimiter("[,\n]");
            while (in.hasNext()) {
                quantities.put(in.next(), in.nextInt());
            }
            for (String name : products.keySet()) {
                inventory.put(products.get(name), quantities.getOrDefault(name, 0));
            }
        }
        return inventory;
    }

    public void write(Map<Product, Integer> products) throws IOException {
        File productFile = new File(productsPath);
        File quantityFile = new File(inventoryPath);
        BufferedWriter productsOut = new BufferedWriter(new FileWriter(productFile));
        BufferedWriter quantitiesOut = new BufferedWriter(new FileWriter(quantityFile));
        for (Entry<Product, Integer> productEntry : products.entrySet()) {
            try {
                Product product = productEntry.getKey();
                StringSerializer serializer = ((StringSerializer) serializers.get(product.getClass()));
                if (serializer == null) {
                    throw new IllegalStateException("No serializer is registered for " + product.getClass().getCanonicalName());
                }

                productsOut.append(serializer.serialize(product, delimiter));

                quantitiesOut.append(String.format("%s,%d",
                        product.getName(),
                        productEntry.getValue())).append("\n");
            } catch (IllegalStateException ex) {
                Logger.getLogger(InventoryDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        productsOut.flush();

        quantitiesOut.flush();
    }

    public void registerDeserializer(Class<?> type, StringDeserializer<?> deserializer) {
        deserializers.put(type, deserializer);
    }

    public void registerSerializer(Class<?> type, StringSerializer<?> serializer) {
        serializers.put(type, serializer);
    }
}
