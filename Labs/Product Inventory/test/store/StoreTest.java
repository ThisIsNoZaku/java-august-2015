package store;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StoreTest {

    Store store;

    public StoreTest() {
    }

    @Before
    public void setUp() {
        store = new Store();
    }

    @Test
    public void testAddNewInventoryItem() {
        Product p = new Product("Skittles", "Mars Inc.", .30, 1.00);
        store.addNewProduct(p, 120);
        assertTrue(store.getProducts().contains(p));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testErrorAddingExistingItem() {
        Product p = new Product("Skittles", "Mars Inc.", .30, 1.00);
        store.addNewProduct(p, 120);
        store.addNewProduct(p, 120);
    }

    @Test
    public void testRemoveItemFromInventory() {
        Product p = new Product("Skittles", "Mars Inc.", .30, 1.00);
        store.addNewProduct(p, 120);
        assertTrue(store.getProducts().contains(p));

        store.removeProductFromInventory(p);
        assertFalse(store.getProducts().contains(p));
    }

    @Test
    public void testRemovingTooMuchFromInventory() {
        Product p = new Product("Skittles", "Mars Inc.", .30, 1.00);
        store.addNewProduct(p, 20);
        assertTrue(store.adjustInventory(p, -100) == 20);
        assertTrue(store.getInventory().get(p) == 0);
    }

    @Test
    public void testCategories() {
        Product p = new Product("Skittles", "Mars Inc.", .30, 1.00);
        store.addNewProduct(p, 20);
        assertTrue(store.getCategories().contains("Candy"));
    }
}
