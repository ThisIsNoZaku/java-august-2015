/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rockpaperscissors;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaperScissors {

    private static final int MIN_ROUNDS = 1;
    private static final int MAX_ROUNDS = 10;

    private final static Random rand = new Random();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("How many rounds would you like to play? (1-10)");
        int rounds = sc.nextInt();
        if (rounds < MIN_ROUNDS || rounds > MAX_ROUNDS) {
            System.out.println("Sorry, you have to choose between 1 and 10.");
        }
        for (int i = 0; i < rounds; i++) {
            System.out.println("Choose your move:");
            System.out.println("1- Rock, 2- Paper, 3- Scissors");
            int userSelect = sc.nextInt();
            String userSelectName = "";
            switch (userSelect) {
                case 1:
                    userSelectName = "Rock";
                    break;
                case 2:
                    userSelectName = "Paper";
                    break;
                case 3:
                    userSelectName = "Scissors";
                    break;
            }
            int computerSelect = rand.nextInt(3) + 1;
            String computerSelectName = "";
            switch (userSelect) {
                case 1:
                    computerSelectName = "Rock";
                    break;
                case 2:
                    computerSelectName = "Paper";
                    break;
                case 3:
                    computerSelectName = "Scissors";
                    break;
            }
            System.out.println("The computer chose " + computerSelectName);
            if (userSelect == 1) {
                computerSelect %= 3;
            }
            if (computerSelect == 1) {
                userSelect %= 3;
            }

            if (userSelect == computerSelect) {
                System.out.println("Tie - Both players chose " + userSelectName + " .");
            } else if (userSelect > computerSelect) {
                System.out.println("You Win - " + userSelectName + " beats " + computerSelectName + ".");
            } else {
                System.out.println("You Lost - " + computerSelectName + " beats " + userSelectName + ".");
            }
        }
    }

    private static enum Selection {

        Rock("Scissors"), Paper("Rock"), Scissors("Paper");
        private final String defeats;

        Selection(String defeats) {
            this.defeats = defeats;
        }
    }
}
