/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author apprentice
 */
public class Car {

    private final String make;
    private final String model;
    private final int year;
    private final String manufacturer;
    private final int passengerCapacity;
    private int engineHorsepower;
    private int fuelEfficiencyCity;
    private int fuelEfficiencyHighway;
    private final double gasCapacity;
    private double remainingFuel;
    private String licensePlate;
    private String registeredOwner;

    public Car(String make, String model, int year, String manufacturer, int passengerCapacity, int gasCapacity) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.manufacturer = manufacturer;
        this.passengerCapacity = passengerCapacity;
        this.gasCapacity = gasCapacity;
    }

    public int driveTo(int distance, boolean city) {

    }

    public void replaceEngine(int horsepower, int highwayEfficiency, int cityEfficiency) {

    }

    public double refillTank(double quantity) {

    }
    
    public void changeOwner(String owner){
        
    }
}
