package models;

import java.util.Calendar;

/**
 *
 * @author apprentice
 */
public class Airplane {

    private String id;
    private String airline;
    private final int passengerCapacity;
    private final int cruisingSpeed;
    private final int maxRange;
    private String currentLocation = "";
    private final Calendar startOfOperation;

    public Airplane(int passengerCapacity, int cruisingSpeed, int maxRange, Calendar startOfOperation) {
        this.passengerCapacity = passengerCapacity;
        this.cruisingSpeed = cruisingSpeed;
        this.maxRange = maxRange;
        this.startOfOperation = startOfOperation;
    }

    public void setOperator(String newOperator) {
        this.airline = newOperator;
    }

    public void setCurrentLocation(String location) {
        this.currentLocation = location;
    }

    public String getId() {
        return id;
    }

    public String getAirline() {
        return airline;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public int getCruisingSpeed() {
        return cruisingSpeed;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public Calendar getStartOfOperation() {
        return startOfOperation;
    }
}
