/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class Truck {

    private final String make;
    private final String model;
    private final int year;
    private final String manufacturer;
    private final int passengerCapacity;
    private final int cargoCapacoty;
    private final Map<String, Integer> cargo = new HashMap<>();
    private int engineHorsepower;
    private int fuelEfficiencyCity;
    private int fuelEfficiencyHighway;
    private final double gasCapacity;
    private double remainingFuel;
    private String licensePlate;
    private String registeredOwner;
    private String driver;

    public Truck(String make, String model, int year, String manufacturer, int passengerCapacity, int cargoCapacoty, double gasCapacity) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.manufacturer = manufacturer;
        this.passengerCapacity = passengerCapacity;
        this.cargoCapacoty = cargoCapacoty;
        this.gasCapacity = gasCapacity;
    }

    public int driveTo(int distance, boolean city) {

    }

    public void replaceEngine(int horsepower, int highwayEfficiency, int cityEfficiency) {

    }

    public double refillTank(double quantity) {

    }

    public void changeOwner(String owner) {

    }
    
    public void addCargo(String cargo, int spaceTaken){
        
    }
    
    public void removeCargo(String cargo, int spaceFreed){
        
    }
}
