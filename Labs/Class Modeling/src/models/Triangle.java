/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.awt.Point;

/**
 *
 * @author apprentice
 */
public class Triangle {

    private final int baseLength;
    private final int height;
    private final Point center;
    private final double rotation;
    
    public Square setRotation(double rotation);

    public Square move(Point moveTo);

    public Square setBaseLength(int length);
    
    public Square setHeight(int height);
}
