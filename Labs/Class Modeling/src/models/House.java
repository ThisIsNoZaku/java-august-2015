/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.awt.Color;

/**
 *
 * @author apprentice
 */
public class House {

    private String owner;
    private final String address;
    private Color color;
    private int floors;
    private boolean basement;
    private int bedrooms;
    private int bathrooms;
    private int yardArea;

    public House(String address, int bedrooms, int bathrooms) {
        this.address = address;
        this.bedrooms = bedrooms;
        this.bathrooms = bathrooms;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String newOwner) {
        this.owner = newOwner;
    }

    public int getYardArea() {
        return yardArea;
    }

    public Color getColor() {
        return color;
    }

    public String getAddress() {
        return address;
    }

    public void paint(Color newColor) {
        this.color = newColor;
    }

    public int getFloorCount() {
        return floors;
    }

    public void addFloor(int newBedrooms, int newBathrooms) {
        floors++;
        bathrooms += newBathrooms;
        bedrooms += newBedrooms;
    }

    public boolean hasBasement() {
        return basement;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

}
