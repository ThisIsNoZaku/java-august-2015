package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class School {

    private String name;
    private int classCapacity;
    private final List<Student> students = new ArrayList<>();
    private final List<String> classes = new ArrayList<>();
    private final List<Employee> teachers = new ArrayList<>();
    private int annualBudget;

    public School(String name, int capacity) {
        this.name = name;
        this.classCapacity = capacity;
    }

    public void addCapacity(int additionalStudentCapacity) {
        classCapacity += additionalStudentCapacity;
    }

    public String getName() {
        return name;
    }

    public int getClassCapacity() {
        return classCapacity;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<String> getClasses() {
        return classes;
    }

    public List<Employee> getTeachers() {
        return teachers;
    }

    public int getAnnualBudget() {
        annualBudget = students.size() * 25_000;
        return annualBudget;
    }

    public void enrollStudent(Student student) {
        students.add(student);
    }

    public void expelStudent(Student student) {
        students.remove(student);
    }

    public void hireTeacher(Employee teacher) {
        teachers.add(teacher);
    }

    public void fireTeacher(Employee teacher, Calendar now) {
        teacher.fire(now);
        teachers.remove(teacher);
    }

    public void addClass(String name) {
        classes.add(name);
    }

    public void removeClass(String name) {
        classes.remove(name);
    }
}
