/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class Student {

    private final int id;
    private String name;
    private double gpa;
    private final Calendar enrollmentDate;
    //Contains class names and grade
    private final Map<String, Double> classes = new HashMap<>();
    private int tuitionDue;

    public Student(int id, String name, Calendar enrollmentDate) {
        this.id = id;
        this.name = name;
        this.enrollmentDate = enrollmentDate;
    }

    public void addClass(String className) {
        classes.put(className, 0.0);
    }

    public void removeClass(String className) {
        classes.remove(className);
    }

    public void changeGrade(String className, double newGrade) {
        classes.put(className, newGrade);
    }

    public double getGpa() {
        double gpa = 0;
        for (String className : classes.keySet()) {
            gpa += classes.get(className);
        }
        gpa /= classes.size();
        this.gpa = gpa;
        return this.gpa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTuitionDue() {
        return tuitionDue;
    }

    public void setTuitionDue(int tuitionDue) {
        this.tuitionDue = tuitionDue;
    }

    public int getId() {
        return id;
    }

    public Calendar getEnrollmentDate() {
        return enrollmentDate;
    }
}
