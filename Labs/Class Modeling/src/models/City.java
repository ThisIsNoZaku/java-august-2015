/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class City {

    private final String name;
    private final String country;
    private int population;
    //Maps addresses to building names. Null value indicates empty lot.
    private final Map<String, String> buildings = new HashMap<>();
    private int annualBudget;
    private int annualCosts;
    private final List<String> departments = new ArrayList<>();

    public City(String name, String country) {
        this.name = name;
        this.country = country;
    }

    /**
     * Removes the building at [code]address[/code], if any. Returns the name of
     * the building, if there was a building or null if there was none.
     *
     * @param address the address to bulldoze
     * @return the building name
     */
    public String bulldozeBuilding(String address) {
        return buildings.replace(address, null);
    }

    /**
     * Construct a new building named [code]name[/code] at [code]address[/code],
     * if it's empty. Returns the name of the building; if the returned name is
     * different from the given name, the address was already occupied and the
     * new building was not added.
     *
     * @param address the location
     * @param buildingName the name of the new building
     * @return the name of the building at location after the operation
     */
    public String constructBuilding(String address, String buildingName) {
        return buildings.putIfAbsent(address, buildingName);
    }

    /**
     * Get the name of the building at [code]address[/code].
     *
     * @param address the location
     * @return the building name at location
     */
    public String getBuilding(String address) {
        return buildings.get(address);
    }

    public int getAvailableBudget() {
        return annualBudget - annualCosts;
    }

    public Collection<String> getAddresses() {
        return buildings.keySet();
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getAnnualBudget() {
        return annualBudget;
    }

    public void setAnnualBudget(int annualBudget) {
        this.annualBudget = annualBudget;
    }

    public int getAnnualCosts() {
        return annualCosts;
    }

    public void setAnnualCosts(int annualCosts) {
        this.annualCosts = annualCosts;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public List<String> getDepartments() {
        return Collections.unmodifiableList(departments);
    }

    public void addDepartment(String department) {
        departments.add(department);
    }

    public void shutdownDepartment(String department) {
        departments.remove(department);
    }
}
