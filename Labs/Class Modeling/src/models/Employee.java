/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Calendar;

/**
 *
 * @author apprentice
 */
public class Employee {

    private final String firstName;
    private final String lastName;
    private final int id;
    private String department;
    private String boss;
    private int annualSalary;
    private int meetingQuota;
    private final Calendar hireDate;
    private int meetingsAttended = 0;
    private String jobTitle;
    private Calendar terminationDate = null;

    public Employee(String firstName, String lastName, int id, int annualSalary, Calendar hireDate, String jobTitle) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.annualSalary = annualSalary;
        this.hireDate = hireDate;
        this.jobTitle = jobTitle;

    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setMeetingQuota(int newMeetingQuota) {
        this.meetingQuota = newMeetingQuota;
    }

    public void setDepartment(String newDepartment) {
        this.department = newDepartment;
    }

    public void setBoss(String newBoss) {
        this.boss = newBoss;
    }

    public Calendar getTerminationDate() {
        return terminationDate;
    }

    public void fire(Calendar fireDate) {
        this.terminationDate = fireDate;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void annualRaise() {
        this.annualSalary += this.annualSalary * .02;
    }

    public void promote(String newJobTitle, int newSalary) {
        this.jobTitle = newJobTitle;
        this.annualSalary = newSalary;
    }

    public void goToMeeting() {
        this.meetingsAttended++;
    }

    public int getMeetingsAttended() {
        return this.meetingsAttended;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getId() {
        return this.id;
    }

    public String getDepartment() {
        return this.department;
    }

    public int getAnnualSalary() {
        return this.annualSalary;
    }

    public int getMeetingQuota() {
        return this.meetingQuota;
    }

    public Calendar getHireDate() {
        return this.hireDate;
    }

    public String getBoss() {
        return this.boss;
    }

}
