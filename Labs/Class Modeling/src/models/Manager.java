/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee {

    private final Set<Employee> subordinates = new TreeSet<>();
    private final Set<Employee> recentlyMicromanagedEmployees = new HashSet<>();

    public Manager(String firstName, String lastName, int id, int annualSalary, Calendar hireDate, String jobTitle) {
        super(firstName, lastName, id, annualSalary, hireDate, jobTitle);
    }

    public Collection<Employee> getSubordinates() {
        return Collections.unmodifiableCollection(subordinates);
    }

    public Collection<Employee> getMicromanagedEmployees() {
        return Collections.unmodifiableCollection(recentlyMicromanagedEmployees);
    }

    public void micromanageEmployee(Employee employee) {
        recentlyMicromanagedEmployees.add(employee);
    }
}
