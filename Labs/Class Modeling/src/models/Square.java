/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.awt.Point;

/**
 *
 * @author apprentice
 */
public class Square {

    private final int length;
    private final int height;
    private final double rotation;
    private final Point center;

    public Square setRotation(double rotation);

    public Square move(Point moveTo);

    public Square setLength(int length);

    public Square setHeigh(int height);
}
