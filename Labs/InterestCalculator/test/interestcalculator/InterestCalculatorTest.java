/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interestcalculator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InterestCalculatorTest {

    double principal;
    double interest;
    int duration;

    public InterestCalculatorTest() {
    }

    @Before
    public void setUp() {
        principal = 10_000;
        interest = 10;
        duration = 10;
    }

    @Test
    public void testInterestAccumulation() {
        for (InterestCalculator.CompoundingRates rate : InterestCalculator.CompoundingRates.values()) {;
            InterestCalculator calc = new InterestCalculator(principal, rate, interest);
            double targetPrincipal = principal;
            double targetAccruedInterest = 0;

            double accruedInterest = 0;
            for (int i = 1; i <= duration * rate.annualCompoundPeriods; i++) {
                double calculatedInterest = targetPrincipal * interest / rate.annualCompoundPeriods;
                targetPrincipal += calculatedInterest;
                targetAccruedInterest += calculatedInterest;

                accruedInterest += calc.applyInterest();
                assertEquals(targetPrincipal, calc.getPrincipal(), .00001);
                assertEquals(targetAccruedInterest, accruedInterest, .00001);
                if (i % rate.annualCompoundPeriods == 0) {
                    targetAccruedInterest = accruedInterest = 0;
                }
            }
        }
    }
}
