package interestcalculator;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {

    private double principal;
    private final int annualCompoundPeriods;
    private final double interestRate;

    public InterestCalculator(double startingPrincipal, CompoundingRates compoundPeriods, double interestRate) {
        principal = startingPrincipal;
        annualCompoundPeriods = compoundPeriods.annualCompoundPeriods;
        this.interestRate = interestRate;
    }

    public double applyInterest() {
        double interest = principal * interestRate / annualCompoundPeriods;
        principal += interest;
        return interest;
    }

    public double getPrincipal() {
        return principal;
    }

    public int getAnnualCompoundPeriods() {
        return annualCompoundPeriods;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public static enum CompoundingRates {

        DAILY(365), MONTHLY(12), QUARTERLY(4), ANNUALLY(1);
        int annualCompoundPeriods;

        private CompoundingRates(int periods) {
            annualCompoundPeriods = periods;
        }
    }
}
