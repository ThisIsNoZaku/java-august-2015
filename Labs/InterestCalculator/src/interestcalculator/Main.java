/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interestcalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the initial principal: $");
        double accountValue = sc.nextDouble();
        System.out.print("Enter the annual interest rate: %");
        double interestRate = (sc.nextDouble() / 100);
        System.out.print("Enter the number of years to invest for:");
        int investmentDuration = sc.nextInt();
        System.out.println("Enter the number of compounding periods each year: ");
        System.out.println("1- Annual (compound 1/year)");
        System.out.println("2- Quarterly (compound 4/year)");
        System.out.println("3- Monthly (compound 12/year)");
        System.out.println("4- Daily (compound 365/year)");
        int compoundChoice = sc.nextInt();
        InterestCalculator.CompoundingRates compound;
        switch (compoundChoice) {
            case 1:
                compound = InterestCalculator.CompoundingRates.ANNUALLY;
                break;
            case 2:
                compound = InterestCalculator.CompoundingRates.QUARTERLY;
                break;
            case 3:
                compound = InterestCalculator.CompoundingRates.MONTHLY;
                break;
            case 4:
                compound = InterestCalculator.CompoundingRates.DAILY;
                break;
            default:
                throw new Error();
        }

        InterestCalculator calc = new InterestCalculator(accountValue, compound, interestRate);

        for (int i = 1; i <= investmentDuration; i++) {
            System.out.println(String.format("Year %d:", i));
            System.out.println(String.format("Year beginnig principal was $%,.2f", accountValue));
            double accruedInterest = 0;
            for (int period = 0; period < compound.annualCompoundPeriods; period++) {
                double interest = calc.applyInterest();
                accruedInterest += interest;
            }
            System.out.println(String.format("Annual accrued interest was $%,.2f", accruedInterest));
            System.out.println(String.format("Year end account value is $%,.2f", accountValue));
            System.out.println();
        }
    }
}
