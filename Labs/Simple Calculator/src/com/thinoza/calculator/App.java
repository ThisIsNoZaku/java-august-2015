/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinoza.calculator;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        CalculatorEngine engine = new CalculatorEngine();
        ConsoleIO io = new ConsoleIO(System.in, System.out);
        String operator = io.promptForString("Choose an operation to perform: + - / * ");
        io.print("Enter two operands on new lines: ");
        int a = io.promptForInt("");
        int b = io.promptForInt("");
        String expression = a + operator + b;
        System.out.println(expression + " = " + engine.parse(expression));
    }
}
