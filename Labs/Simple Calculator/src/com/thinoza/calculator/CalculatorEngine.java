package com.thinoza.calculator;

import java.math.BigDecimal;

public class CalculatorEngine {

    public BigDecimal parse(String expression) {
        return new Expression(expression).compute();
    }
}
