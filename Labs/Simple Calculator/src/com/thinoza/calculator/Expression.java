package com.thinoza.calculator;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A mathematical expression.
 *
 * @author Damien
 *
 */
@SuppressWarnings("serial")
class Expression extends RecursiveTask<BigDecimal> {

    private final static Pattern splitPattern = Pattern.compile("((?<=[(^+/*\\-])|(?=[(^+/*\\-]))");
    private final static Pattern operatorPattern = Pattern.compile("[+\\-*/\\\\]");
    private final String text;

    public Expression(final String text) {
        validate(text);
        this.text = text;
    }

    @Override
    protected BigDecimal compute() {
        // Divide the expression text into tokens: operators and operands
        List<String> tokens = Arrays.asList(text.split(splitPattern.pattern()));
        // If the expression is a single number, return it's value
        if (tokens.size() == 1) {
            return new BigDecimal(tokens.get(0));
        }
        // Find the index of the left-most operator with the lowest precedence
        int lowestPrecedence = Integer.MAX_VALUE;
        int splitIndex = -1;
        Operator nextOperator = null;
        for (int i = 0; i < tokens.size(); i++) {
            try {
                nextOperator = Operator.fromString(tokens.get(i));
                if (nextOperator.getPrecedence() < lowestPrecedence) {
                    lowestPrecedence = nextOperator.getPrecedence();
                    splitIndex = i;
                }
            } catch (IllegalArgumentException e) {
                continue;
            }
        }
        String lhs = tokens.subList(0, splitIndex).stream().collect(Collectors.joining());
        String rhs = tokens.subList(splitIndex + 1, tokens.size()).stream().collect(Collectors.joining());

        // Split the tokens into left and right hand side at the split index
        // Create a new expression based on the chosen operator and evaluate it
        switch (Operator.fromString(tokens.get(splitIndex))) {
            case Divide:
                return new Expression(lhs).invoke().divide(new Expression(rhs).invoke());
            case Multipy:
                return new Expression(lhs).invoke().multiply(new Expression(rhs).invoke());
            case Parenthesis:
                int unmatchedParensCount = 1;
                int rightParensIndex = -1;
                for (int i = 0; i < rhs.length(); i++) {
                    if (rhs.charAt(i) == '(') {
                        unmatchedParensCount++;
                    } else if (rhs.charAt(i) == ')') {
                        unmatchedParensCount--;
                    }
                    if (unmatchedParensCount == 0) {
                        rightParensIndex = i;
                        break;
                    }
                }
                String innerExpression = rhs.substring(0, rightParensIndex);
                rhs = rightParensIndex < rhs.length() ? rhs.substring(rightParensIndex + 1, rhs.length())
                        : "";
                return new Expression(lhs
                        + new Expression(innerExpression).invoke().toString() + rhs).invoke();
            case Plus:
                return new Expression(lhs).invoke().add(new Expression(rhs).invoke());
            case Subtract:
                return new Expression(lhs).invoke().subtract(new Expression(rhs).invoke());
            default:
                assert false;
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }

    private static void validate(String text) {
        validateOperators(text);
        validateParentheses(text);
    }

    private static void validateOperators(String text) {
        char lastCharacter = text.charAt(0);
        for (int i = 1; i < text.length(); i++) {
            if (operatorPattern.matcher(text.charAt(i) + "").matches()
                    && operatorPattern.matcher(lastCharacter + "").matches()) {
                throw new IllegalArgumentException("Can't have two adjacent operators.");
            }
            lastCharacter = text.charAt(i);
        }
    }

    private static void validateParentheses(String text) {
        int unmatchedParensCount = 0;
        int unmatchedParensIndex = -1;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '(') {
                unmatchedParensCount++;
                if (unmatchedParensCount == 1) {
                    unmatchedParensIndex = i;
                }
            } else if (text.charAt(i) == ')') {
                unmatchedParensCount--;
                if (unmatchedParensCount == -1) {
                    unmatchedParensIndex = i;
                }
            }
        }
        if (unmatchedParensCount != 0) {
            throw new IllegalArgumentException("Unmatched parentheses found at "
                    + unmatchedParensIndex);
        }
    }
}
