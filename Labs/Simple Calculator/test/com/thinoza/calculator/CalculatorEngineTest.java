package com.thinoza.calculator;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CalculatorEngineTest {

    CalculatorEngine c = new CalculatorEngine();

    /**
     * Test that a a simple number returns itself.
     */
    @Test
    public void testValueParsing() {
        assertTrue(c.parse("1").equals(new BigDecimal("1")));
    }

    /**
     * Test simple addition.
     */
    @Test
    public void testSimpleAdding() {
        assertTrue(c.parse("1+1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test simple subtraction.
     */
    @Test
    public void testSimpleSubtration() {
        assertTrue(c.parse("3-1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test simple multiplication.
     */
    @Test
    public void testSimpleMultiplication() {
        assertTrue(c.parse("2*2").equals(new BigDecimal(Integer.parseInt("4"))));
    }

    /**
     * Test simple division
     */
    @Test
    public void testSimpleDivision() {
        assertTrue(c.parse("4/2").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test that operator precedence is adhered to.
     */
    @Test
    public void testOperatorPrecedence() {
        assertTrue(c.parse("2+4/2").equals(new BigDecimal(Integer.parseInt("4"))));
        assertTrue(c.parse("4/2+2").equals(new BigDecimal(Integer.parseInt("4"))));
        assertTrue(c.parse("2*2+2*2+2-2*2").equals(new BigDecimal(Integer.parseInt("6"))));
        assertTrue(c.parse("2+2*2-2/2").equals(new BigDecimal(Integer.parseInt("5"))));
    }

    /**
     * Test that parentheses are handled correctly
     */
    @Test
    public void testParentheses() {
        assertTrue(c.parse("(2+4)/2").equals(new BigDecimal(Integer.parseInt("3"))));
        assertTrue(c.parse("2+(4/2)").equals(new BigDecimal(Integer.parseInt("4"))));
    }

    /**
     * Test that unnecessary parentheses don't cause errors.
     */
    @Test
    public void testUnnecessaryParentheses() {
        assertTrue(c.parse("2+(2)").equals(new BigDecimal(Integer.parseInt("4"))));
        assertTrue(c.parse("(2)").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test that multiply nested parentheses are safely ignored.
     */
    @Test
    public void testMultipleNestedParentheses() {
        assertTrue(c.parse("((((2))))").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test that multiple nested parentheses are handled correctly
     */
    @Test
    public void testNestedParentheses() {
        assertTrue(c.parse("((2+4)/(1+1))*2").equals(new BigDecimal(Integer.parseInt("6"))));
    }

    /**
     * Test inputting an invalid expression where multiple operators are placed
     * next to each other.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdjacentOperators() {
        assertTrue(c.parse("1++1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test unmatched right parenthesis.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUnclosedLeftParentheses() {
        try {
            assertTrue(c.parse("1+1)").equals(new BigDecimal(Integer.parseInt("2"))));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    /**
     * Test unmatched parenthesis.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUnclosedParentheses() {
        try {
            assertTrue(c.parse("(1+1").equals(new BigDecimal(Integer.parseInt("2"))));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }
}
