/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape {

    private final int base;
    private final int sideA;
    private final int sideB;
    private Double area = null;

    public Triangle(int base, int sideA, int sideB) {
        this.base = base;
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getArea() {
        if (area == null) {
            double p = getPerimeter() / 2;
            //Heron's Formula
            area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - base));
        }
        return area;
    }

    @Override
    public double getPerimeter() {
        return base + sideA + sideB;
    }
}
