
import java.util.Scanner;

public class RockPaperScissors4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter how many rounds to play.");
        RockPaperScissorsGame game = RockPaperScissorsGame.startNewGame();
        int roundCount = scan.nextInt();
        for (int i = 0; i < roundCount; i++) {
            System.out.println("Round " + (i + 1));
            System.out.println("Choose a move: ");
            System.out.println("1- Rock");
            System.out.println("2- Paper");
            System.out.println("3- Scissors");
            int playerChoice = scan.nextInt();
            if (!(playerChoice >= 1 && playerChoice <= 3)) {
                System.out.println("You have to pick 1, 2 or 3.");
                i--;
                continue;
            }
            RockPaperScissorsGame.Move playerMove = getMove(playerChoice);

            int computerChoice = (int) (Math.random() * 3 + 1);
            RockPaperScissorsGame.Move computerMove = getMove(computerChoice);
            System.out.println("Computer chose " + computerMove.getName());
            switch (game.playRound(playerMove, computerMove)) {
                case PLAYER_A_WIN:
                    System.out.println("You win!");
                    break;
                case PLAYER_B_WIN:
                    System.out.println("The computer won...");
                    break;
                case TIE:
                    System.out.println("Tie!");
                    break;
            }
        }
        System.out.println(String.format("You won %d games, lost %d and tied %d", game.getPlayerAWins(), game.getPlayerBWins(), roundCount - game.getPlayerAWins() - game.getPlayerBWins()));
        int winLossDiff = game.getPlayerAWins() - game.getPlayerBWins();
        if (winLossDiff > 0) {
            System.out.println("You won the most rounds!");
        } else if (winLossDiff < 0) {
            System.out.println("You lost more than you won...");
        } else {
            System.out.println("I guess the whole thing was a tie.");
        }
    }

    private static RockPaperScissorsGame.Move getMove(int choice) {
        switch (choice) {
            case 1:
                return RockPaperScissorsGame.Move.moves.get("Rock");
            case 2:
                return RockPaperScissorsGame.Move.moves.get("Paper");
            case 3:
                return RockPaperScissorsGame.Move.moves.get("Scissors");
        }
        throw new IllegalArgumentException();
    }

}
