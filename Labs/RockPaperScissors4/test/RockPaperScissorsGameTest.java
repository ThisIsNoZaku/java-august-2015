/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RockPaperScissorsGameTest {

    RockPaperScissorsGame game;

    public RockPaperScissorsGameTest() {
    }

    @Before
    public void setUp() {
        game = RockPaperScissorsGame.startNewGame();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRockVsRock() {
        assertEquals(RockPaperScissorsGame.GameResult.TIE, game.playRound(RockPaperScissorsGame.Move.moves.get("Rock"), RockPaperScissorsGame.Move.moves.get("Rock")));
    }

    @Test
    public void testPaperVsPaper() {
        assertEquals(RockPaperScissorsGame.GameResult.TIE, game.playRound(RockPaperScissorsGame.Move.moves.get("Paper"), RockPaperScissorsGame.Move.moves.get("Paper")));
    }

    @Test
    public void testScissorsVsScissors() {
        assertEquals(RockPaperScissorsGame.GameResult.TIE, game.playRound(RockPaperScissorsGame.Move.moves.get("Scissors"), RockPaperScissorsGame.Move.moves.get("Scissors")));
    }

    @Test
    public void testRockVsPaper() {
        assertEquals(RockPaperScissorsGame.GameResult.PLAYER_B_WIN, game.playRound(RockPaperScissorsGame.Move.moves.get("Rock"), RockPaperScissorsGame.Move.moves.get("Paper")));
    }

    @Test
    public void testPaperVsScissors() {
        assertEquals(RockPaperScissorsGame.GameResult.PLAYER_B_WIN, game.playRound(RockPaperScissorsGame.Move.moves.get("Paper"), RockPaperScissorsGame.Move.moves.get("Scissors")));
    }

    @Test
    public void testScissorsVsPaper() {
        assertEquals(RockPaperScissorsGame.GameResult.PLAYER_B_WIN, game.playRound(RockPaperScissorsGame.Move.moves.get("Scissors"), RockPaperScissorsGame.Move.moves.get("Rock")));
    }
}
